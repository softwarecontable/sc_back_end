<!DOCTYPE html>
<html>
<head>
  <title>Invoice</title>
  <style type="text/css">
    /* CSS for styling the invoice */
    body {
        margin-top: 10px;
    }
    .text-center {
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        font-size: 11px;
        text-align: center;
    }
    .text {
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        font-size: 11px;
    }
    .bold {
        font-weight: bold;
    }
    /* Create two unequal columns that floats next to each other */
    .column {
        float: left;
        /* padding: 10px; */
        /* height: 80px; Should be removed. Only for demonstration */
    }
    .logo {
        height: 96px;
    }
    .col-1 {width: 8.33%;}
    .col-2 {width: 16.66%;}
    .col-3 {width: 25%;}
    .col-4 {width: 33.33%;}
    .col-5{
        width: 41.66%;
        align-content: center
    }
    .col-6 {width: 50%;}
    .col-8 {width: 66.66%;}
    .col-9 {width: 75%;}
    .col-10 {width: 83.33%;}
    .col-11 {width: 91.66%}
    .offset-1{margin-left:8.333333%}
    .offset-2{margin-left:16.666667%}
    .offset-3{margin-left:25%}
    .offset-4{margin-left:33.333333%}
    .offset-5{margin-left:41.666667%}
    .offset-6{margin-left:50%}
    .offset-7{margin-left:58.333333%}
    .offset-8{margin-left:66.666667%}
    .offset-9{margin-left:75%}
    .offset-10{margin-left:83.333333%}
    .offset-11{margin-left:91.666667%}
    .title {
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        font-size: 18px;
        font-weight: bold;
    }
    .subtitle {
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        font-size: 14px;
        font-weight: bold;
    }
    .detail-text {
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        font-size: 10px;
    }
    .shadowed {
        border:1px solid #EBEBEB;
        background-color: #EBEBEB;
        border-radius: 10px;
        padding: 16px;
    }
    .border {
        border:1px solid #C4C4C4;
        border-radius: 10px;
        padding: 8px;
        margin: 8px;
    }

    /* Clear floats after the columns */
    .row:after {
        content: "";
        display: table;
        clear: both;
    }
    .invoice-header {
        clear: both;
        background-color: #fdf6e9;
    }
    .text-document {
        font-size: 16px;
        font-weight: bold
    }
    hr {
        display: block;
        height: 1px;
        border: 0;
        border-top: 1px solid #ccc;
        /* margin: 1em 0; */
        padding: 0;
    }
    .container {
        padding: 8px;
    }
    .parent {
        padding: 0px 8px 0px 8px;
    }
    .client-label {
        display: inline-block;
        width: 20%;
        /* vertical-align: top; */
    }
    .client-dot {
        display: inline-block;
        width: 2%;
        /* vertical-align: top; */
    }
    .client-data {
        display: inline-block;
        width: 70%;
    }
    .price-label {
        display: inline-block;
        width: 70%;
    }
    .price-data {
        display: inline-block;
        width: 25%;
    }
    .voucher {
        display: inline-block;
        width: 45%;
    }
    p {
        margin-top: 6px;
        margin-bottom: 6px;
    }
    .qr {
        margin-top: 20px;
        text-align: left;
    }
    .footer {
        display: flex;
        align-items: center;
    }
    .light-weight {
        line-height: 1;
    }
  </style>
</head>
<body>
    <div class="row">
        <div class="column col-2" style="text-align: center">
            <img class="logo" src="{{ $company->logo }}" alt="Company Logo">
        </div>
        <div class="column col-6" style="line-height: 1.5rem;">
            <div class="text-center title">{{ $company->commercial_name }}</div>
            <div class="text-center subtitle light-weight">{{ $company->name }}</div>
            <div class="text-center light-weight">{{ $request->company['address']['direccion'] }}</div>
            <div class="text-center light-weight">{{ $user->cell_phone }}</div>
            <div class="text-center light-weight">{{ $user->email }}</div>
        </div>
        <div class="column col-4">
            <div class="shadowed">
                <div class="text-center text-document">RUC {{ $company->ruc }}</div>
                <div class="text-center text-document">{{ $request->voucherName }}</div>
                <div class="text-center text-document">ELECTRÓNICA</div>
                <div class="text-center text-document">{{ $request->voucher }}</div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top:20px">
        <div class="column col-6">
            <div class="border">
                <div>
                    <div class="client-label"><p class="text bold">RUC/DNI:</p></div>
                    <div class="client-data"><p class="text">{{ $request->client['numDoc'] }}</p></div>
                </div>

                <div>
                    <div class="client-label"><p class="text bold">Nombre:</p></div>
                    <div class="client-data"><p class="text">{{ $request->client['rznSocial'] }}</p></div>
                </div>

                <div>
                    <div class="client-label"><p class="text bold">Dirección:</p></div>
                    <div class="client-data"><p class="text">{{ $request->client['address']['direccion'] }}</p></div>
                </div>
            </div>
        </div>
        <div class="column col-6">
            <div class="border">

                <div class="voucher"><p class="text bold">Forma Pago:</p></div>
                <div class="voucher"><p class="text">{{ $request->planPago }}</p></div>

                <div class="voucher"><p class="text bold">Fecha de Emisión:</p></div>
                <div class="voucher"><p class="text">{{ date_format(now()->parse($request->fechaEmision), "d/m/Y") }}</p></div>

                <div class="voucher"><p class="text bold">Moneda:</p></div>
                <div class="voucher"><p class="text">Soles</p></div>

            </div>
        </div>
    </div>

<!-- Insert details header -->

    <div class="container">
        <div class="parent invoice-header">
            <div class="row">
                <div class="column col-1"><p class="detail-text bold">ÍTEM</p></div>
                <div class="column col-4"><p class="detail-text bold">DESCRIPCIÓN</p></div>
                <div class="column col-1"><p class="detail-text bold">UNIDAD</p></div>
                <div class="column col-1"><p class="detail-text bold">CANTIDAD</p></div>
                <div class="column col-3"><p class="detail-text bold" style="text-align: center">PRECIO UNITARIO</p></div>
                <div class="column col-1"><p class="detail-text bold">DSCTO</p></div>
                <div class="column col-1"><p class="detail-text bold" style="text-align: right">TOTAL</p></div>
            </div>
        </div>

        @foreach ($request->details as $item)
            <div class="parent">
                <div class="row">
                    <div class="column col-1"><p class="detail-text">{{ $loop->iteration }}</p></div>
                    <div class="column col-4"><p class="detail-text">{{ $item['descripcion'] }}</p></div>
                    <div class="column col-1"><p class="detail-text">{{ $item['unidad'] }}</p></div>
                    <div class="column col-1"><p class="detail-text">{{ $item['cantidad'] }}</p></div>
                    <div class="column col-3"><p class="detail-text" style="text-align: center">{{ number_format($item['mtoPrecioUnitario'], 2) }}</p></div>
                    <div class="column col-1"><p class="detail-text">{{ number_format($item['nominalDiscount'], 2) }}</p></div>
                    <div class="column col-1"><p class="detail-text" style="text-align: right">{{ number_format($item['nominalTotal'], 2) }}</p></div>
                </div>
            </div>
        @endforeach

    </div>

    <div class="border" style="margin: 0px; margin-top:10px">

        <div class="row">

            <div class="column col-8">
                <p class="text"><b>SON:</b> {{ $request->legends[0]['value'] }}</p>

                <div class="qr">
                    <img src="data:image/png;base64,{!! base64_encode($qrcode) !!}" />
                </div>
                <p class="text roboto">Autorizado mediante resolución Nº 034-005-0005038/SUNAT</p>
                <p class="text roboto">Representación impresa del comprobante electrónico</p>

            </div>

            <div class="column col-4">

                <div>
                    <div class="price-label detail-text" style="text-align: right"><p class="text">Total descuentos</p></div>
                    <div class="price-data detail-text" style="text-align: right"><p class="text">{{ number_format($request->discount, 2) }}</p></div>
                </div>

                <div>
                    <div class="price-label detail-text" style="text-align: right"><p class="text">Operación exonerada</p></div>
                    <div class="price-data detail-text" style="text-align: right"><p class="text">{{ number_format($request->exonerated, 2) }}</p></div>
                </div>

                <div>
                    <div class="price-label detail-text" style="text-align: right"><p class="text">Operación gravada</p></div>
                    <div class="price-data detail-text" style="text-align: right"><p class="text">{{ number_format($request->mtoOperGravadas, 2) }}</p></div>
                </div>

                <br>

                <div>
                    <div class="price-label detail-text" style="text-align: right"><p class="text">IGV</p></div>
                    <div class="price-data detail-text" style="text-align: right"><p class="text">{{ number_format($request->totalImpuestos, 2) }}</p></div>
                </div>

                <div>
                    <div class="price-label detail-text" style="text-align: right"><p class="text">Propina</p></div>
                    <div class="price-data detail-text" style="text-align: right"><p class="text">{{ number_format($request->tip, 2) }}</p></div>
                </div>

                <div>
                    <div class="price-label detail-text" style="text-align: right"><p class="text bold">IMPORTE TOTAL</p></div>
                    <div class="price-data detail-text" style="text-align: right"><p class="text bold">{{ number_format($request->subTotal, 2) }}</p></div>
                </div>

            </div>

        </div>

    </div>

    <div class="border" style="margin: 0px; margin-top:20px">
        <b class="subtitle">Observaciones:</b>
    </div>

    <div class="row" style="margin-top: 20px">

        <div class="column col-4">

            <div class="border" style="margin: 0px">
                <b class="subtitle">FORMA DE PAGO</b>

                <div>
                    <div class="price-label detail-text" style="text-align: left">
                        <p class="text">{{ $request->payment_detail }}</p>
                    </div>
                    <div class="price-data detail-text" style="text-align: right">
                        <p class="text">S/ {{ number_format($item['nominalTotal'], 2) }}</p>
                    </div>
                </div>

            </div>

        </div>

        <div class="column offset-5 col-3">

            <div class="footer">
                <div class="client-label" style="vertical-align: middle">
                    <p class="text bold">Powered</p>
                    <p class="text bold">by Zumar</p>
                </div>
                <div class="client-data" style="margin-top: 20px; margin-left: 10px">
                    <img src="{{ $logo }}" alt="zumar Logo" width="80px" height="auto">
                </div>
            </div>

        </div>

    </div>

</body>
</html>
