<!DOCTYPE html>
<html>
<head>
  <title>Invoice</title>
  <style type="text/css">
    /* CSS for styling the invoice */
    body {
        margin-top: 10px;
    }
    .text-center {
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        font-size: 11px;
        text-align: center;
    }
    .text {
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        font-size: 11px;
    }
    .bold {
        font-weight: bold;
    }
    /* Create two unequal columns that floats next to each other */
    .column {
        float: left;
        /* padding: 10px; */
        /* height: 80px; Should be removed. Only for demonstration */
    }
    .logo {
        height: 96px;
    }
    .col-1 {width: 8.33%;}
    .col-2 {width: 16.66%;}
    .col-3 {width: 25%;}
    .col-4 {width: 33.33%;}
    .col-5{
        width: 41.66%;
        align-content: center
    }
    .col-6 {width: 50%;}
    .col-8 {width: 66.66%;}
    .col-9 {width: 75%;}
    .col-10 {width: 83.33%;}
    .col-11 {width: 91.66%}
    .offset-1{margin-left:8.333333%}
    .offset-2{margin-left:16.666667%}
    .offset-3{margin-left:25%}
    .offset-4{margin-left:33.333333%}
    .offset-5{margin-left:41.666667%}
    .offset-6{margin-left:50%}
    .offset-7{margin-left:58.333333%}
    .offset-8{margin-left:66.666667%}
    .offset-9{margin-left:75%}
    .offset-10{margin-left:83.333333%}
    .offset-11{margin-left:91.666667%}
    .title {
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        font-size: 18px;
        font-weight: bold;
    }
    .subtitle {
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        font-size: 14px;
        font-weight: bold;
    }
    .detail-text {
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        font-size: 10px;
    }
    .shadowed {
        border:1px solid #EBEBEB;
        background-color: #EBEBEB;
        border-radius: 10px;
        padding: 16px;
    }
    .border {
        border:1px solid #C4C4C4;
        border-radius: 10px;
        padding: 8px;
        margin: 8px;
    }

    /* Clear floats after the columns */
    .row:after {
        content: "";
        display: table;
        clear: both;
    }
    .invoice-header {
        clear: both;
        background-color: #fdf6e9;
    }
    .text-document {
        font-size: 16px;
        font-weight: bold
    }
    hr {
        display: block;
        height: 1px;
        border: 0;
        border-top: 1px solid #ccc;
        /* margin: 1em 0; */
        padding: 0;
    }
    .container {
        padding: 8px;
    }
    .parent {
        padding: 0px 8px 0px 8px;
    }
    .client-label {
        display: inline-block;
        width: 20%;
        /* vertical-align: top; */
    }
    .client-dot {
        display: inline-block;
        width: 2%;
        /* vertical-align: top; */
    }
    .client-data {
        display: inline-block;
        width: 70%;
    }
    .voucher {
        display: inline-block;
        width: 45%;
    }
    p {
        margin-top: 6px;
        margin-bottom: 6px;
    }
    .qr {
        margin-top: 20px;
    }
  </style>
</head>
<body>

<div class="container" style="margin-top:20px">

    <div class="qr">
        {!! $qrcode !!}
    </div>

</div>

</body>
</html>
