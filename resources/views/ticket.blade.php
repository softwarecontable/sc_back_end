<!DOCTYPE html>
<html>
<head>
    <title>Invoice</title>
    <style type="text/css">
        /* CSS for styling the invoice */
        body {
            margin-top: 16px;
        }
        .container {
            padding: 4px;
            /* width: 40%;
            margin-left: 30%; */
            background-color: #fff
        }
        .text-center {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 32px; /** Change */
            text-align: center;
            color: #010101;
        }
        .text {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 32px; /** Change */
            color: #010101;
        }
        .bold {
            font-weight: bold;
        }
        .bold-600 {
            font-weight: 600;
        }
        /* Create two unequal columns that floats next to each other */
        .column {
            float: left;
            /* padding: 10px; */
            /* height: 80px; Should be removed. Only for demonstration */
        }
        .logo {
            height: 256px;  /** Change */
            text-align: center;
        }
        .col-1 {width: 8.33%;}
        .col-2 {width: 16.66%;}
        .col-3 {width: 25%;}
        .col-4 {width: 33.33%;}
        .col-5{
            width: 41.66%;
            align-content: center
        }
        .col-6 {width: 50%;}
        .col-8 {width: 66.66%;}
        .col-9 {width: 75%;}
        .col-10 {width: 83.33%;}
        .col-11 {width: 91.66%}
        .col-12 {width: 100%}
        .offset-1{margin-left:8.333333%}
        .offset-2{margin-left:16.666667%}
        .offset-3{margin-left:25%}
        .offset-4{margin-left:33.333333%}
        .offset-5{margin-left:41.666667%}
        .offset-6{margin-left:50%}
        .offset-7{margin-left:58.333333%}
        .offset-8{margin-left:66.666667%}
        .offset-9{margin-left:75%}
        .offset-10{margin-left:83.333333%}
        .offset-11{margin-left:91.666667%}
        .title {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 52px;  /** Change */
            color: #010101;
            font-weight: bold;
        }
        .subtitle {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 40px; /** Change */
            font-weight: bold;
            color: #010101;
        }
        .detail-text {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 32px; /** Change */
        }
        .shadowed {
            border:1px solid #EBEBEB;
            background-color: #EBEBEB;
            border-radius: 24px; /** Change */
            padding: 40px; /** Change */
        }
        .border {
            border:1px solid #C4C4C4;
            border-radius: 24px; /** Change */
            margin: 16px 16px 16px 16px; /** Change */
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }
        .invoice-header {
            clear: both;
            background-color: #fdf6e9;
        }
        .text-document {
            font-size: 48px; /** Change */
            font-weight: bold
        }
        hr {
            display: block;
            height: 3px; /** Change */
            border: 0;
            border-top: 3px solid #ccc; /** Change */
            /* margin: 1em 0; */
            padding: 0;
            width: 96%;
            margin-left: 2%;
        }
        .divider {
            width: 95%;
            height: 0;
            border: medium none;
            border-top: 3px dashed #C4C4C4!important /** Change */
        }
        .vl-right {
            border-right: 3px dashed #C4C4C4; /** Change */
            height: 160px;
            margin-right:-12px; /* size of gutter */ /** Change */
        }
        .vl-left {
            border-left: 3px dashed #C4C4C4; /** Change */
            height: 160px;
            margin-left:-12px; /* size of gutter */ /** Change */
        }
        .parent {
            padding: 0px 20px 0px 20px; /** Change */
        }
        p {
            margin: 16px 0px 16px 0px;
        }
        .center {
            text-align: center;
        }
        .letterhead {
            background-image: url('http://35.91.239.107/images/ticket_img.png');
            background-repeat: repeat-y;
            background-size: 20% 60%;
        }
        .qr {
            margin-top: 40px;
            text-align: center;
        }
        .header {
            margin-top: 4px;
            margin-bottom: 4px;
        }
        .title .header {
            margin-bottom: 0px;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="center">
            <img class="logo" src="{{ $company->logo }}" alt="Company Logo">
        </div>
        <div class="text-center">
            <p class="title">{{ $company->commercial_name }}</p>
        </div>
        <div class="text-center"><p class="text header">{{ $company->name }}</p></div>
        <div class="text-center"><p class="text header">{{ $company->ruc }}</p></div>
        <div class="text-center"><p class="text header">{{ $request->company['address']['direccion'] }}</p></div>

        <hr>

        <div class="letterhead">
            <div class="text-center"><p class="text">{{ $request->voucherName }}</p></div>
            <div class="text-center"><p class="text header bold">Nº {{ $request->voucher }}</p></div>
            <div class="text-center"><p class="text header">Fecha: {{ date_format(now()->parse($request->fechaEmision), "d/m/Y") }}</p></div>
            <div class="text-center"><p class="text header">Hora: {{ date_format(now()->parse($request->fechaEmision), "H:i:s") }}</p></div>

            <div class="row parent" style="margin-top: 30px">
                <div class="column col-4">
                    <p class="text bold-600">Razón social:</p>
                </div>
                <div class="column col-8">
                    <p class="text">{{ $request->client['rznSocial'] }}</p>
                </div>
            </div>
            <div class="row parent">
                <div class="column col-4">
                    <p class="text bold-600">RUC/DNI:</p>
                </div>
                <div class="column col-8">
                    <p class="text">{{ $request->client['numDoc'] }}</p>
                </div>
            </div>
            <div class="row parent">
                <div class="column col-4">
                    <p class="text bold-600">Correo:</p>
                </div>
                <div class="column col-8">
                    <p class="text">{{ $request->client['email'] }}</p>
                </div>
            </div>
            <div class="row parent">
                <div class="column col-4">
                    <p class="text bold-600">Dirección:</p>
                </div>
                <div class="column col-8">
                    <p class="text">{{ $request->client['address']['direccion'] }}</p>
                </div>
            </div>
            <div class="row parent">
                <div class="column col-4">
                    <p class="text bold-600">Plazo de pago:</p>
                </div>
                <div class="column col-8">
                    <p class="text">{{ $request->planPago }}</p>
                </div>
            </div>
            <div class="row parent">
                <div class="column col-4">
                    <p class="text bold-600">Vencimiento:</p>
                </div>
                <div class="column col-8">
                    <p class="text">{{ $request->expiration }}</p>
                </div>
            </div>

            <hr>

            @foreach ($request->details as $item)
                <div class="border">
                    <p class="text" style="margin-left:30px;margin-right:30px">{{ $item['descripcion'] }}</p>
                    <hr class="divider">

                    <div class="row" style="padding-bottom: 24px;"> <!-- Change -->
                        <div class="column col-3 vl-right">
                            <p class="text bold center">Precio:</p>
                            <p class="text center">s/ {{ number_format($item['nominalTotal'], 2) }}</p>
                        </div>
                        <div class="column col-4">
                            <p class="text bold center">Cantidad:</p>
                            <p class="text center">{{ $item['cantidad'] }}</p>
                        </div>
                        <div class="column col-2 vl-left">
                            <p class="text bold center">Dscto.</p>
                            <p class="text center">s/ {{ number_format($item['nominalDiscount'], 2) }}</p>
                        </div>
                        <div class="column col-3">
                            <p class="text bold center">Subtotal:</p>
                            <p class="text center" style="color: #FDB024">s/ {{ number_format($item['nominalTotal'] * $item['cantidad'] - $item['nominalDiscount'], 2) }}</p>
                        </div>
                    </div>
                </div>
            @endforeach

            <div style="margin-left:24px;margin-right:24px"> <!-- Change -->

                <div class="row">
                    <div class="column col-6">
                        <p class="text">Descuentos</p>
                    </div>
                    <div class="column col-6">
                        <p class="text" style="text-align:right">s/ {{ number_format($request->discount, 2) }}</p>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="column col-6">
                        <p class="text">Op. Exonerada</p>
                    </div>
                    <div class="column col-6">
                        <p class="text" style="text-align:right">s/ {{ number_format($request->exonerated, 2) }}</p>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="column col-6">
                        <p class="text">Op. Gravada</p>
                    </div>
                    <div class="column col-6">
                        <p class="text" style="text-align:right">s/ {{ number_format($request->mtoOperGravadas, 2) }}</p>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="column col-6">
                        <p class="text">IGV</p>
                    </div>
                    <div class="column col-6">
                        <p class="text" style="text-align:right">s/ {{ number_format($request->totalImpuestos, 2) }}</p>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="column col-6">
                        <p class="text">Propina</p>
                    </div>
                    <div class="column col-6">
                        <p class="text" style="text-align:right">s/ {{ number_format($request->tip, 2) }}</p>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="column col-6">
                        <p class="subtitle">Total</p>
                    </div>
                    <div class="column col-6">
                        <p class="subtitle" style="text-align:right">s/ {{ number_format($request->subTotal, 2) }}</p>
                    </div>
                </div>

                <p class="text">Importe en letras: <b>{{ $request->legends[0]['value'] }}</b></p>
                {{-- <p class="text">Representación impresa de la factura de venta electrónica, esta puede ser consultada nuestra <a href="http://crib.conceptomercado.com">página</a></p> --}}

                <div class="qr">
                    <img src="data:image/png;base64,{!! base64_encode($qrcode) !!}" />
                </div>

            </div>

        </div>

    </div>
</body>
</html>
