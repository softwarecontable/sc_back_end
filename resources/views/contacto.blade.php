<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>

    <div>
      <h3>{{ $user['name'] }}</h3>
    </div>

    <div>
      <p>{{ $user['body'] }}</p>
    </div>

    <div>
      Correo de contacto: <strong>{{ $user['email'] }}</strong>
    </div>

    <div>
      Número de contacto: <strong>{{ $user['phone'] }}</strong>
    </div>

  </body>
</html>
