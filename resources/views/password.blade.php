<!DOCTYPE html>

<html lang="en" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
<title></title>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!--[if mso]><xml><o:OfficeDocumentSettings><o:PixelsPerInch>96</o:PixelsPerInch><o:AllowPNG/></o:OfficeDocumentSettings></xml><![endif]-->
<!--[if !mso]><!-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
<!--<![endif]-->
<style>
		* {
			box-sizing: border-box;
		}

		th.column {
			padding: 0
		}

		a[x-apple-data-detectors] {
			color: inherit !important;
			text-decoration: inherit !important;
		}

		#MessageViewBody a {
			color: inherit;
			text-decoration: none;
		}

		p {
			line-height: inherit
		}

		@media (max-width:620px) {
			.icons-inner {
				text-align: center;
			}

			.icons-inner td {
				margin: 0 auto;
			}

			.row-content {
				width: 100% !important;
			}

			.stack .column {
				width: 100%;
				display: block;
			}
		}
	</style>
</head>
<body style="background-color: #f4f4f4; margin: 0; padding: 0; -webkit-text-size-adjust: none; text-size-adjust: none;">
<table border="0" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0; background-color: #f4f4f4;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-1" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0; background-color: #f4f4f4;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0; background-repeat: no-repeat; background-color: #fff; background-image: url(http://services.crib.conceptomercado.com/public/images/Iconografia_Zumar-19_5_1.png); background-position: center top;" width="600">
<tbody>
<tr>
<th class="column" style="mso-table-lspace: 0; mso-table-rspace: 0; font-weight: 400; text-align: left; vertical-align: top;" width="100%">
<table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="width:100%;padding-top:15px;padding-right:20px;padding-bottom:15px;padding-left:0px;">
<div align="right" style="line-height:10px"><img src="http://services.crib.conceptomercado.com/public/images/Logo_Zumar_Mesa_de_trabajo_1_copia_21_1.png" style="display: block; height: auto; border: 0; width: 132px; max-width: 100%;" width="132"/></div>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="heading_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="width:100%;text-align:center;padding-top:40px;padding-left:60px;">
<h1 style="margin: 0; color: #555555; font-size: 23px; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; line-height: 120%; text-align: left; direction: ltr; font-weight: normal; letter-spacing: normal; margin-top: 0; margin-bottom: 0;"><strong>Hola {{ $name }},</strong></h1>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0; word-break: break-word;" width="100%">
<tr>
<td style="padding-top:10px;padding-right:35px;padding-bottom:10px;padding-left:60px;">
<div style="font-family: Arial, sans-serif">
<div style="font-size: 14px; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; color: #575757; line-height: 1.2;">
<p style="margin: 0; font-size: 13px;"><span style="font-size:13px;">Has cambiado tu contraseña satisfactoriamente.</span></p>
<p style="margin: 0; font-size: 13px; text-align: justify;"><br/><span style="font-size:13px;">Te invitamos a que inicies sesión en nuestra APP y continúes con tus operaciones con normalidad. Cualquier consulta no dudes en comunicarte con nosotros.</span></p>
<p style="margin: 0; font-size: 13px; mso-line-height-alt: 16.8px;"><br/></p>
<p style="margin: 0; font-size: 13px;"><br/><span style="font-size:13px;">¡Gracias por usar ZUMAR!</span></p>
<p style="margin: 0; font-size: 13px;"><span style="color:#727272;font-size:13px;">El equipo de seguridad de ZUMAR</span></p>
</div>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0; word-break: break-word;" width="100%">
<tr>
<td style="padding-top:60px;padding-right:20px;padding-bottom:15px;padding-left:60px;">
<div style="font-family: sans-serif">
<div style="font-size: 14px; color: #555555; line-height: 1.2; font-family: Arial, Helvetica Neue, Helvetica, sans-serif;">
<p style="margin: 0; font-size: 14px;"><span style="font-size:12px;"><strong>¿No lo has hecho tú?</strong> Te recomendamos <span style="color:#0292b6;"><strong>cambiar tu contraseña</strong></span> inmediatamente.</span></p>
</div>
</div>
</td>
</tr>
</table>
</th>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-2" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0; background-repeat: no-repeat; background-color: #fff; background-image: url(http://services.crib.conceptomercado.com/public/images/Frame_4.png);" width="600">
<tbody>
<tr>
<th class="column" style="mso-table-lspace: 0; mso-table-rspace: 0; font-weight: 400; text-align: left; vertical-align: top; padding-left: 10px;" width="33.333333333333336%">
<table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="width:100%;padding-top:100px;padding-right:0px;padding-left:0px;">
<div align="center" style="line-height:10px"><img src="http://services.crib.conceptomercado.com/public/images/Logo_Zumar_Mesa_de_trabajo_1_copia_10_1.png" style="display: block; height: auto; border: 0; width: 96px; max-width: 100%;" width="96"/></div>
</td>
</tr>
</table>
</th>
<th class="column" style="mso-table-lspace: 0; mso-table-rspace: 0; font-weight: 400; text-align: left; vertical-align: top;" width="25%">
<table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0; word-break: break-word;" width="100%">
<tr>
<td style="padding-top:115px;padding-bottom:10px;padding-left:10px;">
<div style="font-family: sans-serif">
<div style="font-size: 14px; color: #ff9500; line-height: 1.2; font-family: Arial, Helvetica Neue, Helvetica, sans-serif;">
<p style="margin: 0; font-size: 14px;"><strong>Contáctanos</strong></p>
</div>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="icons_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="color:#0287af;text-align:center;font-family:inherit;font-size:12px;">
<table cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="text-align:center;">
<!--[if vml]><table align="left" cellpadding="0" cellspacing="0" role="presentation" style="display:inline-block;padding-left:0px;padding-right:0px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><![endif]-->
<!--[if !vml]><!-->
<table cellpadding="0" cellspacing="0" class="icons-inner" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0; display: inline-block; margin-right: -4px; padding-left: 0px; padding-right: 0px;">
<!--<![endif]-->
<tr>
<td style="text-align:center;padding-top:5px;padding-bottom:5px;padding-left:5px;padding-right:5px;"><img align="center" alt="telefono" class="icon" height="16" src="http://services.crib.conceptomercado.com/public/images/Phone_Icon_1_1.png" style="display: block; height: auto; border: 0;" width="16"/></td>
<td style="font-family:Arial, Helvetica Neue, Helvetica, sans-serif;font-size:12px;color:#0287af;vertical-align:middle;letter-spacing:undefined;text-align:center;">+51 990001146</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="icons_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="color:#0287af;text-align:center;font-family:inherit;font-size:12px;">
<table cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="text-align:center;">
<!--[if vml]><table align="left" cellpadding="0" cellspacing="0" role="presentation" style="display:inline-block;padding-left:0px;padding-right:0px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><![endif]-->
<!--[if !vml]><!-->
<table cellpadding="0" cellspacing="0" class="icons-inner" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0; display: inline-block; margin-right: -4px; padding-left: 0px; padding-right: 0px;">
<!--<![endif]-->
<tr>
<td style="text-align:center;padding-top:5px;padding-bottom:5px;padding-left:5px;padding-right:5px;"><img align="center" alt="correo" class="icon" height="16" src="http://services.crib.conceptomercado.com/public/images/Mail_Icon_1_1.png" style="display: block; height: auto; border: 0;" width="23"/></td>
<td style="font-family:Arial, Helvetica Neue, Helvetica, sans-serif;font-size:12px;color:#0287af;vertical-align:middle;letter-spacing:undefined;text-align:center;">mario@zumar.pe</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</th>
<th class="column" style="mso-table-lspace: 0; mso-table-rspace: 0; font-weight: 400; text-align: left; vertical-align: top;" width="41.666666666666664%">
<table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0; word-break: break-word;" width="100%">
<tr>
<td style="padding-top:75px;padding-right:15px;padding-bottom:15px;padding-left:15px;">
<div style="font-family: sans-serif">
<div style="font-size: 12px; color: #555555; line-height: 1.2; font-family: Arial, Helvetica Neue, Helvetica, sans-serif;">
<p style="margin: 0; font-size: 12px; mso-line-height-alt: 14.399999999999999px;"><br/></p>
</div>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="icons_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="text-align:right;padding-right:45px;font-family:inherit;font-size:13px;padding-bottom:5px;">
<table align="right" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;">
<tr>
<td style="text-align:center;padding-top:60px;padding-bottom:0px;padding-left:0px;padding-right:0px;"><a href="https://www.facebook.com/zumar.peru"><img align="center" class="icon" height="32" src="http://services.crib.conceptomercado.com/public/images/REDES-09_3.png" style="display: block; height: auto; border: 0;" width="41"/></a></td>
<td style="text-align:center;padding-top:60px;padding-bottom:0px;padding-left:0px;padding-right:0px;"><a href="https://www.instagram.com/zumar.pe/"><img align="center" class="icon" height="32" src="http://services.crib.conceptomercado.com/public/images/REDES-10_3.png" style="display: block; height: auto; border: 0;" width="37"/></a></td>
<td style="text-align:center;padding-top:60px;padding-bottom:0px;padding-left:0px;padding-right:0px;"><a href="https://www.linkedin.com/company/zumar"><img align="center" class="icon" height="32" src="http://services.crib.conceptomercado.com/public/images/REDES-11_3.png" style="display: block; height: auto; border: 0;" width="41"/></a></td>
<td style="text-align:center;padding-top:60px;padding-bottom:0px;padding-left:0px;padding-right:0px;"><a href="http://wa.me/+51939862191"><img align="center" class="icon" height="32" src="http://services.crib.conceptomercado.com/public/images/REDES-12_3.png" style="display: block; height: auto; border: 0;" width="35"/></a></td>
</tr>
</table>
</td>
</tr>
</table>
</th>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-3" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table><!-- End -->
</body>
</html>
