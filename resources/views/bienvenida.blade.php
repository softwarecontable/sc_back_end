<!DOCTYPE html>

<html lang="en" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
<title></title>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!--[if mso]><xml><o:OfficeDocumentSettings><o:PixelsPerInch>96</o:PixelsPerInch><o:AllowPNG/></o:OfficeDocumentSettings></xml><![endif]-->
<!--[if !mso]><!-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
<!--<![endif]-->
<style>
		* {
			box-sizing: border-box;
		}

		th.column {
			padding: 0
		}

		a[x-apple-data-detectors] {
			color: inherit !important;
			text-decoration: inherit !important;
		}

		#MessageViewBody a {
			color: inherit;
			text-decoration: none;
		}

		p {
			line-height: inherit
		}

		@media (max-width:520px) {
			.icons-inner {
				text-align: center;
			}

			.icons-inner td {
				margin: 0 auto;
			}

			.row-content {
				width: 100% !important;
			}

			.image_block img.big {
				width: auto !important;
			}

			.stack .column {
				width: 100%;
				display: block;
			}
		}
	</style>
</head>
<body style="background-color: #f4f4f4; margin: 0; padding: 0; -webkit-text-size-adjust: none; text-size-adjust: none;">
<table border="0" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0; background-color: #f4f4f4;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-1" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0; background-color: #fff; background-repeat: no-repeat; background-position: center top; background-image: url(http://services.crib.conceptomercado.com/public/images/encabezado.png);" width="500">
<tbody>
<tr>
<th class="column" style="mso-table-lspace: 0; mso-table-rspace: 0; font-weight: 400; text-align: left; vertical-align: top;" width="100%">
<table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="width:100%;padding-right:45px;padding-left:0px;padding-bottom:5px;">
<div align="right" style="line-height:10px"><img src="http://services.crib.conceptomercado.com/public/images/Depositphotos_179439370_xl-2015_1.png" style="display: block; height: auto; border: 0; width: 182px; max-width: 100%;" width="182"/></div>
</td>
</tr>
</table>
</th>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-2" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0; background-color: #fff;" width="500">
<tbody>
<tr>
<th class="column" style="mso-table-lspace: 0; mso-table-rspace: 0; font-weight: 400; text-align: left; vertical-align: top;" width="41.666666666666664%">
<table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="width:100%;padding-top:30px;padding-right:0px;padding-left:0px;padding-bottom:5px;">
<div align="center" style="line-height:10px"><img src="http://services.crib.conceptomercado.com/public/images/Logo_Zumar_Mesa_de_trabajo_1_copia_10_1.png" style="display: block; height: auto; border: 0; width: 96px; max-width: 100%;" width="96"/></div>
</td>
</tr>
</table>
</th>
<th class="column" style="mso-table-lspace: 0; mso-table-rspace: 0; font-weight: 400; text-align: left; vertical-align: top; padding-right: 55px;" width="58.333333333333336%">
<table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="width:100%;padding-right:0px;padding-left:0px;padding-top:15px;">
<div align="center" style="line-height:10px"><img src="http://services.crib.conceptomercado.com/public/images/Bienvenido.png" style="display: block; height: auto; border: 0; width: 217px; max-width: 100%;" width="217"/></div>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0; word-break: break-word;" width="100%">
<tr>
<td style="padding-top:10px;padding-right:10px;padding-bottom:25px;padding-left:10px;">
<div style="font-family: Arial, sans-serif">
<div style="font-size: 14px; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; color: #626262; line-height: 1.2;">
<p style="margin: 0; font-size: 14px; text-align: right;">Descubre más de Zumar,<br/>el primer aplicativo peruano<br/>que de forma digital te ayudará<br/>en tu gestión contable.</p>
</div>
</div>
</td>
</tr>
</table>
</th>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-3" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0; background-color: #fff; background-repeat: no-repeat; background-position: center top; background-image: url(http://services.crib.conceptomercado.com/public/images/Frame_2.png);" width="500">
<tbody>
<tr>
<th class="column" style="mso-table-lspace: 0; mso-table-rspace: 0; font-weight: 400; text-align: left; vertical-align: top;" width="100%">
<table border="0" cellpadding="0" cellspacing="0" class="heading_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="width:100%;text-align:center;padding-top:120px;padding-bottom:15px;">
<h1 style="margin: 0; color: #ffffff; font-size: 20px; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; line-height: 120%; text-align: center; direction: ltr; font-weight: normal; letter-spacing: normal; margin-top: 0; margin-bottom: 0;"><strong>Nuestros Servicios</strong></h1>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="width:100%;padding-right:0px;padding-left:0px;">
<div align="center" style="line-height:10px"><img class="big" src="http://services.crib.conceptomercado.com/public/images/Line_63.png" style="display: block; height: auto; border: 0; width: 500px; max-width: 100%;" width="500"/></div>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="icons_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="text-align:center;padding-top:20px;font-family:inherit;font-size:14px;">
<table align="center" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;">
<tr>
<td style="text-align:center;padding-top:5px;padding-bottom:5px;padding-left:5px;padding-right:5px;"><img align="center" class="icon" height="64" src="http://services.crib.conceptomercado.com/public/images/Group_1180.png" style="display: block; height: auto; border: 0;" width="70"/></td>
<td style="text-align:center;padding-top:5px;padding-bottom:5px;padding-left:5px;padding-right:5px;"><img align="center" class="icon" height="64" src="http://services.crib.conceptomercado.com/public/images/Group_1181.png" style="display: block; height: auto; border: 0;" width="78"/></td>
<td style="text-align:center;padding-top:5px;padding-bottom:5px;padding-left:5px;padding-right:5px;"><img align="center" class="icon" height="64" src="http://services.crib.conceptomercado.com/public/images/Group_1182.png" style="display: block; height: auto; border: 0;" width="69"/></td>
<td style="text-align:center;padding-top:5px;padding-bottom:5px;padding-left:5px;padding-right:5px;"><img align="center" class="icon" height="64" src="http://services.crib.conceptomercado.com/public/images/Group_1183.png" style="display: block; height: auto; border: 0;" width="70"/></td>
<td style="text-align:center;padding-top:5px;padding-bottom:5px;padding-left:5px;padding-right:5px;"><img align="center" class="icon" height="64" src="http://services.crib.conceptomercado.com/public/images/Group_1186.png" style="display: block; height: auto; border: 0;" width="63"/></td>
</tr>
</table>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="icons_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="text-align:center;padding-bottom:15px;font-family:inherit;font-size:14px;">
<table align="center" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;">
<tr>
<td style="text-align:center;padding-top:5px;padding-bottom:5px;padding-left:5px;padding-right:5px;"><img align="center" class="icon" height="64" src="http://services.crib.conceptomercado.com/public/images/Group_1179.png" style="display: block; height: auto; border: 0;" width="68"/></td>
<td style="text-align:center;padding-top:5px;padding-bottom:5px;padding-left:5px;padding-right:5px;"><img align="center" class="icon" height="64" src="http://services.crib.conceptomercado.com/public/images/Group_1188.png" style="display: block; height: auto; border: 0;" width="68"/></td>
<td style="text-align:center;padding-top:5px;padding-bottom:5px;padding-left:5px;padding-right:5px;"><img align="center" class="icon" height="64" src="http://services.crib.conceptomercado.com/public/images/Group_1187.png" style="display: block; height: auto; border: 0;" width="69"/></td>
<td style="text-align:center;padding-top:5px;padding-bottom:5px;padding-left:5px;padding-right:5px;"><img align="center" class="icon" height="64" src="http://services.crib.conceptomercado.com/public/images/Group_1185.png" style="display: block; height: auto; border: 0;" width="65"/></td>
</tr>
</table>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="width:100%;padding-bottom:25px;padding-right:0px;padding-left:0px;">
<div align="center" style="line-height:10px"><a href="https://www.zumar.pe" style="outline:none" tabindex="-1" target="_blank"><img src="http://services.crib.conceptomercado.com/public/images/ebee38b5-e944-4a7c-86cb-b5a31b7c0566.png" style="display: block; height: auto; border: 0; width: 76px; max-width: 100%;" width="76"/></a></div>
</td>
</tr>
</table>
</th>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-4" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0; background-color: #fff; background-repeat: no-repeat; background-position: center top; background-image: url(http://services.crib.conceptomercado.com/public/images/Iconografia_Zumar-19_5.png);" width="500">
<tbody>
<tr>
<th class="column" style="mso-table-lspace: 0; mso-table-rspace: 0; font-weight: 400; text-align: left; vertical-align: top; padding-left: 20px;" width="50%">
<table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0; word-break: break-word;" width="100%">
<tr>
<td style="padding-top:15px;padding-right:10px;padding-bottom:10px;padding-left:50px;">
<div style="font-family: Arial, sans-serif">
<div style="font-size: 14px; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; color: #555555; line-height: 1.2;">
<p style="margin: 0; font-size: 12px;"><strong><span style="color:#ffffff;font-size:14px;">Contáctanos</span></strong></p>
<p style="margin: 0; font-size: 12px; mso-line-height-alt: 16.8px;"><br/></p>
<p style="margin: 0; font-size: 12px;"><span style="color:#ffffff;font-size:12px;">Cel. 990 001 146</span></p>
<p style="margin: 0; font-size: 12px;"><span style="color:#ffffff;font-size:12px;">e-mail : mario@zumar.pe</span></p>
</div>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="icons_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="text-align:left;padding-left:40px;font-family:inherit;font-size:14px;">
<table align="left" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;">
<tr>
<td style="text-align:center;padding-top:0px;padding-bottom:0px;padding-left:0px;padding-right:0px;"><a href="https://www.facebook.com/zumar.peru"><img align="center" class="icon" height="32" src="http://services.crib.conceptomercado.com/public/images/REDES-09_3.png" style="display: block; height: auto; border: 0;" width="41"/></a></td>
<td style="text-align:center;padding-top:0px;padding-bottom:0px;padding-left:0px;padding-right:0px;"><a href="https://www.instagram.com/zumar.pe/"><img align="center" class="icon" height="32" src="http://services.crib.conceptomercado.com/public/images/REDES-10_3.png" style="display: block; height: auto; border: 0;" width="37"/></a></td>
<td style="text-align:center;padding-top:0px;padding-bottom:0px;padding-left:0px;padding-right:0px;"><a href="https://www.linkedin.com/company/zumar"><img align="center" class="icon" height="32" src="http://services.crib.conceptomercado.com/public/images/REDES-11_3.png" style="display: block; height: auto; border: 0;" width="41"/></a></td>
<td style="text-align:center;padding-top:0px;padding-bottom:0px;padding-left:0px;padding-right:0px;"><a href="http://wa.me/+51939862191"><img align="center" class="icon" height="32" src="http://services.crib.conceptomercado.com/public/images/REDES-12_3.png" style="display: block; height: auto; border: 0;" width="35"/></a></td>
</tr>
</table>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="width:100%;padding-top:35px;padding-bottom:15px;padding-left:15px;padding-right:0px;">
<div style="line-height:10px"><img src="http://services.crib.conceptomercado.com/public/images/Logo_Zumar_Mesa_de_trabajo_1_copia_21_1_1.png" style="display: block; height: auto; border: 0; width: 79px; max-width: 100%;" width="79"/></div>
</td>
</tr>
</table>
</th>
<th class="column" style="mso-table-lspace: 0; mso-table-rspace: 0; font-weight: 400; text-align: left; vertical-align: top; padding-left: 15px; padding-right: 30px;" width="50%">
<table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0; word-break: break-word;" width="100%">
<tr>
<td style="padding-top:15px;padding-right:10px;padding-bottom:10px;padding-left:30px;">
<div style="font-family: Arial, sans-serif">
<div style="font-size: 14px; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; color: #555555; line-height: 1.2;">
<p style="margin: 0; font-size: 14px;"><strong><span style="color:#ffffff;">Descarga la app</span></strong></p>
</div>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="width:100%;padding-right:60px;padding-left:25px;">
<div style="line-height:10px"><a href="https://play.google.com/store/apps/details?id=com.crib.capital" style="outline:none" tabindex="-1" target="_blank"><img src="http://services.crib.conceptomercado.com/public/images/google-play-badge_LA.png" style="display: block; height: auto; border: 0; width: 120px; max-width: 100%;" width="120"/></a></div>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="width:100%;padding-right:60px;padding-left:25px;padding-bottom:5px;">
<div align="center" style="line-height:10px"><a href="https://apps.apple.com/app/id1540656572" style="outline:none" tabindex="-1" target="_blank"><img src="http://services.crib.conceptomercado.com/public/images/app-store.png" style="display: block; height: auto; border: 0; width: 120px; max-width: 100%;" width="120"/></a></div>
</td>
</tr>
</table>
</th>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-5" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0; background-color: #fff;" width="500">
<tbody>
<tr>
<th class="column" style="mso-table-lspace: 0; mso-table-rspace: 0; font-weight: 400; text-align: left; vertical-align: top;" width="100%">
<table border="0" cellpadding="0" cellspacing="0" class="icons_block" role="presentation" style="mso-table-lspace: 0; mso-table-rspace: 0;" width="100%">
<tr>
<td style="color:#9d9d9d;font-family:inherit;font-size:15px;padding-bottom:10px;padding-top:10px;text-align:center;">
</td>
</tr>
</table>
</th>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table><!-- End -->
</body>
</html>
