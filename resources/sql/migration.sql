ALTER TABLE dbZumarV1.sale_report_detail
ADD `nominalDiscount` DECIMAL(10,2) NOT NULL DEFAULT '0.0' AFTER `mtoBaseIgv`
, ADD `nominalTotal` DECIMAL(10,2) NOT NULL DEFAULT '0.0' AFTER `nominalDiscount`
, ADD `baseAmount` DECIMAL(10,2) NOT NULL DEFAULT '0.0' AFTER `nominalTotal`;
