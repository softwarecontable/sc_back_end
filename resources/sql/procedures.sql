DELIMITER $$
--
-- Queries
--

-- Fill `sale_payment` table from `sale_report`
INSERT INTO dbZumarV1.sale_payment ('report_id', 'payment_id', 'amount')
SELECT id, payment_type, subTotal FROM dbZumarV1.sale_report;

-- Drop column `payment_type` from `sale_payment`
ALTER TABLE dbZumarV1.sale_report DROP payment_type;

DELIMITER ;

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURdE IF EXISTS dbZumarV1.sp_getMovementsBackOffice;
CREATE DEFINER=`zumar_usr_db`@`localhost` PROCEDURE dbZumarV1.sp_getMovementsBackOffice (IN `bank_id` INT, IN `fechaInit` VARCHAR(10), IN `fechaEnd` VARCHAR(10))   if fechaInit is not null and fechaEnd is not null then
		select bank_movements.*, sum(movements_voucher.amount) as amountSustentado  from bank_movements
		left join movements_voucher on bank_movements.id = movements_voucher.id_bank_movements
		where bank_movements.bank_id = bank_id
		and bank_movements.date between fechaInit and fechaEnd
		group by movements_voucher.id_bank_movements
		order by bank_movements.date;
    else
		select bank_movements.*, sum(movements_voucher.amount) as amountSustentado  from bank_movements
		left join movements_voucher on bank_movements.id = movements_voucher.id_bank_movements
		where bank_movements.bank_id = bank_id
		group by movements_voucher.id_bank_movements
		order by bank_movements.date;
	end if$$

DROP PROCEDURdE IF EXISTS dbZumarV1.sp_get_all_productos_by_company;
CREATE DEFINER=`zumar_usr_db`@`localhost` PROCEDURE dbZumarV1.sp_get_all_productos_by_company (IN `id_company` INT)  NO SQL select segmento.codigo as codigosegmento, segmento.descripcion as descripcionsegmento, segmento.id_segmento as idsegmento,
	familia.codigo as codigofamilia, familia.descripcion as descripcionfamilia, familia.id_familia as idfamilia,
	clase.codigo as codigoclase, clase.descripcion as descripcionclase, clase.id_clase as idclase,
	producto.*, inventario.cantidad_inicial, inventario.cantidad_minima, inventario.cantidad_actual
	from segmento
	inner join familia on segmento.codigo = familia.id_segmento
	inner join clase on clase.familia_id = familia.codigo
	inner join producto on producto.categoria_id = clase.id_clase
    left join inventario on producto.id = inventario.producto_id
	where producto.company_id = id_company
    and producto.estado = 1
order by producto.id$$

DROP PROCEDURdE IF EXISTS dbZumarV1.sp_get_catalog;
CREATE DEFINER=`zumar_usr_db`@`localhost` PROCEDURE dbZumarV1.sp_get_catalog ()   SELECT sc_catalog.*, sc_group_catalog.descripcion as grupo FROM sc_catalog
	INNER JOIN sc_group_catalog ON sc_catalog.id_grupo = sc_group_catalog.id
	ORDER BY sc_catalog.id$$

DROP PROCEDURdE IF EXISTS dbZumarV1.sp_get_collection;
CREATE DEFINER=`zumar_usr_db`@`localhost` PROCEDURE dbZumarV1.sp_get_collection (IN `company_id` INT(10), IN `today` VARCHAR(10))   SELECT
    sc.descripcion
    , sr.cantidad as cantidad_actual
    , sr.total as monto
FROM sc_catalog as sc
left JOIN (
    select
    	sr.payment_type
    	, count(*) as cantidad
    	, SUM(sr.subTotal) as total
    from sale_report as sr
    WHERE
    	 Date(sr.created_at) = today
    	 and sr.company_id = company_id
    GROUP BY sr.payment_type
) as sr ON sr.payment_type = sc.id
where
	sc.id_grupo = 16$$

DROP PROCEDURdE IF EXISTS dbZumarV1.sp_get_collection_date;
CREATE DEFINER=`zumar_usr_db`@`localhost` PROCEDURE dbZumarV1.sp_get_collection_date (IN `company_id` INT(10), IN `today` DATE, IN `beginning` DATE)   SELECT
    sc.descripcion
    , sr.cantidad as cantidad_actual
    , sr.total as monto
FROM sc_catalog as sc
left JOIN (
    select
    	sp.payment_id
    	, count(*) as cantidad
    	, SUM(sp.amount) as total
    from sale_report as sr
    JOIN sale_payment as sp on sp.report_id = sr.id
    WHERE
    	 Date(sr.created_at) <= today
    	 and Date(sr.created_at) >= beginning
    	 and sr.company_id = company_id
    	 and sr.tipoDoc in (1, 3, 99)
    GROUP BY sp.payment_id
) as sr ON sr.payment_id = sc.id
where
	sc.id_grupo = 16$$

DROP PROCEDURdE IF EXISTS dbZumarV1.sp_get_group_catalog;
CREATE DEFINER=`zumar_usr_db`@`localhost` PROCEDURE dbZumarV1.sp_get_group_catalog ()   SELECT * FROM sc_group_catalog
	ORDER BY sc_group_catalog.descripcion$$

DROP PROCEDURdE IF EXISTS dbZumarV1.sp_get_sale_by_date;
CREATE DEFINER=`zumar_usr_db`@`localhost` PROCEDURE dbZumarV1.sp_get_sale_by_date (IN `company_id` INT(10), IN `today` DATE)   SELECT
    sc.description as comprobante
    , sr.cantidad
    , sr.total
FROM sale_catalog as sc
left JOIN (
    select
    	sr.tipoDoc
    	, count(*) as cantidad
    	, SUM(sr.subTotal) as total
    from sale_report as sr
    WHERE
    	 Date(sr.created_at) = today
    	 and sr.company_id = company_id
    GROUP BY sr.tipoDoc
) as sr ON sr.tipoDoc = sc.label
where
	sc.id_group = 1
    and sc.value < 2$$

DROP PROCEDURdE IF EXISTS dbZumarV1.sp_get_sale_by_range_date;
CREATE DEFINER=`zumar_usr_db`@`localhost` PROCEDURE dbZumarV1.sp_get_sale_by_range_date (IN `company_id` INT(10), IN `today` DATE, IN `beginning` DATE)   SELECT
    sc.description as comprobante
    , sr.cantidad
    , sr.total
FROM sale_catalog as sc
left JOIN (
    select
    	sr.tipoDoc
    	, count(*) as cantidad
    	, SUM(sr.subTotal) as total
    from sale_report as sr
    WHERE
    	 Date(sr.created_at) <= today
    	 and Date(sr.created_at) >= beginning
    	 and sr.company_id = company_id
    	 and sr.tipoDoc in (1, 3, 99)
    GROUP BY sr.tipoDoc
) as sr ON sr.tipoDoc = sc.label
where
	sc.id_group = 1
    and sc.value < 2$$

DROP PROCEDURdE IF EXISTS dbZumarV1.sp_get_voucher_ins;
CREATE DEFINER=`zumar_usr_db`@`localhost` PROCEDURE dbZumarV1.sp_get_voucher_ins (IN `id` INT(10), IN `today` VARCHAR(10), IN `yesterday` VARCHAR(10), IN `firstDay` VARCHAR(10))   select
	sum(dates.amount) as amount
    , db.description
    , UNIX_TIMESTAMP(DATE_FORMAT(db.date, "%y-%m-%d")) date
    , DATE_FORMAT(db.date, "%d/%m") label
from (
    select * from (
        select "Ingresos" as description, firstDay as date
        union all select "Ingresos", yesterday
        union all select "Ingresos", today
	) as db
) as db
left join (
    select
		sum(v.amount) amount
    	, DATE_FORMAT(v.created_at, "%Y-%m-%d") date
	from voucher as v
    	inner join spending as s on s.id = v.id_spending
    	inner join voucher_categories as vc on vc.id = s.id_voucher_categories
	where
		vc.company_id = id
    	and vc.type = 2
    	and DATE(v.created_at) between firstDay and today
	group by TIMESTAMP(v.created_at)
) dates on DATE(dates.date) = DATE(db.date)
group by date
order by date$$

DROP PROCEDURdE IF EXISTS dbZumarV1.sp_get_voucher_movements;
CREATE DEFINER=`zumar_usr_db`@`localhost` PROCEDURE dbZumarV1.sp_get_voucher_movements ()   select *, sum(amount) as totalAmount from movements_voucher
	where state = 1
	group by id_voucher$$

DROP PROCEDURdE IF EXISTS dbZumarV1.sp_get_voucher_output;
CREATE DEFINER=`zumar_usr_db`@`localhost` PROCEDURE dbZumarV1.sp_get_voucher_output (IN `id` INT(10), IN `today` VARCHAR(10), IN `yesterday` VARCHAR(10), IN `firstDay` VARCHAR(10))   select
	sum(dates.amount) as amount
    , db.description
    , UNIX_TIMESTAMP(DATE_FORMAT(db.date, "%y-%m-%d")) date
    , DATE_FORMAT(db.date, "%d/%m") label
from (
    select * from (
        select "Egresos" as description, firstDay as date
        union all select "Egresos", yesterday
        union all select "Egresos", today
	) as db
) as db
left join (
    select
		sum(v.amount) amount
    	, DATE_FORMAT(v.created_at, "%Y-%m-%d") date
	from voucher as v
    	inner join spending as s on s.id = v.id_spending
    	inner join voucher_categories as vc on vc.id = s.id_voucher_categories
	where
		vc.company_id = id
    	and vc.type = 1
    	and DATE(v.created_at) between firstDay and today
	group by TIMESTAMP(v.created_at)
) dates on DATE(dates.date) = DATE(db.date)
group by date
order by date$$

DELIMITER ;
