<?php

namespace App\Console\Commands;

use App\Http\Controllers\EncryptionController;
use App\Http\Controllers\GreenterController;
use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;

class SunatSummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sunat:summary';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "In this task the server will send all the vouchers generated electronically to Sunat end points.";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //$enc = new EncryptionController;
        //$greenter = new GreenterController($enc);
        Log::channel('summary')->info("DB url: ".env('DATABASE_URL'));
    }
}
