<?php

namespace App\Imports;

use App\Models\PlanCuentas;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class PlanCuentasImport implements ToCollection {
    private $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function collection(Collection $rows) {
        for ($i = 2; $i < count($rows); $i++) {
            PlanCuentas::create([
                'codigo' => $rows[$i][0],
                'descripcion' => $rows[$i][1],
                'company_id' => $this->data['company_id']
            ]);
        }
    }
}