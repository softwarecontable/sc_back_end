<?php

namespace App\Imports;

use App\Models\BankMovement;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class BankScotiaMovementImport implements ToCollection
{
    private $data;
    private $arrayPreview = [];

    public function __construct($data) {
        $this->data = $data;
    }

    public function collection(Collection  $rows) {
        for ($i = 8; $i < count($rows); $i++) {
            $element = [
                'date' => $rows[$i][0],
                'concept' => $rows[$i][2],
                'amount' => $rows[$i][3],
                'bank_id' => $this->data['bank_id'],
                'state' => 1
            ];
            array_push($this->arrayPreview, $element);
        }
    }

    public function responseArray() {
        return $this->arrayPreview;
    }
}