<?php

namespace App\Imports;

use App\Models\BankMovement;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class BankMovementImport implements ToCollection
{
    private $data;
    private $arrayPreview = [];

    public function __construct($data) {
        $this->data = $data;
    }

    public function collection(Collection $rows)
    {
        for ($i = 1; $i < count($rows); $i++) {
            $element = [
                'date' => $rows[$i][0],
                'concept' => $rows[$i][1],
                'amount' => $rows[$i][2],
                'bank_id' => $this->data['bank_id'],
                'state' => 1
            ];
            array_push($this->arrayPreview, $element);
        }
    }

    public function responseArray() {
        return $this->arrayPreview;
    }
}
