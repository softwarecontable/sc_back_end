<?php

namespace App\Imports;

use App\Models\Producto;
use App\Models\Inventario;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use App\Models\InventarioMovimiento;
use Maatwebsite\Excel\Concerns\ToCollection;

class ProductImport implements ToCollection
{
    private $data;
    private $index = 1;
    //private $arrayPreview = [];

    public function __construct($data) {
        $this->data = $data;
    }

    public function collection(Collection  $rows) {
        if ($this->index > 0) {
            for ($i = 1; $i < count($rows); $i++) {
                $item = new Producto;
                $item->categoria_id = $rows[$i][0];
                $item->nombre = $rows[$i][1];
                $item->precio_venta = $rows[$i][2];
                $item->impuesto = $rows[$i][3];
                $item->unidad_medida = $rows[$i][4];
                $item->sunat_id = $rows[$i][5];
                $item->company_id = $this->data['company_id'];
                $item->estado = 1;
                $item->cuenta_contable = 0;
                $item->save();

                if (!empty($rows[$i][6]) && intval($rows[$i][6]) > 0) {
                  $inv = new Inventario;
                  $inv->producto_id = $item->id;
                  $inv->cantidad_inicial = intval($rows[$i][6]);
                  $inv->cantidad_actual = intval($rows[$i][6]);
                  $inv->cantidad_minima = intval($rows[$i][7]);
                  $inv->cantidad_maxima = intval($rows[$i][8]);
                  $inv->company_id = $this->data['company_id'];
                  $inv->save();

                  $invMov = new InventarioMovimiento;
                  $invMov->producto_id = $item->id;
                  $invMov->inventario_id = $inv->id;
                  $invMov->cantidad = intval($rows[$i][6]);
                  $invMov->save();
                }
            }
        }
        $this->index = $this->index - 1;
    }

    //public function responseArray() {
    //    return $this->arrayPreview;
    //}
}
