<?php

namespace App\Imports;

use App\Models\BankMovement;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class BankBbvaMovementImport implements ToCollection
{
    private $data;
    private $arrayPreview = [];

    public function __construct($data) {
        $this->data = $data;
    }

    public function collection(Collection  $rows) {
        for ($i = 4; $i < count($rows); $i++) {
            if ($rows[$i][2]) {
                $element = [
                    'date' => $rows[$i][2],
                    'concept' => $rows[$i][4],
                    'amount' => $rows[$i][10],
                    'bank_id' => $this->data['bank_id'],
                    'state' => 1
                ];
                array_push($this->arrayPreview, $element);
            }
        }
    }

    public function responseArray() {
        return $this->arrayPreview;
    }
}