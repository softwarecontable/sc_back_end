<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Dompdf\Dompdf;
use Dompdf\Options;
use Luecano\NumeroALetras\NumeroALetras;

class PdfController extends Controller
{

    public const TICKET = 0;
    public const DOCUMENT = 1;

    protected EncryptionController $encrypt;
    protected QrCodeController $qrcode;
    protected string $zumarLogo;

    public function __construct(EncryptionController $encrypt, QrCodeController $qrcode) {
        $this->encrypt = $encrypt;
        $this->qrcode = $qrcode;
        $this->zumarLogo = "Logo_Zumar_Mesa_de_trabajo_1_copia_10_1.png";
    }

    public function test(Request $request) {
        $fileName = "/test.pdf";

        $address = new \stdClass();
        $address->direccion = 'Calle Galileo Galilei 153 La Molina';

        $company = new \stdClass();
        $company->address = $address;
        $company->nombreComercial = NULL;
        $company->razonSocial = 'Fábrica de Software Sacome';
        $company->name = 'Walter Santiago Cojal Medina';
        $company->commercial_name = 'Fábrica de Software Sacome';
        $company->ruc = '10469806770';
        $company->logo = 'Logo_Zumar_Mesa_de_trabajo_1_copia_10_1.png';

        $user = new \stdClass();
        $user->cell_phone = '991988248';
        $user->email = 'walter_cojal@hotmail.com';

        $qr = $this->qrcode->billQrCode($fileName);
        $logo = asset('images/Logo_Zumar_Mesa_de_trabajo_1_copia_10_1.png');
        $saveUrl = public_path('img') .$fileName;
        return view('invoice', ['request' => $request, 'company' => $company, 'qrcode' => $qr, 'logo' => $logo, 'user' => $user]);

        // $options = new Options();
        // $options->set('isHtml5ParserEnabled', true);
        // $options->set('isRemoteEnabled', true);
        // $options->set('encoding', 'UTF-8');

        // $pdf = new Dompdf($options);
        // $pdf->loadHtml($view);
        // $pdf->setPaper("A4", "portrait");
        // $pdf->render();

        // $content = $pdf->output();
        // file_put_contents($saveUrl, $content);

        // return asset('img') .$fileName;
    }

    /**
     * @method Response showBill()
     * @param string $url es el url del documento generado
     */
    public function showBill(string $url) {
        $_url = $this->encrypt->decrypt($url);
        $filename = basename($_url);

        $content = Storage::disk("public_uploads")->get($_url);
        return Response::make($content, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$filename.'"'
        ]);
    }

    /**
     * @method createTicket genera el comprobante en tipo TICKET
     * @param string $filename es el nombre del archivo del comprobante.
     * @param Request $request contiene la información monetaria del comprobante.
     * @param Company $company trae toda la información de la compañía generadora.
     * @return string con la dirección web del comprobante.
     */
    public function createTicket(string $fileName, Request $request, Company $company): string {
        $qr = $this->qrcode->billQrCode($fileName);
        $referenceWidth = 640;
        $company->logo = asset('img/' . $company->logo);

        $view = view('ticket', ['request' => $request, 'company' => $company, 'qrcode' => $qr]);
        $saveUrl = public_path('img') .$fileName;
        $options = new Options();
        $options->set('isFontSubsettingEnabled', true);
        $options->set('isHtml5ParserEnabled', true);
        $options->set('isRemoteEnabled', true);
        $options->set('encoding', 'UTF-8');

        $pdf = new Dompdf($options);
        $pdf->setPaper([0,0,$referenceWidth,2000]);
        $pdf->loadHtml($view);

        $GLOBALS['bodyHeight'] = 0;

        $pdf->setCallbacks([
            'myCallbacks' => [
                'event' => 'end_frame', 'f' => function ($frame) {
                    $node = $frame->get_node();
                    if (strtolower($node->nodeName) === "body") {
                        $padding_box = $frame->get_padding_box();
                        $GLOBALS['bodyHeight'] += $padding_box['h'];
                    }
                }
            ]
        ]);

        $pdf->render();
        unset($pdf);

        $docHeight = $GLOBALS['bodyHeight'] + 100;

        $pdf = new Dompdf($options);
        $pdf->setPaper([0,0,$referenceWidth, $docHeight]);
        $pdf->loadHtml($view);
        $pdf->render();

        file_put_contents($saveUrl, $pdf->output());
        return asset('img') .$fileName;
    }

    /**
     * @method createA4Ticket genera el comprobante en tipo DOCUMENTO A4
     * @param string $filename es el nombre del archivo del comprobante.
     * @param Request $request contiene la información monetaria del comprobante.
     * @param Company $company trae toda la información de la compañía generadora.
     * @return string con la dirección web del comprobante.
     */
    public function createA4Ticket(string $fileName, Request $request, Company $company): string {
        $formatter = new NumeroALetras();
        $request->legends = [["value" => $formatter->toInvoice($request->subTotal, 2, "SOLES")]];
        $qr = $this->qrcode->billQrCode($fileName);
        $company->logo = asset('img/' . $company->logo);
        $logo = asset("images/" . $this->zumarLogo);
        $user = DB::table('users as u')
            ->join('user_company as uc', 'uc.user_id', '=', 'u.id')
            ->where('uc.company_id', $company->id)
            ->select('u.*')
            ->first();
        $request->payment_detail = DB::table('sc_catalog')
            ->where('id_grupo', 16)
            ->where('id', $request->payment_type[0]['id'])
            ->first()
            ->descripcion;

        $view = view('invoice', ['request' => $request, 'company' => $company, 'qrcode' => $qr, 'logo' => $logo, 'user' => $user]);
        $saveUrl = public_path('img') .$fileName;

        $options = new Options();
        $options->set('isHtml5ParserEnabled', true);
        $options->set('isRemoteEnabled', true);
        $options->set('encoding', 'UTF-8');

        $pdf = new Dompdf($options);
        $pdf->loadHtml($view);
        $pdf->setPaper("A4", "portrait");
        $pdf->render();

        $content = $pdf->output();
        file_put_contents($saveUrl, $content);

        return asset('img') .$fileName;
    }

}
