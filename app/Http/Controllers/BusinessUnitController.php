<?php

namespace App\Http\Controllers;

use App\Models\BusinessUnit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BusinessUnitController extends Controller
{
    public function store(Request $request) {
        $input = $request->all();
        $businessUnit = DB::table('unidad_negocio')->where('id', '=', $input['id'])->get();
        if (count($businessUnit) > 0) {
            unset($input['name']);
            $businessUnit = DB::table('unidad_negocio')->where('id', '=', $input['id'])->update($input);
        } else {
            unset($input['id']);
            $businessUnit = BusinessUnit::create($input);
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $businessUnit
            ],
            'message' => 'Item creado correctamente'
        ]);
    }

    public function list(Request $request) {
        $input = $request->all();
        $query = DB::table('unidad_negocio')
        ->join('company', 'unidad_negocio.company_id', '=', 'company.id')
        ->where('unidad_negocio.company_id', '=', $input['company_id'])
        ->select('unidad_negocio.*', 'company.name');

        if (isset($input['codigo'])) {
            $query->where('codigo', '=', $input['codigo']);
        }
        $businessUnits = $query->get();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $businessUnits
            ],
            'message' => 'Consultado correctamente'
        ]);
    }

    public function delete(Request $request) {
        $input = $request->all();
        $businessUnit = DB::table('unidad_negocio')->where('id', '=', $input['id'])->delete();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $businessUnit
            ],
            'message' => 'Eliminado correctamente'
        ]);
    }
}
