<?php

namespace App\Http\Controllers;

use App\Models\UserCompanyPlan;
use Illuminate\Http\Request;

class UserCompanyPlanController extends Controller
{
    public function store(Request $request) {
        $input = $request->all();

        $user_company_plan = UserCompanyPlan::create($input);
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $user_company_plan
            ],
            'message' => 'Plan guardado correctamente'
        ]);
    }
}
