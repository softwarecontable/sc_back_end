<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\CompanyBank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BankController extends Controller
{
    public function store(Request $request) {
        $input = $request->all();
        $bank = Bank::create($input);
        $input['bank_id'] = $bank->id;
        $input['state'] = 1;
        $company_bank = CompanyBank::create($input);
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $bank
            ],
            'message' => 'Banco creado correctamente'
        ]);
    }

    public function list(Request $request) {
        $input = $request->all();
        $bank = DB::table('banks')
        ->leftjoin('sc_catalog', 'banks.bank_catalog_id', '=', 'sc_catalog.id')
        ->leftjoin('sc_catalog as sc', 'banks.currency_catalog_id', '=', 'sc.id')
        ->join('company_bank', 'banks.id', '=', 'company_bank.bank_id')
        ->where('company_bank.company_id', $input['filterCompany'])
        ->orderBy('type', 'desc')
        ->select(DB::raw('sc_catalog.descripcion, sc_catalog.id_grupo, banks.id,
        banks.bank_catalog_id, banks.cuenta_catalog_id, banks.account_number,
        banks.account_name, banks.currency_catalog_id, banks.initial_balance,
        banks.updated_at, banks.type, company_bank.company_id, banks.date,
        (banks.initial_balance + banks.income + banks.expenditure) as current_balance,
        sc.descripcion as currency'))
        ->get();

        foreach ($bank as $key => $value) {
          if ($value->bank_catalog_id == 14) {
            $value->url = asset('img/bank/interbank.png');
          } else if ($value->bank_catalog_id == 15) {
            $value->url = asset('img/bank/bcp.png');
          } else if ($value->bank_catalog_id == 16) {
            $value->url = asset('img/bank/bbva.png');
          } else if ($value->bank_catalog_id == 17) {
            $value->url = asset('img/bank/scotiabank.png');
          } else {
            $value->url = asset('img/bank/box.png');
          }
        }

        return response()->json([
            'res' => true,
            'body' => [
                'data' => $bank
            ],
            'message' => 'Correcto'
        ]);
    }

    public function addCash(Request $request) {
      $bank = Bank::find($request->bank_id);
      $bank->initial_balance = $bank->initial_balance + $request->amount;
      $bank->save();

      return response()->json([
          'res' => true,
          'body' => [
              'data' => $bank
          ],
          'message' => 'Saldo agregado'
      ]);
    }
}
