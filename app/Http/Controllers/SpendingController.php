<?php

namespace App\Http\Controllers;

use App\Models\Spending;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SpendingController extends Controller
{
    public function store(Request $request) {
        $input = $request->all();

        // $count = DB::table('spending')
        //   ->where('id_voucher_categories', $input['id_voucher_categories'])
        //   ->where('state', 1)
        //   ->count();
        //
        // Log::info("# sub categorías: " .$count);
        //
        // if ($count >= 10) {
        //   return response()->json([
        //     'res' => false,
        //     'message' => 'Estimado usuario, solo puede crear un máximo de 10 categorías'
        //     ]);
        // }

        $sending = Spending::create($input);
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $sending
            ],
            'message' => 'Gasto creado correctamente'
        ]);
    }

    public function edit(Request $request) {
      $spending = Spending::find($request->id);

      if ($spending->state == 2) {
        return response()->json([
            'res' => false,
            'message' => 'No se puede editar esa categoría por ser autogenerada'
            ]);
      }

      $spending->name = $request->name;
      $spending->save();

      return response()->json([
          'res' => true,
          'body' => [
              'data' => 'Actualizado'
          ],
          'message' => 'Gasto actualizado correctamente'
      ]);
    }

    public function delete(Request $request) {
      $spending = Spending::find($request->id);

      if ($spending->state == 2) {
        return response()->json([
            'res' => false,
            'message' => 'No se puede editar esa categoría por ser autogenerada'
            ]);
      }

      $spending->state = 0;
      $spending->save();

      return response()->json([
          'res' => true,
          'body' => [
              'data' => 'Eliminado'
          ],
          'message' => 'Gasto eliminado correctamente'
      ]);
    }

    public function list(Request $request) {
        $input = $request->all();

        if (!$input['filter']) {
            $sending = Spending::where('state', 1)->get();
        } else {
            $sending = DB::table('spending')
              ->where('id_voucher_categories', $input['filter'])
              ->whereIn('state', [1,2])
              ->get();
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $sending
            ],
            'message' => 'Consultado correctamente'
        ]);
    }
}
