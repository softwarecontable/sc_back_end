<?php

namespace App\Http\Controllers;

use App\Models\MovementsVoucher;
use App\Models\Voucher;
use App\Models\BankMovement;
use App\Models\Bank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MovementsVoucherController extends Controller
{
    public function store(Request $request) {
        $input = $request->all();

        $total = 0.0;
        foreach ($input['movementsVouchers'] as $key => $value) {
          $total += doubleval($value['amount']);
        }

        $movement = BankMovement::find($input['movementsVouchers'][0]['id_bank_movements']);
        $associated = DB::table('movements_voucher')
          ->where('id_bank_movements', $input['movementsVouchers'][0]['id_bank_movements'])
          ->sum('amount');

          if ((abs(round($movement->amount, 2)) - round($associated, 2)) < $total) {
              return response()->json([
                  'res' => false,
                  'message' => 'No puede asociar por montos superiores'
              ]);
          } else if ((abs($movement->amount) - $associated) == $total) {
              $movement->state = 2;
              $movement->save();
          }

        for ($i = 0; $i < count($input['movementsVouchers']); $i++) {
            MovementsVoucher::create($input['movementsVouchers'][$i]);
            $voucher = Voucher::find($input['movementsVouchers'][$i]['id_voucher']);
            if (round(doubleval($input['movementsVouchers'][$i]['remaining_amount']), 2) == round(doubleval($input['movementsVouchers'][$i]['amount']), 2)) {
                $voucher->state = 2;
                $voucher->save();
            }

            $bank = Bank::find($movement->bank_id);
            if ($input['movementsVouchers'][$i]['type'] == 1) {
                $bank->expenditure = $bank->expenditure + $movement->amount;
                $bank->save();
            } else if ($input['movementsVouchers'][$i]['type'] == 2) {
                $bank->income = $bank->income + $movement->amount;
                $bank->save();
            }
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => []
            ],
            'message' => 'Movimiento vinculado correctamente'
        ]);
    }

    public function listByMovement(Request $request) {
        $input = $request->all();
        $vouchers = DB::table('movements_voucher')
        ->join('voucher', 'movements_voucher.id_voucher', 'voucher.id')
        ->where('movements_voucher.id_bank_movements', '=', $input['id_bank_movements'])
        ->get();
        for ($i = 0; $i < count($vouchers); $i++) {
            $vouchers[$i]->img_url = asset('img/' . $vouchers[$i]->file_img);
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $vouchers
            ],
            'message' => 'Consultado correctamente'
        ]);
    }

    public function listVoucherByMovement(Request $request) {
        $input = $request->all();
        $vouchers = DB::table('movements_voucher')
        ->join('voucher as v', 'movements_voucher.id_voucher', 'v.id')
        ->where('movements_voucher.id_bank_movements', '=', $input['id_bank_movements'])
        //->where('voucher.state', '=', $input['state'])
        ->select(DB::raw('v.id, v.id_spending, v.file_img, v.state, v.created_at, v.updated_at, movements_voucher.amount, v.currency_id'))
        ->get();

        for ($i = 0; $i < count($vouchers); $i++) {
            $vouchers[$i]->img_url = asset('img/' . $vouchers[$i]->file_img);
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $vouchers
            ],
            'message' => 'Consultado correctamente'
        ]);
    }

    public function listAudioByMovement(Request $request) {
        $input = $request->all();
        $audios = DB::table('movements_audios')
        ->join('audios', 'movements_audios.audio_id', 'audios.id')
        ->where('movements_audios.bank_movement_id', '=', $input['id_bank_movements'])
        ->select(DB::raw('audios.id, audios.name, audios.url, movements_audios.bank_movement_id'))
        ->get();

        for($i = 0; $i < count($audios); $i++) {
            $audios[$i]->audio_url = asset('img/' . $audios[$i]->url);
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $audios
            ],
            'message' => 'Consultado correctamente'
        ]);
    }
}
