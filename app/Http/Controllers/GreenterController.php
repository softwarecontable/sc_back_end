<?php

namespace App\Http\Controllers;

use Greenter\Model\Sale\FormaPagos\FormaPagoContado;
use Greenter\Model\Sale\Note;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
use Greenter\Model\Summary\SummaryDetail;
use Greenter\Ws\Services\SunatEndpoints;
use Greenter\Model\Voided\VoidedDetail;
use Greenter\Model\Voided\Reversion;
use Greenter\Model\Company\Address;
use Greenter\Model\Company\Company;
use Greenter\Model\Sale\SaleDetail;
use Greenter\Model\Summary\Summary;
use Greenter\Model\Client\Client;
use Greenter\Model\Sale\Document;
use Greenter\Model\Sale\Invoice;
use Greenter\Model\Sale\Charge;
use Greenter\Model\Sale\Legend;
use Greenter\See;

use App\Models\Company as ZumarCompany;
use App\Models\TypeDocument;
use App\Http\Controllers\EncryptionController;
use DateTime;

class GreenterController extends Controller
{

    public const CORRELATIVE_ERROR = 1033; // Error: El comprobante XXX-XXXX fue informado anteriormente
    public const EXCEPTION_ERROR = 1111; // Error: Una excepción

    protected $encryption;

    public function __construct(EncryptionController $encryption) {
        $this->encryption = $encryption;
    }

    public function test() {
        self::sendReportsToSummary();
    }

    public function saveCertificate($id, $password, UploadedFile $file) {
        $file_name = 'company_certified_'. $id .'.pem';
        //$store = $file->storeAs('certified', $file_name, 'public_uploads');

        $pfx = file_get_contents($file);

        openssl_pkcs12_read($pfx, $certs, $password);

        $public = isset($certs['cert']) ? $certs['cert'] : "";
        $private = isset($certs['pkey']) ? $certs['pkey'] : "";

        //Log::info($public.$private);

        Storage::disk('public_uploads')->put('certified/'.$file_name, $public.$private);

        // return asset('img/certified/' . $file_name);
        return 'certified/' . $file_name;
    }

    private function config(ZumarCompany $company, $endPoint) {
        $see = new See();
        $see->setCertificate(file_get_contents(asset('img/' . $company->certified)));
        $see->setService($endPoint); // SunatEndpoints::FE_PRODUCCION
        $see->setClaveSOL(
            $company->ruc,
            $this->encryption->decrypt($company->sol_user),
            $this->encryption->decrypt($company->sol_pass)
        );

        return $see;
    }

    public function sendInvoice($data, ZumarCompany $zumarCompany, $isDebug = false) {
        // Cliente
        $client = (new Client())
            ->setTipoDoc($data['client']['tipoDoc'])
            ->setNumDoc($data['client']['numDoc'])
            ->setRznSocial($data['client']['rznSocial']);

        // Emisor
        $address = (new Address())
            ->setUbigueo($data['company']['address']['ubigueo'])
            ->setDepartamento($data['company']['address']['departamento'])
            ->setProvincia($data['company']['address']['provincia'])
            ->setDistrito($data['company']['address']['distrito'])
            ->setUrbanizacion('-')
            ->setDireccion($data['company']['address']['direccion'])
            ->setCodLocal('0000'); // Codigo de establecimiento asignado por SUNAT, 0000 por defecto.

        $company = (new Company())
            ->setRuc($data['company']['ruc'])
            ->setRazonSocial($data['company']['razonSocial'])
            ->setNombreComercial($data['company']['razonSocial'])
            ->setAddress($address);

        $formaPago = (new FormaPagoContado())
            ->setTipo($data['formaPago']['tipo'])
            ->setMoneda($data['formaPago']['moneda']);

        // Venta
        $invoice = (new Invoice())
            ->setUblVersion($data['ublVersion'])
            ->setTipoOperacion($data['tipoOperacion']) // Venta - Catalog. 51
            ->setTipoDoc($data['tipoDoc']) // Factura - Catalog. 01 ; Boleta 03
            ->setSerie($data['serie']) //
            ->setCorrelativo($data['correlativo'])
            ->setFechaEmision(new DateTime())
            ->setFormaPago($formaPago) // FormaPago: Contado
            ->setTipoMoneda($data['tipoMoneda']) // Sol - Catalog. 02
            ->setCompany($company)
            ->setClient($client)
            ->setMtoOperGravadas($data['mtoOperGravadas'])
            ->setMtoIGV($data['mtoIGV'])
            ->setTotalImpuestos($data['totalImpuestos'])
            ->setValorVenta($data['valorVenta'])
            ->setSubTotal($data['subTotal'])
            ->setMtoImpVenta($data['mtoImpVenta']);

        $items = [];
        foreach ($data['details'] as $value) {
            array_push(
                $items,
                (new SaleDetail())
                    ->setCodProducto($value['codProducto'])
                    ->setUnidad($value['unidad'])
                    ->setCantidad($value['cantidad'])
                    ->setMtoValorUnitario($value['mtoValorUnitario'])
                    ->setDescuentos([
                        (new Charge())
                            ->setCodTipo('00') // Catalog. 53 (00: Descuento que afecta la Base Imponible)
                            ->setMontoBase($value['baseAmount'])
                            ->setFactor($value['factor'])
                            ->setMonto($value['discount'])
                    ])
                    ->setDescripcion($value['descripcion'])
                    ->setMtoBaseIgv($value['mtoBaseIgv'])
                    ->setPorcentajeIgv($value['porcentajeIgv'])
                    ->setIgv($value['igv'])
                    ->setTipAfeIgv($value['tipAfeIgv'])
                    ->setTotalImpuestos($value['totalImpuestos'])
                    ->setMtoValorVenta($value['mtoValorVenta']) // cantidad * valor unitario - descuento (que afecta la base)
                    ->setMtoPrecioUnitario($value['mtoPrecioUnitario']) // (Valor venta + Total Impuestos) / Cantidad
            );
        }

        $legends = [];
        foreach ($data['legends'] as $value) {
            array_push(
                $legends,
                (new Legend())
                    ->setCode($value['code'])
                    ->setValue($value['value'])
            );
        }

        $invoice->setDetails($items)
                ->setLegends($legends);

        $endPoint = $isDebug ? SunatEndpoints::FE_BETA : SunatEndpoints::FE_PRODUCCION;
        $see = self::config($zumarCompany, $endPoint);
        try {
            $result = $see->send($invoice);
            $fileName = $zumarCompany->id .'/' .$data['company']['ruc'] .'_' .$data['serie']. '_' .$data['correlativo'];

            // Verificamos que la conexión con SUNAT fue exitosa.
            if (!$result->isSuccess()) {
                // Mostrar error al conectarse a SUNAT.

                return [
                    "success" => false,
                    "code" => $result->getError()->getCode(),
                    "message" => "Codigo error: " .$result->getError()->getCode(). "\n" .$result->getError()->getMessage()
                ];
            } else {
                Storage::disk('public_sunat')->put($fileName.'.xml', $see->getFactory()->getLastXml());
                Storage::disk('public_sunat')->put($fileName.'.zip', $result->getCdrZip());

                $cdr = $result->getCdrResponse();
                $code = (int)$cdr->getCode();

                if ($code === 0) {
                    $message = $cdr->getDescription();
                    // echo 'ESTADO: ACEPTADA'.PHP_EOL;
                    if (count($cdr->getNotes()) > 0) {
                        foreach ($cdr->getNotes() as $key => $value) {
                            $message = $message ."\n". strval($value);
                        }
                        Log::channel('sunat')->info("******** Observaciones del comprobante emitido ********");
                        Log::channel('sunat')->info($message);
                        Log::channel('sunat')->info("******** Observaciones del comprobante emitido ********");
                        //echo 'OBSERVACIONES:'.PHP_EOL;
                        // Corregir estas observaciones en siguientes emisiones.
                    }
                    return [
                        "success" => true,
                        "message" => $message,
                        "code"=> $code
                    ];
                } else if ($code >= 2000 && $code <= 3999) {
                    //echo 'ESTADO: RECHAZADA'.PHP_EOL;
                    return [
                        "success" => true,
                        "message" => "Generación del CDR ha sido rechaza.",
                        "code"=> $code
                    ];
                } else {
                    /* Esto no debería darse, pero si ocurre, es un CDR inválido que debería tratarse como un error-excepción. */
                    /*code: 0100 a 1999 */
                    return [
                        "success" => true,
                        "message" => "Ocurrió un error al generar CDR.",
                        "code"=> $code
                    ];
                }

                // echo $cdr->getDescription().PHP_EOL;
            }
        } catch (\Throwable $th) {
            Log::channel("sunat")->error($th->getMessage());
            return [
                "success" => false,
                "message" => "No se completó la operación, por favor comuníquese con su proveedor.",
                "code" => GreenterController::EXCEPTION_ERROR
            ];
        }
    }

    public function sendReversion($data, ZumarCompany $zumarCompany) {
        Log::channel('sunat')->info(
            "**************************************** Inicio - Reversión ****************************************"
        );
        $correlative = TypeDocument::getNumber($zumarCompany->id)->retention;
        $items = [];
        foreach ($data['details'] as $value) {
            array_push(
                $items,
                (new VoidedDetail())
                    ->setTipoDoc($value['tipoDoc'])
                    ->setSerie($value['serie'])
                    ->setCorrelativo($value['correlativo'])
                    ->setDesMotivoBaja($value['desMotivoBaja'])
            );
        }

        $address = (new Address())
            ->setUbigueo($data['company']['address']['ubigueo'])
            ->setDepartamento($data['company']['address']['departamento'])
            ->setProvincia($data['company']['address']['provincia'])
            ->setDistrito($data['company']['address']['distrito'])
            ->setUrbanizacion('-')
            ->setDireccion($data['company']['address']['direccion'])
            ->setCodLocal('0000'); // Codigo de establecimiento asignado por SUNAT, 0000 por defecto.

        $company = (new Company())
            ->setRuc($data['company']['ruc'])
            ->setRazonSocial($data['company']['razonSocial'])
            ->setNombreComercial($data['company']['razonSocial'])
            ->setAddress($address);

        $fecGeneration = new DateTime();
        $fecGeneration->setTimestamp(strtotime($data['fecGeneration']));

        $fecComunicacion = new DateTime();
        $fecGeneration->setTimestamp(strtotime($data['fecComunicacion']));

        $reversion = (new Reversion())
            ->setCorrelativo($correlative)
            ->setFecGeneracion($fecGeneration)
            ->setFecComunicacion($fecComunicacion)
            ->setCompany($company)
            ->setDetails($items);

        // Envio a SUNAT.
        $see = self::config($zumarCompany, SunatEndpoints::RETENCION_BETA);
        $res = $see->send($reversion);
        // self::writeXml($reversion, $see->getFactory()->getLastXml());
        $xmlFileName = $reversion->getName().'.xml';
        Storage::disk('public_sunat')->put($xmlFileName, $see->getFactory()->getLastXml());

        if (!$res->isSuccess()) {
            Log::channel('sunat')->info("******** Observaciones del comprobante revertido ********");
            Log::channel('sunat')->info(self::getErrorResponse($res->getError()));
            Log::channel('sunat')->info("******** Observaciones del comprobante revertido ********");
            return [
                'success' => false,
                'message' => self::getErrorResponse($res->getError())
            ];
        }

        $ticket = $res->getTicket();
        Log::channel('sunat')->info('Ticket: ' .$ticket);

        $res = $see->getStatus($ticket);
        if (!$res->isSuccess()) {
            Log::channel('sunat')->info("******** Observaciones del comprobante revertido ********");
            Log::channel('sunat')->info(self::getErrorResponse($res->getError()));
            Log::channel('sunat')->info("******** Observaciones del comprobante revertido ********");
            return [
                'success' => false,
                'message' => self::getErrorResponse($res->getError())
            ];
        }

        $cdr = $res->getCdrResponse();
        // $util->writeCdr($reversion, $res->getCdrZip());

        $cdrFileName = 'R-'.$reversion->getName().'.zip';
        Storage::disk('public_sunat')->put($cdrFileName, $res->getCdrZip());

        Log::channel('sunat')->info(
            "**************************************** Fin - Reversión ****************************************"
        );

        return [
            'success' => true,
            'message' => $cdr->getDescription(),
            'data' => [
                'notes' => $cdr->getNotes(),
                'xml' => asset('summary/'.$xmlFileName),
                'cdr' => asset('summary/'.$cdrFileName)
            ]
        ];
    }

    public function sendNote($data, ZumarCompany $zumarCompany, $isDebug = false) {

        $client = (new Client())
            ->setTipoDoc($data['client']['tipoDoc'])
            ->setNumDoc($data['client']['numDoc'])
            ->setRznSocial($data['client']['rznSocial']);

        // Emisor
        $address = (new Address())
            ->setUbigueo($data['company']['address']['ubigueo'])
            ->setDepartamento($data['company']['address']['departamento'])
            ->setProvincia($data['company']['address']['provincia'])
            ->setDistrito($data['company']['address']['distrito'])
            ->setUrbanizacion('-')
            ->setDireccion($data['company']['address']['direccion'])
            ->setCodLocal('0000'); // Codigo de establecimiento asignado por SUNAT, 0000 por defecto.

        $company = (new Company())
            ->setRuc($data['company']['ruc'])
            ->setRazonSocial($data['company']['razonSocial'])
            ->setNombreComercial($data['company']['razonSocial'])
            ->setAddress($address);

        $note = (new Note())
            ->setUblVersion($data['ublVersion'])
            ->setTipoDoc($data['tipoDoc'])
            ->setSerie($data['serie'])
            ->setCorrelativo($data['correlativo'])
            ->setFechaEmision(new DateTime())
            ->setTipDocAfectado($data['tipoDocAfectado']) // Tipo Doc: Factura
            ->setNumDocfectado($data['numDocAfectado']) // Factura: Serie-Correlativo
            ->setCodMotivo($data['codMotivo']) // Catalogo. 09
            ->setDesMotivo($data['desMotivo'])
            ->setTipoMoneda($data['tipoMoneda'])
            // ->setGuias([/* Guias (Opcional) */
            //     (new Document())
            //     ->setTipoDoc('09')
            //     ->setNroDoc('0001-213')
            // ])
            ->setCompany($company)
            ->setClient($client)
            ->setMtoOperGravadas($data['mtoOperGravadas'])
            ->setMtoIGV($data['mtoIGV'])
            ->setTotalImpuestos($data['totalImpuestos'])
            ->setMtoImpVenta($data['mtoImpVenta']);

        $items = [];
        foreach ($data['details'] as $value) {
            array_push(
                $items,
                (new SaleDetail())
                    ->setCodProducto($value['codProducto'])
                    ->setUnidad($value['unidad'])
                    ->setCantidad($value['cantidad'])
                    ->setMtoValorUnitario($value['mtoValorUnitario'])
                    ->setDescuentos([
                        (new Charge())
                            ->setCodTipo('00') // Catalog. 53 (00: Descuento que afecta la Base Imponible)
                            ->setMontoBase($value['baseAmount'])
                            ->setFactor($value['factor'])
                            ->setMonto($value['discount'])
                    ])
                    ->setDescripcion($value['descripcion'])
                    ->setMtoBaseIgv($value['mtoBaseIgv'])
                    ->setPorcentajeIgv($value['porcentajeIgv'])
                    ->setIgv($value['igv'])
                    ->setTipAfeIgv($value['tipAfeIgv'])
                    ->setTotalImpuestos($value['totalImpuestos'])
                    ->setMtoValorVenta($value['mtoValorVenta']) // cantidad * valor unitario - descuento (que afecta la base)
                    ->setMtoPrecioUnitario($value['mtoPrecioUnitario']) // (Valor venta + Total Impuestos) / Cantidad
            );
        }

        $legends = [];
        foreach ($data['legends'] as $value) {
            array_push(
                $legends,
                (new Legend())
                    ->setCode($value['code'])
                    ->setValue($value['value'])
            );
        }

        $note->setDetails($items)
            ->setLegends($legends);

        $endPoint = $isDebug ? SunatEndpoints::FE_BETA : SunatEndpoints::FE_PRODUCCION;
        $see = self::config($zumarCompany, $endPoint);
        try {
            $result = $see->send($note);
            $fileName = $zumarCompany->id .'/' .$data['company']['ruc'] .'_' .$data['serie']. '_' .$data['correlativo'] .'_'. $data['numDocAfectado'];

            // Verificamos que la conexión con SUNAT fue exitosa.
            if (!$result->isSuccess()) {
                // Mostrar error al conectarse a SUNAT.
                return [
                    "success" => false,
                    "message" => "Codigo error: " .$result->getError()->getCode(). "\n" .$result->getError()->getMessage()
                ];
            } else {
                Storage::disk('public_sunat')->put($fileName.'.xml', $see->getFactory()->getLastXml());
                Storage::disk('public_sunat')->put($fileName.'.zip', $result->getCdrZip());

                $cdr = $result->getCdrResponse();
                $code = (int)$cdr->getCode();

                if ($code === 0) {
                    $message = $cdr->getDescription();
                    // echo 'ESTADO: ACEPTADA'.PHP_EOL;
                    if (count($cdr->getNotes()) > 0) {
                        foreach ($cdr->getNotes() as $key => $value) {
                            $message = $message ."\n". strval($value);
                        }
                        Log::channel('sunat')->info("******** Observaciones del comprobante emitido ********");
                        Log::channel('sunat')->info($message);
                        Log::channel('sunat')->info("******** Observaciones del comprobante emitido ********");
                        //echo 'OBSERVACIONES:'.PHP_EOL;
                        // Corregir estas observaciones en siguientes emisiones.
                    }
                    return [
                        "success" => true,
                        "message" => $message
                    ];
                } else if ($code >= 2000 && $code <= 3999) {
                    //echo 'ESTADO: RECHAZADA'.PHP_EOL;
                    return [
                        "success" => true,
                        "message" => "Generación del CDR ha sido rechaza."
                    ];
                } else {
                    /* Esto no debería darse, pero si ocurre, es un CDR inválido que debería tratarse como un error-excepción. */
                    /*code: 0100 a 1999 */
                    return [
                        "success" => true,
                        "message" => "Ocurrió un error al generar CDR."
                    ];
                }

                // echo $cdr->getDescription().PHP_EOL;
            }
        } catch (\Throwable $th) {
            Log::channel("sunat")->error($th->getMessage());
            return [
                "success" => false,
                "message" => "No se completó la operación, por favor comuníquese con su proveedor."
            ];
        }
    }

    public function sendReportsToSummary() {
        Log::channel('summary')->info(
            "**************************************** Inicio - Envío de resúmen ****************************************"
        );

        $startDate = new DateTime('-15days');
        $endDate = new DateTime();

        $reports = DB::table('sale_report')
            ->groupBy('company_id')
            ->whereIn('tipoDoc', TypeDocument::docSummaries())
            ->whereDate('created_at', '=', $startDate)
            ->select('company_id')
            ->get();

        $correlative = DB::table('parameters')
            ->where('group_id', 'summary_number')
            ->first()
            ->value;

        foreach ($reports as $key => $value) {
            $zumarCompany = ZumarCompany::find($value->company_id);
            $details = [];
            $ids = [];

            $documents = DB::table('sale_report')
                ->whereIn('tipoDoc', TypeDocument::docSummaries())
                ->whereDate('created_at', '=', $startDate)
                ->get();

            $address = (new Address())
                ->setUbigueo('150101')
                ->setDepartamento('LIMA')
                ->setProvincia('LIMA')
                ->setDistrito('LIMA')
                ->setUrbanizacion('-')
                ->setDireccion($zumarCompany->address)
                ->setCodLocal('0000');

            $company = (new Company())
                ->setRuc($zumarCompany->ruc)
                ->setRazonSocial($zumarCompany->commercial_name)
                ->setNombreComercial($zumarCompany->commercial_name)
                ->setAddress($address);

            foreach ($documents as $key => $document) {
                $item = (new SummaryDetail())
                    ->setTipoDoc($document->tipoDoc)
                    ->setSerieNro($document->serie .'-'. $document->correlativo)
                    ->setEstado(strval($document->state))
                    ->setClienteTipo($document->client_tipo_doc)
                    ->setClienteNro($document->client_doc)
                    ->setTotal($document->subTotal) // 26.60
                    ->setMtoOperGravadas($document->operacionGravadas) // 20.00
                    ->setMtoOperExoneradas($document->precio - $document->operacionGravadas) // 0.00
                    ->setMtoIGV($document->totalImpuestos); // 6.60

                array_push($details, $item);
                array_push($ids, $document->id);
            }
            Log::channel('summary')->info(json_encode($ids));

            $sum = new Summary();
            // Fecha Generacion menor que Fecha Resumen
            $sum->setFecGeneracion($startDate)
                ->setFecResumen($endDate)
                ->setCorrelativo("00$correlative")
                ->setCompany($company)
                ->setDetails($details);

            $sumamrySent = self::sendSummary($zumarCompany, $sum);
            $result = json_encode($sumamrySent);
            Log::channel('summary')->info($result);

            if ($sumamrySent['success']) {
                $correlative = $correlative + 1;
            }
        }

        DB::table('parameters')
            ->where('group_id', 'summary_number')
            ->update([
                "value" => $correlative
            ]);

        Log::channel('summary')->info(
            "**************************************** Fin - Envío de resúmen ****************************************"
        );
    }

    public function sendSummary(ZumarCompany $zumarCompany, Summary $sum) {

        $see = self::config($zumarCompany, SunatEndpoints::FE_BETA);
        $res = $see->send($sum);

        $xmlFileName = $sum->getName().'.xml';
        Storage::disk('public_summary')->put($xmlFileName, $see->getFactory()->getLastXml());

        if (!$res->isSuccess()) {
            return [
                'success' => false,
                'message' => self::getErrorResponse($res->getError())
            ];
        }

        $ticket = $res->getTicket();
        Log::channel('summary')->info('Ticket: ' .$ticket);
        $res = $see->getStatus($ticket);

        if (!$res->isSuccess()) {
            return [
                'success' => false,
                'message' => self::getErrorResponse($res->getError())
            ];
        }

        $cdr = $res->getCdrResponse();
        $cdrFileName = 'R-'.$sum->getName().'.zip';
        Storage::disk('public_summary')->put($cdrFileName, $res->getCdrZip());

        return [
            'success' => true,
            'message' => $cdr->getDescription(),
            'data' => [
                'notes' => $cdr->getNotes(),
                'xml' => asset('summary/'.$xmlFileName),
                'cdr' => asset('summary/'.$cdrFileName)
            ]
        ];
    }

    private function getErrorResponse($error): string {
        $result = $error->getCode() .'\n'. $error->getMessage();
        return $result;
    }

}
