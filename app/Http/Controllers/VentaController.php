<?php

namespace App\Http\Controllers;

use App\Models\Venta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class VentaController extends Controller
{
    public function store(Request $request) {
        $input = $request->all();
        $venta = DB::table('venta')->where('id_movimiento', '=', $input['id_movimiento'])->get();
        if (count($venta) > 0) {
            $venta = DB::table('venta')->where('id_movimiento', '=', $input['id_movimiento'])->update($input);
        } else {
            unset($input['id']);
            $venta = Venta::create($input);
        }       
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $venta
            ],
            'message' => 'Venta creada correctamente'
        ]);
    }

    public function listByMovement(Request $request) {
        $input = $request->all();
        $venta = DB::table('venta')->where('id_movimiento', '=', $input['id_movimiento'])->get();
        if (count($venta) == 0) {
            $secuencia = DB::table('venta')
            ->join('bank_movements', 'venta.id_movimiento', 'bank_movements.id')
            ->join('company_bank', 'bank_movements.bank_id', 'company_bank.bank_id')
            ->where('company_bank.company_id', '=', $input['company_id'])
            ->orderBy('venta.secuencia', 'desc')
            ->select('venta.secuencia', 'company_bank.*')
            ->get();
            if (count($secuencia) > 0) {
                $venta['secuencia'] = $secuencia[0]->secuencia + 1;
            } else {
                $venta['secuencia'] = 1;
            }
            return response()->json([
                'res' => false,
                'body' => [
                    'data' => $venta
                ],
                'message' => 'Consultado correctamente'
            ]);
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $venta
            ],
            'message' => 'Consultado correctamente'
        ]); 
    }

    public function deleteVenta(Request $request) {
        $input = $request->all();
        $venta_detalle = DB::table('venta_detalle')->where('venta_id', '=', $request['id_venta'])->get();
        for ($i = 0; $i < count($venta_detalle); $i++) {
            $detalle = DB::table('venta_detalle')->where('id', '=', $venta_detalle[$i]->id)->delete();
        }
        $venta = DB::table('venta')->where('id', '=', $input['id_venta'])->delete();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $venta
            ],
            'message' => 'Venta eliminada correctamente'
        ]); 
    }
}
