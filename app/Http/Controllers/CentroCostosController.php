<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class CentroCostosController extends Controller
{
    public function searchByCod(Request $request) {
        $input = $request->all();
        $centro_costos = DB::table('centro_costos')->where('codigo', '=', $input['cod'])->get();
        if (count($centro_costos) > 0) {
            return response()->json([
                'res' => true,
                'body' => [
                    'data' => $centro_costos
                ],
                'message' => 'Consultado correctamente'
            ]);
        } else {
            return response()->json([
                'res' => false,
                'body' => [
                    'data' => []
                ],
                'message' => 'Consultado correctamente'
            ]);
        }
    }
}
