<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClienteController extends Controller
{
    public function store(Request $request) {
        $input = $request->all();
        $cliente = DB::table('cliente')->where('id', '=', $input['id'])->get();
        if (count($cliente) > 0) {
            unset($input['name']);
            unset($input['name_document']);
            $cliente = DB::table('cliente')->where('id', '=', $input['id'])->update($input);
        } else {
            unset($input['id']);
            $cliente = Cliente::create($input);
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $cliente
            ],
            'message' => 'Cliente creado correctamente'
        ]);
    }

    public function list(Request $request) {
        $input = $request->all();
        $query = DB::table('cliente')
        ->join('company', 'cliente.company_id', '=', 'company.id')
        ->where('cliente.company_id', '=', $input['company_id'])
        ->select('cliente.*', 'company.name');
        
        if (isset($input['nro_document'])) {
            $query->where('nro_document', '=', $input['nro_document']);
        }
        $clientes = $query->get();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $clientes
            ],
            'message' => 'Consultado correctamente'
        ]);
    }

    public function delete(Request $request) {
        $input = $request->all();
        $cliente = DB::table('cliente')->where('id', '=', $input['id'])->delete();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $cliente
            ],
            'message' => 'Eliminado correctamente'
        ]);
    }
}
