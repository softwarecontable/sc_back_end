<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\GreenterController;
use App\Http\Controllers\EncryptionController;

class CompanyController extends Controller
{

    protected $greenter;
    protected $encryption;

    public function __construct(GreenterController $greenter, EncryptionController $encryption) {
        $this->greenter = $greenter;
        $this->encryption = $encryption;
    }

    public function store(Request $request) {
      $input = $request->all();

      if (DB::table('company')->where('ruc', $input['ruc'])->exists()) {
        return response()->json([
            'res' => false,
            'message' => 'La empresa ya se encuentra creada'
        ]);
      }

      $company = Company::create($input);
      return response()->json([
          'res' => true,
          'body' => [
              'data' => $company
          ],
          'message' => 'Empresa creada correctamente'
      ]);
    }

    public function update(Request $request) {

      Log::info($request->all());
      $logo = "";
      $certified = "";

      $company = Company::find($request->id);
      $company->name = $request->name;
      $company->line_business = $request->line_business;
      $company->type_company = $request->type_company;
      $company->tax_regime = $request->tax_regime;
      $company->ruc = $request->ruc;
      $company->address = $request->address;
      $company->currency = $request->currency;
      $company->sol_user = $this->encryption->encrypt($request->sol_user);
      $company->sol_pass = $this->encryption->encrypt($request->sol_pass);
      $company->commercial_name = $request->commercial_name;

      if ($request->hasFile('file_img')) {
          $logo = self::uploadLogo($request->id, $request->file('file_img'));
          $company->logo = $logo;
      }

      if ($request->hasFile('certified')) {
        $company->certified = $this->greenter->saveCertificate(
            $request->id,
            $request->certified_password,
            $request->file('certified')
        );
      }

      $company->save();
      $company->type_company = intval($company->type_company);
      $company->tax_regime = intval($company->tax_regime);
      $company->currency = intval($company->currency);
      $company->sol_user = $request->sol_user;
      $company->sol_pass = $request->sol_pass;

      return response()->json([
          'res' => true,
          'body' => [
              'data' => $company
          ],
          'message' => 'Empresa editada correctamente'
      ]);
    }

    private function uploadLogo($id, UploadedFile $file) {
      // Log::info($request->all());
      //if ($request->hasFile($request->file('file_img'))) {
        // $company = Company::find($request->id);

        // $file = $request->file('file_img');
        $extension = $file->getClientOriginalExtension();
        $file_name = 'company_logo_'. $id .'.'. $extension;

        if (Storage::disk('public_uploads')->exists($file_name)) {
            Storage::disk('public_uploads')->delete($file_name);
        }

        $store = $file->storeAs('logo', $file_name, 'public_uploads');
        return $store;
        // return asset('img/' . $store);
        // $company->logo = asset('img/' . $store);
        // $company->save();

        // return response()->json([
        //     'res' => true,
        //     'body' => [
        //         'data' => asset('img/' . $store)
        //     ],
        //     'message' => 'Empresa actualizada correctamente'
        // ]);
      //} else {
      //  return response()->json([
      //      'res' => false,
      //      'message' => 'No se encontró el archivo seleccionado'
      //  ]);
      //}
    }

    public function list(Request $request) {
        $input = $request->all();
        $companies = DB::table('company')
        ->get();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $companies
            ],
            'message' => 'Consultado correctamente'
        ]);
    }
}
