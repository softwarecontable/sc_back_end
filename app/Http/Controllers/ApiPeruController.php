<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ApiPeruController extends Controller
{
    public function test(Request $request) {
        $cert64 = base64_encode(file_get_contents($request->file('certified')));

        $certData = array(
            'cert' => $cert64,
            'cert_pass' => $request->certified_password,
            'base64' => true
        );

        $response = self::getCertified($certData);
        return json_encode($response);
    }

    public function login() {
      $user = env('API_PERU_USER', '');
      $pwd = env('API_PERU_PWD', '');
      $url = env('API_PERU_URL', '');

      $curl = curl_init();
      // Make Post Fields Array
      $data = [
          'username' => $user,
          'password' => $pwd
      ];

      curl_setopt_array($curl, array(
          CURLOPT_URL => "$url/auth/login",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30000,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
              // Set here requred headers
              "Accept: application/json",
              "content-type: application/json"
          ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);

      if ($err) {
          //echo "cURL Error #:" . $err;
          return 'Error' .$err;
      } else {
          //print_r(json_decode($response));
          return json_decode($response)->token;
      }
    }

    public function createCompany($data) {
        $token = self::login();
        $url = env('API_PERU_URL', '');

        $curl = curl_init();
        // Make Post Fields Array

        curl_setopt_array($curl, array(
            CURLOPT_URL => "$url/companies",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                // Set here requred headers
                "Accept: application/json",
                "content-type: application/json",
                "Authorization: Bearer $token"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            //echo "cURL Error #:" . $err;
            return 'Error' .$err;
        } else {
            $res = json_decode($response, true);
            Log::info($res);

            if (isset($res["id"])) {
                $token = self::getCompanyToken($res["id"], $token);
                $res["token"] = $token;
                $res["success"] = true;
            } else if (isset($res["error"])) {
                // Return error with message
                $res["success"] = false;
            } else {
                // Return general error
                $res = array(
                    "success" => false,
                    "error" => "Hubo un error al guardar, por favor contacte con el proveedor"
                );
            }
            return $res;
        }
    }

    public function updateCompany($data, $id) {
      $token = self::login();
      $url = env('API_PERU_URL', '');

      $curl = curl_init();
      // Make Post Fields Array

      curl_setopt_array($curl, array(
          CURLOPT_URL => "$url/companies/$id",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30000,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "PUT",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
              // Set here requred headers
              "Accept: application/json",
              "content-type: application/json",
              "Authorization: Bearer $token"
          ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);

      if ($err) {
          //echo "cURL Error #:" . $err;
          return 'Error' .$err;
      } else {
          //print_r(json_decode($response));
          return json_decode($response);
      }
    }

    public function getCertified($data) {
      $token = self::login();
      $url = env('API_PERU_URL', '');
      $curl = curl_init();
      // Make Post Fields Array

      curl_setopt_array($curl, array(
          CURLOPT_URL => "$url/companies/certificate",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30000,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
              // Set here requred headers
              "Accept: application/json",
              "content-type: application/json",
              "Authorization: Bearer $token"
          ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);

      if ($err) {
          //echo "cURL Error #:" . $err;
          return '';
      } else {
          //print_r(json_decode($response));
          return json_decode($response);
      }
    }

    public function getCompanyToken($id, $token) {
      $url = env('API_PERU_URL', '');
      $curl = curl_init();
      // Make Post Fields Array

      curl_setopt_array($curl, array(
          CURLOPT_URL => "$url/companies/$id",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30000,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          //CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
              // Set here requred headers
              "Accept: application/json",
              "content-type: application/json",
              "Authorization: Bearer $token"
          ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);

      if ($err) {
          //echo "cURL Error #:" . $err;
          return '';
      } else {
          //print_r(json_decode($response));
          return json_decode($response)->token->code;
      }

    }

    public function sendVoucher($data, $token) {
      $url = env('API_PERU_URL', '');

      $curl = curl_init();
      // Make Post Fields Array

      curl_setopt_array($curl, array(
          CURLOPT_URL => "$url/invoice/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30000,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
              // Set here requred headers
              "Accept: application/json",
              "content-type: application/json",
              "Authorization: Bearer $token"
          ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);

      if ($err) {
          //echo "cURL Error #:" . $err;
          return '';
      } else {
          //print_r(json_decode($response));
          return json_decode($response);
      }

    }

    public function nullReport($data, $token) {
      $url = env('API_PERU_URL', '');

      $curl = curl_init();
      // Make Post Fields Array

      curl_setopt_array($curl, array(
          CURLOPT_URL => "$url/voided/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30000,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
              // Set here requred headers
              "Accept: application/json",
              "content-type: application/json",
              "Authorization: Bearer $token"
          ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);

      if ($err) {
          //echo "cURL Error #:" . $err;
          return '';
      } else {
          //print_r(json_decode($response));
          return json_decode($response);
      }
    }

    public function creditNote($data, $token) {
      $url = env('API_PERU_URL', '');

      $curl = curl_init();
      // Make Post Fields Array

      curl_setopt_array($curl, array(
          CURLOPT_URL => "$url/note/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30000,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
              // Set here requred headers
              "Accept: application/json",
              "content-type: application/json",
              "Authorization: Bearer $token"
          ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);

      if ($err) {
          //echo "cURL Error #:" . $err;
          return '';
      } else {
          //print_r(json_decode($response));
          return json_decode($response);
      }
    }

}
