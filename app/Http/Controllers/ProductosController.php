<?php

/**
 * @author wscojal <walter_cojal@hotmail.com>
 * @copyright Copyright (c) 2024, Walter Santiago Cojal Medina
 * @abstract Gestiona todo relacionado con los productos que vende el negocio.
 */

namespace App\Http\Controllers;

use App\Models\VoucherCategories;
use App\Models\SaleReportDetail;
use App\Models\TypeDocument;
use App\Models\SaleReport;
use App\Models\Spending;
use App\Models\Cliente;
use App\Models\Company;
use App\Models\Voucher;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiPeruController;
use App\Http\Controllers\GreenterController;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\VoucherController;

class ProductosController extends Controller
{

    protected $greenter;
    protected $pdf;
    protected $voucher;

    public function __construct(
        GreenterController $greenter,
        PdfController $pdf,
        VoucherController $voucher
    ) {
        $this->greenter = $greenter;
        $this->voucher = $voucher;
        $this->pdf = $pdf;
    }

    public function searchByCod(Request $request) {
        $input = $request->all();
        $productos = DB::table('productos')->where('codigo_producto', '=', $input['cod'])->get();

        return response()->json([
            'res' => count($productos) > 0,
            'body' => [
                'data' => $productos
            ],
            'message' => (count($productos) > 0) ? 'Consultado correctamente' : 'No tiene productos agregados'
        ]);
    }

    public function getAllProductsByCompany(Request $request) {
        $input = $request->all();
        $productos = DB::select('call sp_get_all_productos_by_company(?)', array($input['company_id']));
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $productos
            ],
            'message' => 'Consultado correctamente'
        ]);
    }

    public function getProductsByClase(Request $request) {
        $input = $request->all();
        $productos = DB::table('producto')
        ->join('inventario', 'producto.id', '=', 'inventario.producto_id')
        ->where('categoria_id', '=', $input['categoria_id'])
        ->get();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $productos
            ],
            'message' => 'Consultado correctamente'
        ]);
    }

    /**
     * Crea los comprobantes de tipo BOLETA DE VENTA, FACTURA, NOTA DE VENTA, COTIZACION.
     * @param Request $request trae toda la información necesaria.
     */
    public function ceSend(Request $request) {
      $ceSendCode = time();
      Log::info("Code send invoice: $ceSendCode");
      Log::info($request->all());
      $company = Company::find($request->company_id);

      /**
       * Valida si el cliente con ese documento ya existe.
       * Si no existe, crea uno nuevo.
       * Pero si ya existe, solo actualiza los datos.
       * @var int $request->company_id es el id de la compañía.
       * @var string $request->client['numDoc'] es el documento del cliente.
       */

      if (!DB::table('cliente')
        ->where('company_id', $request->company_id)
        ->where('nro_document', $request->client['numDoc'])
        ->exists()
      ) {
          $cliente = new Cliente;
          $cliente->type_document = intval($request->client['tipoDoc']);
          $cliente->nro_document = $request->client['numDoc'];
          $cliente->razon_social = $request->client['rznSocial'];
          $cliente->email = isset($request->client['email']) ? $request->client['email'] : "";
          $cliente->telefono = isset($request->client['phone']) ? $request->client['phone'] : "";
          $cliente->direccion = isset($request->client['address']['direccion']) ? $request->client['address']['direccion'] : "";
          $cliente->type = $company->type_company;
          $cliente->company_id = $request->company_id;
          $cliente->save();
      } else {
          $cliente = Cliente::where('company_id', $request->company_id)
            ->where('nro_document', $request->client['numDoc'])
            ->first();

          $cliente->type_document = intval($request->client['tipoDoc']);
          $cliente->nro_document = $request->client['numDoc'];
          $cliente->razon_social = $request->client['rznSocial'];
          $cliente->email = isset($request->client['email']) ? $request->client['email'] : "";
          $cliente->telefono = isset($request->client['phone']) ? $request->client['phone'] : "";
          $cliente->direccion = isset($request->client['address']['direccion']) ? $request->client['address']['direccion'] : "";
          $cliente->type = $company->type_company;
          $cliente->company_id = $request->company_id;
          $cliente->save();
      }

      /**
       * Verifica si hay stock suficiente para continuar con la venta.
       * @var string $request->tipoDoc es el tipo de documento que se está generando.
       */
      $itemsToUpdate = [];
      if (TypeDocument::requiresStock($request->tipoDoc)) { // Valida si el comprobante va a usar stock o no
        foreach ($request->details as $key => $value) {
            $product = DB::table('producto as p')
                ->leftJoin('inventario as i', 'i.producto_id', '=', 'p.id')
                ->where('p.id', $value['idProduct'])
                ->select(DB::raw('i.id, i.cantidad_actual, i.cantidad_minima'))
                ->first();

            if (isset($product->id)) {
                if (($product->cantidad_actual - $value['cantidad']) < $product->cantidad_minima) {
                    return response()->json([
                        'res' => false,
                        'message' => 'Stock insuficiente'
                    ]);
                } else {
                    $it = array(
                      'id' => $product->id,
                      'cantidad_actual' => $product->cantidad_actual,
                      'cantidad' => $value['cantidad']
                    );
                    array_push($itemsToUpdate, $it);
                }
            }
          }
      }

      $newSerie = TypeDocument::getSerie($request->company_id);
      if (strlen($request->serie) == 1) {
          $request->serie = $request->serie . $newSerie;
          $request->voucher = $request->serie ."-". TypeDocument::getTag($request->company_id, $request->tipoDoc);
      } else {
          $request->voucher = str_replace("001-", $newSerie ."-", $request->voucher);
          $request->serie =  str_replace("001", $newSerie, $request->serie);
      }

      /**
       * Solo comprueba si el comprobante va a la sunat o no, y si el correlativo aumentará.
       */
      $isDebug = $request->header('Env') !== null ? intval($request->header('Env')) : 1; // 1: Prod, 0: Debug
      $message = "";
      $reportId = 0;
      $correlative = 0;

      if (TypeDocument::needsSunatSend($request->tipoDoc)) { // Quiere decir que es BOLETA DE VENTA (7) o FACTURA (8).

        /**
         * Valida los datos más importantes para facturar.
         */
        if (
            !isset($company->logo)
             || !isset($company->certified)
             || !isset($company->sol_user)
             || !isset($company->sol_pass)
             || !isset($company->address)
        ) {
            return response()->json([
                'res' => false,
                'body' => [
                    'data' => null
                ],
                'message' => 'Los datos de su empresa no están actualizados.'
            ]);
        }

        $data = self::parseDataToReport($request);
        Log::channel('sunat')->info("Code: $ceSendCode");
        Log::channel('sunat')->info("------------------------- Send voucher params: -------------------------");
        Log::channel('sunat')->info(json_encode($data));

        $res = $this->greenter->sendInvoice($data, $company, $isDebug == 0);
        Log::channel('sunat')->info("------------------------- Send voucher response: -------------------------");
        Log::channel('sunat')->info(json_encode($res));
        $message = $res['message'];

        if ($res['success']) {
            /**
             * Actualiza el stock de los productos vendidos.
             */
            foreach ($itemsToUpdate as $value) {
                DB::table('inventario')
                    ->where('id', $value['id'])
                    ->update([
                        'cantidad_actual' => ($value['cantidad_actual'] - $value['cantidad'])
                    ]);
            }

            /**
             * Guarda en la BD los datos del reporte generado.
             * En las tablas `sale_report` y `sale_report_detail`
             */

            $reportId = SaleReport::saveReport($request);

            // Use new correlative number validation
            $correlative = TypeDocument::updateNumber(
                $isDebug == 0,
                intval($request->tipoDoc),
                $request->company_id
            );

            $message = 'Comprobante generado.';
        } else {

            /**
             * Si el error es sobre el correlativo del documento,
             * se aumentará este.
             */
            if (intval($res['code']) == GreenterController::CORRELATIVE_ERROR) {
                $correlative = TypeDocument::updateNumber(
                    $isDebug == 0,
                    intval($request->tipoDoc),
                    $request->company_id
                );
            }

            return response()->json([
                'res' => false,
                'body' => [
                    'data' => null
                ],
                'message' => $message
            ]);
        }

      } else { // Es otro tipo de documento, NOTA DE VENTA (99) o COTIZACION (100).
        // Guardar nota de venta en base de datos

        if (TypeDocument::requiresStock($request->tipoDoc)) { // Valida que requiera stock para actualizar las cantidades.

            /**
             * Actualiza el stock de los productos vendidos.
             */
            foreach ($itemsToUpdate as $value) {
                DB::table('inventario')
                    ->where('id', $value['id'])
                    ->update([
                        'cantidad_actual' => ($value['cantidad_actual'] - $value['cantidad'])
                    ]);
            }

        }

        $reportId = SaleReport::saveReport($request);

        // Use new correlative number validation
        $correlative = TypeDocument::updateNumber(
            $isDebug == 0,
            intval($request->tipoDoc),
            $request->company_id
        );

      }

      /**
       * Cuando todo ha sido exitósamente, se procede a generar el comprobamte.
       * Este comprobante es generado como pdf y es automáticamente almacenado en el servidor.
       * @var $filename tiene el formato: /(Id de la compañia)/(ruc)_(serie)_(correlativo).pdf
       */
      $realCorrelative = $correlative - 1;
      $filename = $company->ruc. "_" .$request->serie. "_" .$realCorrelative. ".pdf";
      $file = $company->id. "/" .$filename;
      $url = "";

      if ($request->document == PdfController::TICKET) { // Ticket
        $url = $this->pdf->createTicket("/" .$file, $request, $company);
      } else if ($request->document == PdfController::DOCUMENT) { // Documento
        $url = $this->pdf->createA4Ticket("/" .$file, $request, $company);
      }

      /**
       * Ahora procedemos a crear el ingreso de la empresa.
       * Se guardará de la siguiente manera
       */
      $this->voucher->addVoucher($company->id, $file, $request->subTotal, $reportId, $request->currency_id, $request->voucherName);

      DB::table('sale_report')
        ->where('id', $reportId)
        ->update(['url' => $file]);

      return response()->json([
          'res' => true,
          'body' => [
              'data' => [
                  "correlativo" => $correlative,
                  "report_id" => $reportId,
                  "url" => $url
              ]
          ],
          'message' => 'Consultado correctamente'
      ]);

    }

    /**
     * Guarda el comprobante generado en formato PDF
     */
    public function saveDocument(Request $request) {
        $documents = VoucherController::SUNAT_DOCUMENT;

        $image = "";
        if ($request->hasFile('file_img')) {
          $image = self::storeImage($request->id, $request->name, $request->amount, $request->file('file_img'));
        } else if ($request->hasFile('file_pdf')) {
          $image = self::storeImage($request->id, $request->name, $request->amount, $request->file('file_pdf'));
        }

        if (
          DB::table('voucher_categories')
              ->where('company_id', $request->id)
              ->where('type', 2)
              ->where('name', $documents)
              ->exists()
        ) {

          $spending = DB::table('voucher_categories as vc')
              ->join('spending as s', 's.id_voucher_categories', '=', 'vc.id')
              ->where('vc.company_id', $request->id)
              ->where('vc.type', 2)
              ->where('vc.name', $documents)
              ->where('s.name', $request->name)
              ->select('s.*')
              ->first();

          if ($spending) {

            $voucher = new Voucher;
            $voucher->id_spending = $spending->id;
            $voucher->amount = $request->amount;
            $voucher->file_img = $image;
            $voucher->state = 1;
            $voucher->sale_report_id = $request->report_id;
            $voucher->currency_id = isset($request->currency_id) ? $request->currency_id : 9;

            $voucher->save();

          } else { // Crear spending y voucher

            $category = DB::table('voucher_categories')
                ->where('company_id', $request->id)
                ->where('type', 2)
                ->where('name', $documents)
                ->first();

            $spending = new Spending;
            $spending->name = $request->name;
            $spending->id_voucher_categories = $category->id;
            $spending->state = 2;
            $spending->save();

            $voucher = new Voucher;
            $voucher->id_spending = $spending->id;
            $voucher->amount = $request->amount;
            $voucher->file_img = $image;
            $voucher->state = 1;
            $voucher->sale_report_id = $request->report_id;
            $voucher->currency_id = isset($request->currency_id) ? $request->currency_id : 9;
            $voucher->save();

          }

        } else { // Crear todos los registros
          $category = new VoucherCategories;
          $category->name = $documents;
          $category->state = 2;
          $category->company_id = $request->id;
          $category->type = 2;
          $category->save();

          $spending = new Spending;
          $spending->name = $request->name;
          $spending->id_voucher_categories = $category->id;
          $spending->state = 2;
          $spending->save();

          $voucher = new Voucher;
          $voucher->id_spending = $spending->id;
          $voucher->amount = $request->amount;
          $voucher->file_img = $image;
          $voucher->state = 1;
          $voucher->sale_report_id = $request->report_id;
          $voucher->currency_id = isset($request->currency_id) ? $request->currency_id : 9;
          $voucher->save();
        }

        $report = SaleReport::find($request->report_id);
        $report->url = $image;
        $report->save();

        return response()->json([
            'res' => true,
            'body' => [
                'data' => true
            ],
            'message' => 'Voucher creado correctamente'
        ]);
    }

    /**
     * Guarda el comprobante generado en formato imagen.
     */
    private function storeImage($id, $name, $amount, UploadedFile $file) {
        $extension = $file->getClientOriginalExtension();
        $file_name = $name .'_'. date('mdYhis') .'_'. $amount .'.'. $extension;

        $result = $file->storeAs(strval($id), $file_name, 'public_uploads');
        return $result;
    }

    /**
     * Valida el numero de documento del cliente y verifica si está o no en la base de datos.
     */
    public function validateDocument(Request $request) {
      if (DB::table('cliente')
        ->where('nro_document', $request->document)
        ->exists()
      ) {
        $data = Cliente::where('nro_document', $request->document)
          ->first();

        return response()->json([
            'res' => true,
            'body' => [
                'data' => $data
            ],
            'message' => 'Consultado correctamente'
        ]);
      } else {
        $type = "";
        if (strcmp($request->tipoDoc, "DNI") === 0) {
          $type = "dni";
        } else if (strcmp($request->tipoDoc, "RUC") === 0) {
          $type = "ruc";
        }

        $response = self::requestDocument($request->document, $type);
        if ($response == null) {
          return response()->json([
              'res' => false,
              'message' => 'Consultado correctamente'
          ]);
        } else {
    		  $res = isset($response->success) ? $response->success : false;
    		  $message = isset($response->message) ? $response->message : "";
    		  $data = isset($response->data) ? $response->data : null;

          $body = self::parseDocumentResponse($data, $type);

          return response()->json([
              'res' => $res,
              'body' => [
                  'data' => $body
              ],
              'message' => $message
          ]);
        }
      }
    }

    /**
     * Obtiene la respuesta del api de consulta de documento para responder en el ENDPOINT.
     */
    private function parseDocumentResponse($data, $type) {
        if (strcmp($type, "dni") === 0) {
          return array(
            'id' => 1,
            'type_document' => 1,
            'nro_document' => isset($data) ? $data->dni : "",
            'razon_social' => isset($data) ? $data->fullname : "",
            'email' => "",
            'telefono' => "",
            'direccion' => ""
          );

        } else if (strcmp($type, "ruc") === 0) {
          return array(
            'id' => 1,
            'type_document' => 2,
            'nro_document' => isset($data) ? $data->ruc : "",
            'razon_social' => isset($data) ? $data->name : "",
            'email' => "",
            'telefono' => "",
            'direccion' => isset($data) ? $data->address .' '. $data->district : ""
          );
        }
    }

    /**
     * Consulta el documento en PeruApis.
     */
    private function requestDocument($doc, $type) {
      $token = "5liyWwaAewsC3OqPn2vRHpGn3GPfjLQiTs6ZZRynMK3oKkf7lqFw5EqnNi9i";
      $curl = curl_init();
      // Make Post Fields Array
      $document = [
          'document' => $doc
      ];

      curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.peruapis.com/v1/$type",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30000,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($document),
          CURLOPT_HTTPHEADER => array(
              // Set here requred headers
              "Accept: application/json",
              "content-type: application/json",
              "Authorization: Bearer $token"
          ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
          //echo "cURL Error #:" . $err;
          return null;
      } else {
          //print_r(json_decode($response));
          return json_decode($response);
      }
    }

    /**
     * Crea el array necesario para enviarlo a @GreenterController, que enviará los datos a SUNAT y crear BOLETA DE VENTA o FACTURA.
     */
    private function parseDataToReport(Request $request) {
      $data = array(
        'ublVersion' => '2.1',
        'tipoOperacion' =>  $request->tipoOperacion,
        'tipoDoc' => $request->tipoDoc,
        'serie' => $request->serie,
        'correlativo' => $request->correlativo,
        'fechaEmision' => $request->fechaEmision,
        'formaPago' => array(
          'moneda' => $request->tipoMoneda,
          'tipo' => isset($request->planPago) ? $request->planPago : 'Contado'
        ),
        'tipoMoneda' => $request->tipoMoneda,
        'client' => array(
          'tipoDoc' => $request->client['tipoDoc'],
          'numDoc' => $request->client['numDoc'],
          'rznSocial' => $request->client['rznSocial'],
          'address' => array(
            'direccion' => $request->client['address']['direccion'],
            'provincia' => 'LIMA',
            'departamento' => 'LIMA',
            'distrito' => 'LIMA',
            'ubigueo' => '150101'
          )
        ),
        'company' => array(
          'ruc' => $request->company['ruc'],
          'razonSocial' => $request->company['razonSocial'],
          'address' => array(
            'direccion' => $request->company['address']['direccion'],
            'provincia' => 'LIMA',
            'departamento' => 'LIMA',
            'distrito' => 'LIMA',
            'ubigueo' => '150101'
          )
        ),
        'mtoOperGravadas' => $request->mtoOperGravadas,
        'mtoIGV' => $request->mtoIGV,
        'valorVenta' => $request->valorVenta,
        'totalImpuestos' => $request->totalImpuestos,
        'subTotal' => $request->mtoImpVenta,
        'mtoImpVenta' => $request->mtoImpVenta
      );

      $data['details'] = [];
      $data['legends'] = [];

      foreach ($request->details as $key => $value) {
        array_push($data['details'], $value);
      }

      foreach ($request->legends as $key => $value) {
        array_push($data['legends'], $value);
      }

      return $data;
    }

    /**
     * Crea el array necesario para enviarlo a @GreenterController, que enviará los datos a SUNAT y crear NOTA DE DEBITO o NOTA DE CREDITO.
     */
    private function parseDataToNote(Request $request) {
      $data = array(
        'ublVersion' => '2.1',
        //'tipoOperacion' =>  $request->tipoOperacion,
        'tipoDoc' => $request->tipoDoc,
        'serie' => $request->serie,
        'correlativo' => $request->correlativo,
        'fechaEmision' => $request->fechaEmision,
        'tipDocAfectado' => $request->tipDocAfectado,//: '03',
        'numDocfectado' => $request->numDocfectado,//: 'B001-12',
        'codMotivo' => $request->codMotivo,//: '01',
        'desMotivo' => $request->desMotivo,//: 'ANULACION DE LA OPERACION',
        //'formaPago' => array(
        //  'moneda' => $request->tipoMoneda,se
        //  'tipo' => 'Contado'
        //),
        'tipoMoneda' => $request->tipoMoneda,
        'client' => array(
          'tipoDoc' => $request->client['tipoDoc'],
          'numDoc' => $request->client['numDoc'],
          'rznSocial' => $request->client['rznSocial'],
          'address' => array(
            'direccion' => $request->client['address']['direccion'],
            'provincia' => 'LIMA',
            'departamento' => 'LIMA',
            'distrito' => 'LIMA',
            'ubigueo' => '150101'
          )
        ),
        'company' => array(
          'ruc' => $request->company['ruc'],
          'razonSocial' => $request->company['razonSocial'],
          'nombreComercial' => $request->company['nombreComercial'],
          'address' => array(
            'direccion' => $request->company['address']['direccion'],
            'provincia' => 'LIMA',
            'departamento' => 'LIMA',
            'distrito' => 'LIMA',
            'ubigueo' => '150101'
          )
        ),
        'mtoOperGravadas' => $request->mtoOperGravadas,
        'mtoIGV' => $request->mtoIGV,
        'totalImpuestos' => $request->totalImpuestos,
        'subTotal' => $request->mtoImpVenta,
        'mtoImpVenta' => $request->mtoImpVenta
      );

      $data['details'] = [];
      $data['legends'] = [];

      foreach ($request->details as $key => $value) {
        array_push($data['details'], $value);
      }

      foreach ($request->legends as $key => $value) {
        array_push($data['legends'], $value);
      }
      return $data;
    }

    /**
     * Crea los comprobantes de tipo NOTA DE DEBITO y NOTA DE CREDITO.
     */
    public function noteSend(Request $request) {

      $company = Company::find($request->company_id);
      $data = self::parseDataToNote($request);

      Log::info("------------------------- Send voucher params - Nota de crédito: -------------------------");
      Log::info(json_encode($data));
      Log::info($company->api_token);

      $apiPeru = new ApiPeruController;
      $res = $apiPeru->creditNote($data, $company->api_token);

      if (isset($res)) {

        $success = isset($res->sunatResponse) ? $res->sunatResponse->success : false;
        $message = isset($res->sunatResponse->error) ? $res->sunatResponse->error->message : 'Ejecutado satisfactoriamente';

        Log::info("------------------------- Send voucher response - Nota de crédito: -----------------------");
        Log::info(json_encode($res));

        if ($success) {
          $report = new SaleReport;
          $report->company_id = $request->company_id;
          $report->ublVersion = '2.1';
          $report->tipoOperacion = $request->tipoOperacion;
          $report->tipoDoc = $request->tipoDoc; // nota de credito
          $report->serie = $request->serie; // nota de credito
          $report->correlativo = $request->correlativo; // nota de credito
          $report->fechaEmision = $request->fechaEmision; // nota de credito
          $report->tipoMoneda = $request->tipoMoneda;
          $report->formaPago = 'Contado';
          $report->client_tipo_doc = $request->client['tipoDoc'];
          $report->client_doc = $request->client['numDoc'];
          $report->client_name = $request->client['rznSocial'];
          $report->client_email = isset($request->client['email']) ? $request->client['email'] : "";
          $report->client_phone = isset($request->client['phone']) ? $request->client['phone'] : "";
          $report->client_address = isset($request->client['address']['direccion']) ? $request->client['address']['direccion'] : "";
          $report->company_doc = $request->company['ruc'];
          $report->company_name = $request->company['razonSocial'];
          $report->company_address = $request->company['address']['direccion'];
          $report->operacionGravadas = $request->mtoOperGravadas;
          $report->igv = $request->mtoIGV;
          $report->precio = $request->valorVenta;
          $report->totalImpuestos = $request->totalImpuestos;
          $report->subTotal = $request->subTotal;
          $report->planPago = isset($request->planPago) ? $request->planPago : 'Contado';
          $report->expiration = isset($request->expiration) ? $request->expiration : '';
          $report->legend = $request->legends[0]['value'];
          $report->state = 1;
          $report->report_dest = isset($request->report_id) ? $request->report_id : 0;
          $report->save();

          foreach ($request->details as $key => $value) {
            $detail = new SaleReportDetail;
            $detail->sale_report_id = $report->id;
            $detail->mtoPrecioUnitario = $value['mtoPrecioUnitario'];
            $detail->igv = $value['igv'];
            $detail->mtoValorUnitario = $value['mtoValorUnitario'];
            $detail->mtoValorVenta = $value['mtoValorVenta'];
            $detail->unidad = $value['unidad'];
            $detail->discount = $value['discount'];
            $detail->descripcion = $value['descripcion'];
            $detail->cantidad = $value['cantidad'];
            $detail->idProduct = $value['idProduct'];
            $detail->totalImpuestos = $value['totalImpuestos'];
            $detail->porcentajeIgv = $value['porcentajeIgv'];
            $detail->codProducto = $value['codProducto'];
            $detail->tipAfeIgv = $value['tipAfeIgv'];
            $detail->mtoBaseIgv = $value['mtoBaseIgv'];
            $detail->save();
          }

          $company->voucher_number = $company->voucher_number + 1;
          $company->save();

          return response()->json([
              'res' => $success,
              'body' => [
                  'data' => [
                    "correlativo" => $company->voucher_number + 1,
                    "report_id" => $report->id
                  ]
              ],
              'message' => $message
          ]);

        } else {
          return response()->json([
              'res' => $success,
              'body' => [
                  'data' => null
              ],
              'message' => $message
          ]);

        }
      } else {
        return response()->json([
            'res' => false,
            'body' => [
              'data' => null
            ],
            'message' => 'No se pudo completar la operación'
        ]);
      }
    }

    public function getProductByCode(Request $request) {
        if (!isset($request->company_id)) {
          return response()->json([
              'res' => false,
              'body' => [
                'data' => null
              ],
              'message' => 'No se encontró compañía'
          ]);
        }

        if (!isset($request->code)) {
          return response()->json([
              'res' => false,
              'body' => [
                'data' => null
              ],
              'message' => 'Código de producto inválido'
          ]);
        }

        $product = array(
          "codigosegmento" => "10",
          "descripcionsegmento" => "MATERIAL VIVO VEGETAL Y ANIMAL, ACCESORIOS Y SUMINISTROS",
          "idsegmento" => 1,
          "codigofamilia" => "1010",
          "descripcionfamilia" => "ANIMALES VIVOS",
          "idfamilia" => 1,
          "codigoclase" => "101016",
          "descripcionclase" => "PÁJAROS Y AVES DE CORRAL",
          "idclase" => 2,
          "id" => 8,
          "categoria_id" => 2,
          "nombre" => "LLantas para tractor",
          "precio_venta" => 3000,
          "impuesto" => 1,
          "unidad_medida" => 2,
          "sunat_id" => "657465",
          "cuenta_contable" => 0,
          "created_at" => "2021-03-20 17:24:16",
          "updated_at" => "2021-03-20 17:24:16",
          "company_id" => 8,
          "estado" => 1,
          "cantidad_inicial" => 10,
          "cantidad_minima" => 5,
          "cantidad_actual" => 6
        );

        return response()->json([
            'res' => true,
            'body' => [
              'data' => $product
            ],
            'message' => 'Detalle del producto'
        ]);
    }

}
