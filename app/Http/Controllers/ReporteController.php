<?php

namespace App\Http\Controllers;

use App\Models\Reporte;
use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
use Illuminate\Support\Facades\DB;

class ReporteController extends Controller
{
    public function store(Request $request) {
        $validator = Validator::make($request->all(), 
        [ 
            'company_id' => 'required',
            'file' => 'required|mimes:doc,docx,pdf,txt|max:2048',
            'name' => 'required'
        ]);
        if ($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);                        
        }

        if ($files = $request->file('file')) { 
            //store your file into database
            $reporte = new Reporte();
            // $reporte->file = $file;
            $reporte->name = $request->name;
            $reporte->company_id = $request->company_id;
            $reporte->save();

            //store file into document folder
            $file = $request->file->storeAs('documents', 'PDF_reporte_'.$reporte->id.'.'.$request->file('file')->getClientOriginalExtension(), 'public_uploads');

            $update = Reporte::find($reporte->id);
            $update->file = $file;
            $update->save();
              
            return response()->json([
                'res' => true,
                'body' => [
                    'data' => $reporte
                ],
                'message' => 'Registrado correctamente'
            ]);
        }
    }

    public function listReports(Request $request) {
        $input = $request->all();
        $reportes = DB::table('reportes')
        ->join('company', 'reportes.company_id', '=', 'company.id')
        ->where('reportes.company_id', '=', $input['company_id'])
        ->select('reportes.*', 'company.name as company_name')
        ->get();
        for ($i = 0; $i < count($reportes); $i++) {
            $reportes[$i]->file_url = asset('img/' . $reportes[$i]->file);
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $reportes
            ],
            'message' => 'Consultado correctamente'
        ]);
    }

    public function deleteReport(Request $request) {
        $input = $request->all();
        $reporte = DB::table('reportes')->where('id', '=', $input['id'])->delete();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $reporte
            ],
            'message' => 'Eliminado correctamente'
        ]);
    }
}
