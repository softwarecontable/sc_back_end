<?php

namespace App\Http\Controllers;

use App\Models\Trabajador;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TrabajadorController extends Controller
{
    public function store(Request $request) {
        $input = $request->all();
        $trabajador = DB::table('trabajador')->where('id', '=', $input['id'])->get();
        if (count($trabajador) > 0) {
            unset($input['name']);
            unset($input['name_document']);
            $trabajador = DB::table('trabajador')->where('id', '=', $input['id'])->update($input);
        } else {
            unset($input['id']);
            $trabajador = Trabajador::create($input);
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $trabajador
            ],
            'message' => '>Trabajador creado correctamente'
        ]);
    }

    public function list(Request $request) {
        $input = $request->all();
        $query = DB::table('trabajador')
        ->join('company', 'trabajador.company_id', '=', 'company.id')
        ->where('trabajador.company_id', '=', $input['company_id'])
        ->select('trabajador.*', 'company.name');
        
        if (isset($input['nro_document'])) {
            $query->where('nro_document', '=', $input['nro_document']);
        }
        $trabajadores = $query->get();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $trabajadores
            ],
            'message' => 'Consultado correctamente'
        ]);
    }

    public function delete(Request $request) {
        $input = $request->all();
        $trabajador = DB::table('trabajador')->where('id', '=', $input['id'])->delete();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $trabajador
            ],
            'message' => 'Eliminado correctamente'
        ]);
    }
}
