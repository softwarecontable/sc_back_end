<?php

namespace App\Http\Controllers;

use App\Models\Compra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompraController extends Controller
{
    public function store(Request $request) {
        $input = $request->all();
        $compra = DB::table('compra')->where('id_movimiento', '=', $input['id_movimiento'])->get();
        if (count($compra) > 0) {
            $compra = DB::table('compra')->where('id_movimiento', '=', $input['id_movimiento'])->update($input);
        } else {
            unset($input['id']);
            $compra = Compra::create($input);
        }       
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $compra
            ],
            'message' => 'Compra creada correctamente'
        ]); 
    }

    public function listByMovement(Request $request) {
        $input = $request->all();
        $compra = DB::table('compra')->where('id_movimiento', '=', $input['id_movimiento'])->get();
        if (count($compra) == 0) {
            $secuencia = DB::table('compra')
            ->join('bank_movements', 'compra.id_movimiento', 'bank_movements.id')
            ->join('company_bank', 'bank_movements.bank_id', 'company_bank.bank_id')
            ->where('company_bank.company_id', '=', $input['company_id'])
            ->orderBy('compra.secuencia', 'desc')
            ->select('compra.secuencia', 'company_bank.*')
            ->get();
            if (count($secuencia) > 0) {
                $compra['secuencia'] = $secuencia[0]->secuencia + 1;
            } else {
                $compra['secuencia'] = 1;
            }
            return response()->json([
                'res' => false,
                'body' => [
                    'data' => $compra
                ],
                'message' => 'Consultado correctamente'
            ]);
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $compra
            ],
            'message' => 'Consultado correctamente'
        ]); 
    }

    public function deleteCompra(Request $request) {
        $input = $request->all();
        $compra_detalle = DB::table('compra_detalle')->where('compra_id', '=', $request['id_compra'])->get();
        for ($i = 0; $i < count($compra_detalle); $i++) {
            $detalle = DB::table('compra_detalle')->where('id', '=', $compra_detalle[$i]->id)->delete();
        }
        $compra = DB::table('compra')->where('id', '=', $input['id_compra'])->delete();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $compra
            ],
            'message' => 'Compra eliminada correctamente'
        ]); 
    }
}
