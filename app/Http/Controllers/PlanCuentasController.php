<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Imports\PlanCuentasImport;
use Maatwebsite\Excel\Facades\Excel;

class PlanCuentasController extends Controller
{
    public function searchByCod(Request $request) {
        $input = $request->all();
        $plan_cuenta = DB::table('plan_cuentas')->where('codigo', '=', $input['cod'])->get();
        if (count($plan_cuenta) > 0) {
            return response()->json([
                'res' => true,
                'body' => [
                    'data' => $plan_cuenta
                ],
                'message' => 'Consultado correctamente'
            ]);
        } else {
            return response()->json([
                'res' => false,
                'body' => [
                    'data' => []
                ],
                'message' => 'Consultado correctamente'
            ]);
        }
    }

    public function importSavePlanCuentas(Request $request) {
        $data = [
            'company_id' => $request->get('company_id')
        ];
        $import = new PlanCuentasImport($data);
        Excel::import($import, $request->file('file_excel'));
        return response()->json([
            'res' => true,
            'body' => [
                'data' => []
            ],
            'message' => 'Importacion correcta'
        ]);
    }
}
