<?php

namespace App\Http\Controllers;

use App\Models\TypeDocument;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class AnalyticController extends Controller
{

    public function getReport($id, $start = "", $end = "") {

        // Days:

        $today = Carbon::today();
        $yesterday = Carbon::yesterday();
        $firstDay = Carbon::today()->subDays(2);
        $last = Carbon::today();

        if (!empty($start)) {
            $today = Carbon::createFromFormat('Y-m-d', $end);
            $yesterday = Carbon::createFromFormat('Y-m-d', $end)->sub(1, 'day');
            $firstDay = Carbon::createFromFormat('Y-m-d', $end)->sub(2, 'day');
            $last = Carbon::createFromFormat('Y-m-d', $start);
        }

        $today = $today->format('Y-m-d');
        $yesterday = $yesterday->format('Y-m-d');
        $firstDay = $firstDay->format('Y-m-d');
        $last = $last->format('Y-m-d');

        // ------------ Para determinar el porcentaje de conciliación total ----------
        $allMovements = DB::table('bank_movements as bm')
            ->join('company_bank as cb', 'bm.bank_id', '=', 'cb.bank_id')
            ->where('cb.company_id', $id)
            ->whereDate('bm.created_at', '<=', $today)
            ->whereDate('bm.created_at', '>=', $last)
            ->count();
        $conciliateMovements = DB::table('bank_movements as bm')
            ->join('company_bank as cb', 'bm.bank_id', '=', 'cb.bank_id')
            ->where('cb.company_id', $id)
            ->whereDate('bm.created_at', '<=', $today)
            ->whereDate('bm.created_at', '>=', $last)
            ->where('bm.state', 2) // Significa que el movimiento ha sido conciliado
            ->count();

        $conciliation = 0;
        if ($allMovements > 0) {
            $conciliation = round($conciliateMovements / $allMovements);
        }

        // ------------ Para determinar la variación de ventas por comprobantes - NUEVO  ----------

        $currentSale = DB::select('call sp_get_sale_by_range_date(?,?,?)', array($id, $today, $last));
        // $pastSale = DB::select('call sp_get_sale_by_date(?,?)', array($id, $yesterday));

        // foreach ($currentSale as $key => $value) {
        //     $value->variance = self::getVariance($value->total, $pastSale[$key]->total);
        // }

        // ------------ Para las cobranzas - NUEVO  ----------

        $collection = DB::select('call sp_get_collection_date(?,?,?)', array($id, $today, $last));

        // ------------ Para determinar la variación de ventas en el día ----------
        $existsSaleDay = false;
        $docs = [TypeDocument::NOTA_VENTA, TypeDocument::FACTURA, TypeDocument::BOLETA_VENTA]; // Agregar Notas de credito y debito

        $saleDayProducts = DB::table('sale_report')
            ->where('company_id', $id)
            ->whereIn('tipoDoc', $docs)
            ->whereDate('created_at', Carbon::today())
            ->groupBy('company_id')
            ->select(DB::raw('count(*) as cantidad, sum(subTotal) as total'))
            ->first();

        $salePastProducts = DB::table('sale_report')
            ->where('company_id', $id)
            ->whereIn('tipoDoc', $docs)
            ->whereDate('created_at', Carbon::yesterday())
            ->groupBy('company_id')
            ->select(DB::raw('count(*) as cantidad, sum(subTotal) as total'))
            ->first();

        $existsSaleDay = isset($saleDayProducts->cantidad) ? $saleDayProducts->cantidad > 0 : false;

        if (!isset($saleDayProducts->cantidad)) {
            $saleDayProducts = (object) [
                'cantidad' => 0,
                'variance' => 0,
                'total'    => "0.0"
            ];
        }

        if (!isset($salePastProducts->cantidad)) {
            $salePastProducts = (object) [
                'cantidad' => 0,
                'variance' => 0,
                'total'    => "0.0"
            ];
        }

        $advance = 0;
        if ($salePastProducts->cantidad == 0 && $saleDayProducts->cantidad > 0) {
            $advance = 100;
        } else if ($salePastProducts->cantidad > 0 && $saleDayProducts->cantidad > 0) {
            $advance = round($saleDayProducts->cantidad / $salePastProducts->cantidad) * 100;
        }

        $saleDayProducts->variance = $advance;

        $sales = DB::table('sale_report')
            ->where('company_id', $id)
            ->orderBy('id', 'desc')
            ->select(DB::raw('DATE_FORMAT(created_at, "%H:%i %p") as time'))
            ->first();

        $lastSale = $sales ? $sales->time : '';

        // ------------ Detalle de los ingresos/egresos de los últimos 3 días ----------

        $voucherInputs = DB::select('call sp_get_voucher_ins(?,?,?,?)', array($id, $today, $yesterday, $firstDay));
        $voucherOutputs = DB::select('call sp_get_voucher_output(?,?,?,?)', array($id, $today, $yesterday, $firstDay));

        // ------------ 10 ítems más vendidos del mes ----------

        $topItemsQuery = DB::table('sale_report_detail as srd')
            ->join('sale_report as sr', 'sr.id', '=', 'srd.sale_report_id')
            ->groupBy(DB::raw('srd.idProduct, srd.descripcion'))
            ->where('sr.company_id', $id);

        if (!empty($start)) {
            $topItemsQuery->whereDate('sr.created_at','>=', $last)
                ->whereDate('sr.created_at','<=', $today);
        } else {
            $topItemsQuery->whereDate('sr.created_at','=', $today);
        }

        $topItems = $topItemsQuery
            ->select(DB::raw('COUNT(*) as cantidad, SUM(srd.mtoValorVenta + srd.igv) as monto, srd.descripcion, SUM(srd.cantidad) as cantidad_actual'))
            ->orderBy('cantidad_actual', 'desc')
            ->limit(10)
            ->get();

        // ------------ Gastos del mes ----------

        $monthWasteQuery = DB::table('voucher_categories as vc')
            ->join('spending as s', 's.id_voucher_categories', '=', 'vc.id')
            ->join('voucher as v', 'v.id_spending', '=', 's.id')
            ->where('vc.company_id', $id)
            ->where('vc.type', 1) // Egresos
            ->whereIn('v.state', [1, 2]); // No tiene en cuenta comprobantes eliminados

        if (!empty($start)) {
            $monthWasteQuery->whereDate('v.created_at','>=', $last)
                ->whereDate('v.created_at','<=', $today);
        } else {
            $monthWasteQuery->whereDate('v.created_at','=', $today);
        }

        $monthWaste = $monthWasteQuery
            ->groupBy(DB::raw('vc.name'))
            ->select(DB::raw('SUM(v.amount) as monto, vc.name as nombre'))
            ->get();

        // Mover la fecha para que sea general. ✓
        // Reporte de cobranza, diseño Mario.
        // Modificar unidades en 5 ítems vendidos. Cambiar a 10 items. ✓
        // Agregar fecha en grafico de ingresos y egresos. ✓
        // Ventas del día, desglosar detalle de comprobante.
        //  Boletas de venta
        //  Facturas
        //  Notas de venta
        //  TOTAL

        return response()->json([
            'res' => true,
            'body' => [
                'data' => [
                    'conciliation' => $conciliation,
                    'sale_day' => [
                        'show' => $existsSaleDay,
                        'summary' => $saleDayProducts,
                        'last_sale' => $lastSale,
                        'summaries' => $currentSale
                    ],
                    'voucher_movements_in' => $voucherInputs,
                    'voucher_movements_out' => $voucherOutputs,
                    'top_items' => $topItems,
                    'month_waste' => $monthWaste,
                    'collection' => $collection
                ]
            ],
            'message' => 'Consultado correctamente'
        ]);
    }

    public function getSaleReportByDate($id, $start, $end) {
        $sale = DB::select("SELECT count(*) as cantidad, sum(subTotal) as total FROM sale_report WHERE company_id = $id AND DATE(created_at) BETWEEN '" .$start. "' AND '" .$end. "'");

        return response()->json([
            'res' => true,
            'body' => [
                'data' => [
                  	'cantidad' => $sale[0]->cantidad,
	                'total'    => $sale[0]->total,
                ]
            ],
            'message' => 'Consultado correctamente'
        ]);
    }

    private function getVariance($current, $past): float {
        if (!isset($current) && !isset($past)) {
            return 0.0;
        } else if (!isset($current) && isset($past)) {
            return -100.0;
        } else if (isset($current) && !isset($past)) {
            return 100.0;
        } else {
            return (($current - $past) / $past) * 100.0;
        }
    }

}
