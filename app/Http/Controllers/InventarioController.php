<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use App\Models\Inventario;
use App\Imports\ProductImport;
use App\Models\InventarioMovimiento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class InventarioController extends Controller
{
    public function import(Request $request) {

        if (!$request->hasFile('file_excel')) {
          return response()->json([
            'res' => false,
            'body' => [
                'data' => null
            ],
            'message' => 'No se encontró el archivo'
            ]);
        }

        $data = ['company_id' => $request->get('company_id')];
        $import = new ProductImport($data);
        $path1 = $request->file('file_excel')->store('temp');
        $path = storage_path('app').'/'.$path1;
        Excel::import($import, $path);
        //Excel::import($import, $request->file('file_excel'));
        return response()->json([
            'res' => true,
            'body' => [
                'data' => null
            ],
            'message' => 'Productos cargados a su inventario'
        ]);
      }

    public function store(Request $request) {
        $input = $request->all();
        Log::info($input);
        if (!isset($input['sunat_id'])) {
            $input['sunat_id'] = "0";
        }
        $producto = Producto::create($input);
        if (isset($input['flag_inventario']) && $input['flag_inventario'] == 1) {
            $input['producto_id'] = $producto->id;
            $input['cantidad_actual'] = $input['cantidad_inicial'];
            $inventario = Inventario::create($input);
            $input['inventario_id'] = $inventario->id;
            $input['cantidad'] = $input['cantidad_inicial'];
            $inventarioMovimiento = InventarioMovimiento::create($input);

            $producto->cantidad_inicial = $input['cantidad_inicial'];
            $producto->cantidad_actual = $input['cantidad_inicial'];
            $producto->cantidad_minima = $input['cantidad_minima'];
            $producto->cantidad_maxima = $input['cantidad_maxima'];
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $producto
            ],
            'message' => 'Producto registrado correctamente'
        ]);
    }

    public function edit(Request $request) {
      $producto = Producto::find($request->id);
      $producto->categoria_id = $request->categoria_id;
      $producto->nombre = $request->nombre;
      $producto->precio_venta = $request->precio_venta;
      $producto->impuesto = $request->impuesto;
      $producto->unidad_medida = $request->unidad_medida;
      $producto->sunat_id = $request->sunat_id;
      $producto->cuenta_contable = $request->cuenta_contable;
      $producto->company_id = $request->company_id;
      $producto->save();

      // Se va a actualizar el precio del producto también en la tabla `producto`
      DB::table('product_store')
        ->where('product_id', $request->id)
        ->update([
            'price' => $request->precio_venta
        ]);

      $has_inventario = DB::table('inventario')->where('producto_id', $request->id)->exists();
      $want_inventario = $request->flag_inventario == 1;

      if ($want_inventario && $has_inventario) {
        // actualizar inventario
        DB::table('inventario')
          ->where('producto_id', $request->id)
          ->update([
            'cantidad_actual' => $request->cantidad_actual,
            'cantidad_minima' => $request->cantidad_minima,
            'cantidad_maxima' => $request->cantidad_maxima
          ]);
        DB::table('inventario_movimiento')
          ->where('producto_id', $request->id)
          ->update([
            'cantidad' => $request->cantidad_actual
          ]);

          $producto->cantidad_inicial = $request->cantidad_inicial;
          $producto->cantidad_actual = $request->cantidad_actual;
          $producto->cantidad_minima = $request->cantidad_minima;
          $producto->cantidad_maxima = $request->cantidad_maxima;
      } else if ($want_inventario && !$has_inventario) {
        // add inventario
        $input = $request->all();
        $input['producto_id'] = $producto->id;
        $input['cantidad_actual'] = $input['cantidad_inicial'];
        $inventario = Inventario::create($input);
        $input['inventario_id'] = $inventario->id;
        $input['cantidad'] = $input['cantidad_inicial'];
        $inventarioMovimiento = InventarioMovimiento::create($input);

        $producto->cantidad_inicial = $request->cantidad_inicial;
        $producto->cantidad_actual = $request->cantidad_actual;
        $producto->cantidad_minima = $request->cantidad_minima;
        $producto->cantidad_maxima = $request->cantidad_maxima;
      } else if (!$want_inventario && $has_inventario) {
        // remove inventario
        DB::table('inventario')
          ->where('producto_id', $request->id)
          ->delete();
        DB::table('inventario_movimiento')
          ->where('producto_id', $request->id)
          ->delete();
      }

      return response()->json([
          'res' => true,
          'body' => [
              'data' => $producto
          ],
          'message' => 'Producto actualizado correctamente'
      ]);
    }

    public function delete(Request $request) {
        DB::table('producto')
            ->where('id', $request->id)
            ->update([
                'estado' => 0
            ]);

        return response()->json([
            'res' => true,
            'body' => [
                'data' => true
            ],
            'message' => 'Producto eliminado correctamente'
        ]);
    }

    public function addInventario(Request $request) {
        $input = $request->all();
        $input['cantidad_actual'] = $input['cantidad_inicial'];
        $inventario = Inventario::create($input);
        $input['inventario_id'] = $inventario->id;
        $input['cantidad'] = $input['cantidad_inicial'];
        $inventarioMovimiento = InventarioMovimiento::create($input);
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $inventario
            ],
            'message' => 'Inventario creado correctamente'
        ]);
    }
}
