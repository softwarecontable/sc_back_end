<?php

namespace App\Http\Controllers;

use App\Models\Proveedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProveedorController extends Controller
{
    public function store(Request $request) {
        $input = $request->all();
        $proveedor = DB::table('proveedor')->where('id', '=', $input['id'])->get();
        if (count($proveedor) > 0) {
            unset($input['name']);
            unset($input['name_document']);
            $proveedor = DB::table('proveedor')->where('id', '=', $input['id'])->update($input);
        } else {
            unset($input['id']);
            $proveedor = Proveedor::create($input);
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $proveedor
            ],
            'message' => 'Proveedor creado correctamente'
        ]);
    }

    public function list(Request $request) {
        $input = $request->all();
        $query = DB::table('proveedor')
        ->join('company', 'proveedor.company_id', '=', 'company.id')
        ->where('proveedor.company_id', '=', $input['company_id'])
        ->select('proveedor.*', 'company.name');

        if (isset($input['nro_document'])) {
            $query->where('nro_document', '=', $input['nro_document']);
        }
        $proveedores = $query->get();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $proveedores
            ],
            'message' => 'Consultado correctamente'
        ]);
    }

    public function delete(Request $request) {
        $input = $request->all();
        $proveedor = DB::table('proveedor')->where('id', '=', $input['id'])->delete();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $proveedor
            ],
            'message' => 'Eliminado correctamente'
        ]);
    }
}
