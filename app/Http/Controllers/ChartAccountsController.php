<?php

namespace App\Http\Controllers;

use App\Models\ChartAccounts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ChartAccountsController extends Controller
{
    public function store(Request $request) {
        $input = $request->all();
        $chartAccount = DB::table('plan_cuentas')->where('id', '=', $input['id'])->get();
        if (count($chartAccount) > 0) {
            unset($input['name']);
            $chartAccount = DB::table('plan_cuentas')->where('id', '=', $input['id'])->update($input);
        } else {
            unset($input['id']);
            $chartAccount = ChartAccounts::create($input);
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $chartAccount
            ],
            'message' => 'Item creado correctamente'
        ]);
    }

    public function list(Request $request) {
        $input = $request->all();
        $query = DB::table('plan_cuentas')
        ->join('company', 'plan_cuentas.company_id', '=', 'company.id')
        ->where('plan_cuentas.company_id', '=', $input['company_id'])
        ->select('plan_cuentas.*', 'company.name');

        if (isset($input['codigo'])) {
            $query->where('codigo', '=', $input['codigo']);
        }
        $chartAccounts = $query->get();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $chartAccounts
            ],
            'message' => 'Consultado correctamente'
        ]);
    }

    public function delete(Request $request) {
        $input = $request->all();
        $chartAccount = DB::table('plan_cuentas')->where('id', '=', $input['id'])->delete();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $chartAccount
            ],
            'message' => 'Eliminado correctamente'
        ]);
    }
}
