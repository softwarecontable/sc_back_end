<?php

namespace App\Http\Controllers;

use App\Models\Exchange;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExchangeController extends Controller
{
    public function store(Request $request) {
        $input = $request->all();
        $exchange = DB::table('exchange')->where('fecha', '=', $input['fecha'])->get();
        if (count($exchange) > 0) {
            return response()->json([
                'res' => false,
                'body' => [
                    'data' => []
                ],
                'message' => 'Ya existe uno creado'
            ]);
        } else {
            $exchangenew = Exchange::create($input);
            return response()->json([
                'res' => true,
                'body' => [
                    'data' => $exchangenew
                ],
                'message' => 'Exchange creado correctamente'
            ]);
        }   
    }

    public function list(Request $request) {
        $input = $request->all();
        $exchange = DB::table('exchange')->where('fecha', '=', $input['fecha'])->get();
        if (count($exchange) > 0) {
            return response()->json([
                'res' => true,
                'body' => [
                    'data' => $exchange
                ],
                'message' => 'Consultado correctamente'
            ]);
        } else {
            return response()->json([
                'res' => false,
                'body' => [
                    'data' => $exchange
                ],
                'message' => 'Consultado correctamente'
            ]);
        }
    }
}
