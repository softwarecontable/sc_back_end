<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class EncryptionController extends Controller
{

    /**
     * Encrypts any string that is sent.
     */
    public function encrypt($string)
   {
        $encrypted = Crypt::encryptString($string);
        return $encrypted;
   }

    /**
     * Decrypts any string that is sent.
     */
   public function decrypt($string)
   {
        $decrypt = Crypt::decryptString($string);
        return $decrypt;
   }

}
