<?php

namespace App\Http\Controllers;

use App\Models\CostCenter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CostCenterController extends Controller
{
    public function store(Request $request) {
        $input = $request->all();
        $costCenter = DB::table('centro_costos')->where('id', '=', $input['id'])->get();
        if (count($costCenter) > 0) {
            unset($input['name']);
            $costCenter = DB::table('centro_costos')->where('id', '=', $input['id'])->update($input);
        } else {
            unset($input['id']);
            $costCenter = CostCenter::create($input);
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $costCenter
            ],
            'message' => 'Item creado correctamente'
        ]);
    }

    public function list(Request $request) {
        $input = $request->all();
        $query = DB::table('centro_costos')
        ->join('company', 'centro_costos.company_id', '=', 'company.id')
        ->where('centro_costos.company_id', '=', $input['company_id'])
        ->select('centro_costos.*', 'company.name');

        if (isset($input['codigo'])) {
            $query->where('codigo', '=', $input['codigo']);
        }
        $costCenters = $query->get();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $costCenters
            ],
            'message' => 'Consultado correctamente'
        ]);
    }

    public function delete(Request $request) {
        $input = $request->all();
        $costCenter = DB::table('centro_costos')->where('id', '=', $input['id'])->delete();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $costCenter
            ],
            'message' => 'Eliminado correctamente'
        ]);
    }
}
