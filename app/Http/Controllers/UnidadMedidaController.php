<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class UnidadMedidaController extends Controller
{
    public function list(Request $request) {
        $unidad_medida = DB::table('unidad_medida')->get();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $unidad_medida
            ],
            'message' => 'Consultado correctamente'
        ]);
    }
}
