<?php

namespace App\Http\Controllers;

use App\Models\BankMovement;
use App\Models\Bank;
use Illuminate\Http\Request;
use App\Imports\BankMovementImport;
use App\Imports\BankScotiaMovementImport;
use App\Imports\BankBbvaMovementImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use App\Models\MovementsAudio;
use App\Models\Audio;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\UploadedFile;

class BankMovementController extends Controller
{
    public function previewImport(Request $request) {
        $data = [
            'bank_id' => $request->get('bank_id'),
            'bank_catalog_id' => $request->get('bank_catalog_id')
        ];
        switch ($data['bank_catalog_id']) {
            case '15':
                $import = new BankMovementImport($data);
                Excel::import($import, $request->file('file_excel'));
                return response()->json([
                    'res' => true,
                    'body' => [
                        'data' => $import->responseArray()
                    ],
                    'message' => 'Consulta vista previa'
                ]);
                break;
            case '17':
                $import = new BankScotiaMovementImport($data);
                Excel::import($import, $request->file('file_excel'));
                return response()->json([
                    'res' => true,
                    'body' => [
                        'data' => $import->responseArray()
                    ],
                    'message' => 'Consulta vista previa'
                ]);
                break;
            case '16':
                $import = new BankBbvaMovementImport($data);
                Excel::import($import, $request->file('file_excel'));
                return response()->json([
                    'res' => true,
                    'body' => [
                        'data' => $import->responseArray()
                    ],
                    'message' => 'Consulta vista previa'
                ]);
                break;
            case '14':
                return response()->json([
                    'res' => false,
                    'message' => 'Intente con el formato genérico.'
                ]);
                break;
            default:
                $import = new BankMovementImport($data);
                Excel::import($import, $request->file('file_excel'));
                return response()->json([
                    'res' => true,
                    'body' => [
                        'data' => $import->responseArray()
                    ],
                    'message' => 'Consulta vista previa'
                ]);
                break;
        }
    }
    public function import(Request $request) {
        $input = $request->all();

        if (empty($input['arrayMovements'])) {
    			return response()->json([
    				'res' => false,
    				'message' => 'Importación incorrecta'
    			]);
    		}

    		$bank = Bank::find($input['arrayMovements'][0]['bank_id']);

        for ($i = 0; $i < count($input['arrayMovements']); $i++) {
            BankMovement::create([
                'date' => $input['arrayMovements'][$i]['date'],
                'concept' => $input['arrayMovements'][$i]['concept'],
                'amount' => $input['arrayMovements'][$i]['amount'],
                'bank_id' => $input['arrayMovements'][$i]['bank_id'],
                'state' => 1
            ]);

    			if(doubleval($input['arrayMovements'][$i]['amount']) > 0) {
    				$bank->income = $bank->income + $input['arrayMovements'][$i]['amount'];
    			} else {
    				$bank->expenditure = $bank->expenditure + $input['arrayMovements'][$i]['amount'];
    			}
        }

    		$bank->save();
        return response()->json([
            'res' => true,
            'message' => 'Importación correcta'
        ]);
    }

    public function store(Request $request) {
        $input = $request->all();

        if (!isset($input['date']) || empty($input['date'])) {
            return response()->json([
                'res' => false,
                'message' => 'Debe ingresar la fecha del registro'
            ]);
        }

        if (!isset($input['concept']) || empty($input['concept'])) {
            return response()->json([
                'res' => false,
                'message' => 'Debe ingresar la descripción del movimiento'
            ]);
        }

        if (!isset($input['amount']) || empty($input['amount'])) {
            return response()->json([
                'res' => false,
                'message' => 'Debe ingresar el monto del movimiento'
            ]);
        }

        if (!isset($input['bank_id']) || $input['bank_id'] == 0) {
            return response()->json([
                'res' => false,
                'message' => 'El movimiento no está asociado con ningún banco'
            ]);
        }

        $data = BankMovement::create($input);
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $data
            ],
            'message' => 'Movimiento bancario creado correctamente'
        ]);
    }

    public function delete(Request $request) {

      if (DB::table('movements_voucher')->where('id_bank_movements', $request->id)->exists()) {
        return response()->json([
            'res' => false,
            'message' => 'El movimiento ya tiene comprobantes asociados'
        ]);
      }

      $mov = BankMovement::find($request->id);
      $bank = Bank::find($mov->bank_id);

      if (doubleval($mov->amount) < 0) {
        $bank->expenditure = $bank->expenditure - doubleval($mov->amount);
      } else {
        $bank->income = $bank->income - doubleval($mov->amount);
      }
      $bank->save();
      $mov->delete();

      return response()->json([
          'res' => true,
          'body' => [
              'data' => true
          ],
          'message' => 'Eliminado correctamente'
      ]);
    }

    public function storeAudio(Request $request) {
      $name = 'Mov_'. $request->id .'_'. date('mdYhis');
      $url = self::addAudio($name, $request->file('file_img'));

      $audio = new Audio;
      $audio->name = $name;
      $audio->url = $url;
      $audio->state = 1;
      $audio->save();

      $link = new MovementsAudio;
      $link->bank_movement_id = $request->id;
      $link->audio_id = $audio->id;
      $link->state = 1;
      $link->save();

      return response()->json([
          'res' => true,
          'body' => [
              'data' => true
          ],
          'message' => 'Audio guardado correctamente'
      ]);
    }

    public function addAudio($name, UploadedFile $file) {
      $extension = $file->getClientOriginalExtension();

      $file_name = $name .'.'. $extension;

      $result = $file->storeAs('audio', $file_name, 'public_uploads');
      return $result;
    }

    public function list(Request $request) {
        $input = $request->all();

        $query = DB::table('bank_movements')
        ->where('bank_id', '=', $input['bank_id']);

        if (isset($input['filter'])) {
          $query->where('state', '=', $input['filter']);
        }

        $bankMovements = $query->get();

        foreach ($bankMovements as $key => $value) {
          $value->associated = DB::table('movements_voucher')
            ->where('id_bank_movements', $value->id)
            ->sum('amount');
        }

        return response()->json([
            'res' => true,
            'body' => [
                'data' => $bankMovements
            ],
            'message' => 'Consultado correctamente'
        ]);
    }

    public function listBackOffice(Request $request) {
        $input = $request->all();
        $movements = DB::select('call sp_getMovementsBackOffice(?,?,?)', array($input['bank_id'], $input['fechaInit'], $input['fechaEnd']));
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $movements
            ],
            'message' => 'Consultado correctamente'
        ]);
    }

    public function listTransactionByMovement(Request $request) {
        $input = $request->all();
        for ($i = 0; $i < count($input['movements']); $i++) {
            if ($input['movements'][$i]['type'] === 'Egreso') {
                $compra = DB::table('compra')->where('id_movimiento', '=', $input['movements'][$i]['id'])->get();
                if (count($compra) > 0) {
                    $input['movements'][$i]['totalTrans'] = $compra[0]->total;
                } else {
                    $input['movements'][$i]['totalTrans'] = 0;
                }
            } else {
                $venta = DB::table('venta')->where('id_movimiento', '=', $input['movements'][$i]['id'])->get();
                if (count($venta) > 0) {
                    $input['movements'][$i]['totalTrans'] = $venta[0]->total;
                } else {
                    $input['movements'][$i]['totalTrans'] = 0;
                }
            }
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $input
            ],
            'message' => 'Consultado correctamente'
        ]);
    }
}
