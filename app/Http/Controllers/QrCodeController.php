<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QrCodeController extends Controller
{

    protected EncryptionController $encrypt;

    public function __construct(EncryptionController $encrypt) {
        $this->encrypt = $encrypt;
    }

    public function test() {
        $qr = self::billQrCode("/8/invoiceData.pdf");
        return view("qr", ["qrcode" => $qr]);
    }

    /**
     * @method billQrCode() crea el código QR del documento.
     * @param string $url es el enlace del archivo.
     */
    public function billQrCode(string $url) {
        $text = asset("api/showBill") ."/". $this->encrypt->encrypt($url);
        return QrCode::size(120)
            ->margin(1)
            ->generate($text);
    }

}
