<?php

namespace App\Http\Controllers;

use App\Models\CompraDetalle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompraDetalleController extends Controller
{
    public function store(Request $request) {
        $input = $request->all();
        $compra_detalle = DB::table('compra_detalle')->where('id', '=', $input['id'])->get();
        if (count($compra_detalle) > 0) {
            $compra_detalle = DB::table('compra_detalle')->where('id', '=', $input['id'])->update($input);
        } else {
            $compra_detalle = CompraDetalle::create($input);
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $compra_detalle
            ],
            'message' => 'Detalle compra creada correctamente'
        ]);
    }

    public function listDetalleCompra(Request $request) {
        $input = $request->all();
        $compra = DB::table('compra')
        ->where('id_movimiento', '=', $input['movimiento_id'])
        ->orderBy('secuencia', 'asc')
        ->get();
        if (count($compra) > 0) {
            $compra_detalle = DB::table('compra_detalle')->where('compra_id', '=', $compra[0]->id)->get();
        } else {
            $compra_detalle = [];
        }
        
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $compra_detalle
            ],
            'message' => 'Consultado correctamente'
        ]); 
    }

    public function deleteDetalleCompra(Request $request) {
        $input = $request->all();
        $compra_detalle = DB::table('compra_detalle')->where('id', '=', $input['id'])->delete();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $compra_detalle
            ],
            'message' => 'Eliminado correctamente'
        ]); 
    }
}
