<?php

namespace App\Http\Controllers;

use App\Models\Parameter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{

  public function contact(Request $request) {
    $destiny = Parameter::where('group_id', 'contact_email')->first()->value;

    $user = array(
      'name' => $request->name,
      'phone' => $request->phone,
      'email' => $request->email,
      'body' => $request->body
    );

    $message = view('contacto')->with(['user'=>$user]);

    // Always set content-type when sending HTML email
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

    // More headers
    $headers .= 'From: <develop@zumar.pe>' . "\r\n";
    //$headers .= 'Cc: walter.cojal@gmail.com' . "\r\n";

    mail($destiny,'¡Contáctanos!',$message,$headers);

    return response()->json([
        'res' => true,
        'body' => [
            'data' => true
        ],
        'message' => 'Correo enviado correctamente'
    ]);

  }

}
