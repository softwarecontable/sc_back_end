<?php

namespace App\Http\Controllers;

use App\Models\CategoriaInventario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriaInventarioController extends Controller
{
    public function store(Request $request) {
        $input = $request->all();
        $categoriaInventario = DB::table('categoria_inventario')->where('id', '=', $input['id'])->get();
        if (count($categoriaInventario) > 0) {
            $categoriaInventario = DB::table('categoria_inventario')->where('id', '=', $input['id'])->update($input);
        } else {
            unset($input['id']);
            $categoriaInventario = CategoriaInventario::create($input);
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $categoriaInventario
            ],
            'message' => 'Categoria inventario creado correctamente'
        ]);
    }

    public function listByPadre(Request $request) {
        $input = $request->all();
        $categoriaInventario = DB::table('categoria_inventario')
        ->where('id_padre', '=', $input['id_padre'])
        ->where('company_id', '=', $input['company_id'])
        ->get();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $categoriaInventario
            ],
            'message' => 'Consultado correctamente'
        ]);
    }

    public function deleteCatInv(Request $request) {
        $input = $request->all();
        $categoriaInventario = DB::table('categoria_inventario')
        ->where('id_padre', '=', $input['id'])
        ->get();
        for ($i = 0; $i < count($categoriaInventario); $i++) {
            $deleteCat = DB::table('categoria_inventario')->where('id', '=', $categoriaInventario[$i]->id)->delete();
        }
        $deleteCatInv = DB::table('categoria_inventario')->where('id', '=', $input['id'])->delete();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $deleteCatInv
            ],
            'message' => 'Eliminado correctamente'
        ]);
    }

    public function getAllSegmento(Request $request) {
        $input = $request->all();
        $segmentos = DB::table('segmento')->get();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $segmentos
            ],
            'message' => 'Consultado correctamente'
        ]);
    }

    public function getFamiliaBySegmento(Request $request) {
        $input = $request->all();
        $query = DB::table('familia');
        if ($input['id_segmento']) {
            $segment = DB::table('segmento')->where('id_segmento', $input['id_segmento'])->first();
            $query->where('id_segmento', '=', $segment->codigo);
        }
        $familias = $query->get();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $familias
            ],
            'message' => 'Consultado correctamente'
        ]);
    }

    public function getClaseByFamilia(Request $request) {
        $input = $request->all();
        $query = DB::table('clase');
        if ($input['familia_id']) {
            $family = DB::table('familia')->where('id_familia', $input['familia_id'])->first();
            $query->where('familia_id', '=', $family->codigo);
        }
        $clases = $query->get();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $clases
            ],
            'message' => 'Consultado correctamente'
        ]);
    }
}
