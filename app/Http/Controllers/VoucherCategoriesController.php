<?php

namespace App\Http\Controllers;

use App\Models\VoucherCategories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class VoucherCategoriesController extends Controller
{
    public function store(Request $request) {
        $input = $request->all();

        // $count = DB::table('voucher_categories')
        //   ->where('company_id', $input['company_id'])
        //   ->where('type', $input['type'])
        //   ->where('state', 1)
        //   ->count();
        //
        // Log::info("# categorías: " .$count);
        //
        // if ($count >= 10) {
        //   return response()->json([
        //     'res' => false,
        //     'message' => 'Estimado usuario, solo puede crear un máximo de 10 categorías'
        //     ]);
        // }

        $voucherCategories = VoucherCategories::create($input);
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $voucherCategories
            ],
            'message' => 'Categoría creada correctamente'
        ]);
    }

    public function edit(Request $request) {
      $voucher = VoucherCategories::find($request->id);

      if ($voucher->state == 2) {
        return response()->json([
            'res' => false,
            'message' => 'No se puede editar esa categoría por ser autogenerada'
            ]);
      }

      $voucher->name = $request->name;
      $voucher->save();

      return response()->json([
        'res' => true,
        'body' => [
            'data' => "Actualizado"
        ],
        'message' => 'Categoría actualizada correctamente'
        ]);
    }

    public function delete(Request $request) {
      $voucher = VoucherCategories::find($request->id);

      if ($voucher->state == 2) {
        return response()->json([
            'res' => false,
            'message' => 'No se puede eliminar esa categoría por ser autogenerada'
            ]);
      }

      $voucher->state = 0;
      $voucher->save();

      DB::table('spending')->where('id_voucher_categories', $request->id)
        ->update([
          "state" => 0
        ]);

      return response()->json([
        'res' => true,
        'body' => [
            'data' => "Eliminado"
        ],
        'message' => 'Categoría eliminada correctamente'
        ]);
    }

    public function list(Request $request) {
        $input = $request->all();
        $query = DB::table('voucher_categories')
          ->where('company_id', '=', $input['filterCompany'])
          ->whereIn('state', [1, 2]);

        if (isset($input['type'])) {
          $query->where('type', $input['type']);
        }


        $voucherCategories = $query->get();

        return response()->json([
            'res' => true,
            'body' => [
                'data' => $voucherCategories
            ],
            'message' => 'Consultado correctamente'
        ]);
    }
}
