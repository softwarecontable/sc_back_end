<?php

namespace App\Http\Controllers;

use App\Models\Spending;
use App\Models\Voucher;
use App\Models\VoucherCategories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\BankMovement;
use App\Models\MovementsVoucher;
use Illuminate\Http\UploadedFile;
use App\Models\MovementsAudio;
use App\Models\Audio;
use App\Models\Bank;
use Carbon\Carbon;
use App\Http\Controllers\BankMovementController;

class VoucherController extends Controller
{

    public const SUNAT_DOCUMENT = "DOCUMENTOS ELECTRONICOS";

  public function store(Request $request) {
      $input = $request->all();
      Log::info($request->all());

      /*$spending = DB::table('spendings')
        ->where('id', $input['id_spending'])
        ->first();

      $file = $input['file_img'];
      $extension = $file->getClientOriginalExtension();
      $file_name = $spending->name .'_'. date('mdY') .'_'. $input['amount'] .'.'. $extension;

      $input['file_img'] = $input['file_img']->storeAs('', $file_name, 'public_uploads');*/

      $image = "";
      if ($request->hasFile('file_img')) {
        $image = self::storeImage($request->id_spending, $request->amount, $request->file('file_img'));
      } else if ($request->hasFile('file_pdf')) {
        $image = self::storeImage($request->id_spending, $request->amount, $request->file('file_pdf'));
      }

      //$image = self::storeImage($request->id_spending, $request->amount, $request->file('file_img'));

      $voucher = new Voucher;
      $voucher->id_spending = $request->id_spending;
      $voucher->amount = $request->amount;
      $voucher->file_img = $image;
      $voucher->state = 1;
      $voucher->currency_id = isset($request->currency_id) ? $request->currency_id : 9;
      $voucher->save();
      //$voucher = Voucher::create($input);
  	  $voucher->id_spending = intval($voucher->id_spending);
  	  $voucher->amount = doubleval($voucher->amount);
      $voucher->img_url = asset('img/' . $image);
      $voucher->currency_id = intval($voucher->currency_id);

      return response()->json([
          'res' => true,
          'body' => [
              'data' => $voucher
          ],
          'message' => 'Voucher creado correctamente'
      ]);
  }

  private function storeImage($id, $amount, UploadedFile $file) {
    $spending = DB::table('spending')
      ->where('id', $id)
      ->first();

    $extension = $file->getClientOriginalExtension();
    $file_name = $spending->name .'_'. date('mdYhis') .'_'. $amount .'.'. $extension;

    $result = $file->storeAs(strval($id), $file_name, 'public_uploads');
    return $result;
  }

  public function storeBox(Request $request) {
      // add voucher
      /*$spending = DB::table('spendings')
        ->where('id', $request->id_spending)
        ->first();

      $file = $request->file_img;
      $extension = $file->getClientOriginalExtension();
      $file_name = $spending->name .'_'. date('mdY') .'_'. $request->amount .'.'. $extension;

      $request->file_img = $request->file_img->storeAs('', $file_name, 'public_uploads');*/
      $image = "";
      if ($request->hasFile('file_img')) {
        $image = self::storeImage($request->id_spending, $request->amount, $request->file('file_img'));
      } else if ($request->hasFile('file_pdf')) {
        $image = self::storeImage($request->id_spending, $request->amount, $request->file('file_pdf'));
      }

      $voucher = new Voucher;
      $voucher->id_spending = $request->id_spending;
      $voucher->amount = $request->amount;
      $voucher->file_img = $image;
      $voucher->state = 2;
      $voucher->currency_id = isset($request->currency_id) ? $request->currency_id : 9;
      $voucher->save();

  	  $voucher->id_spending = intval($voucher->id_spending);
  	  $voucher->amount = doubleval($voucher->amount);
      $voucher->img_url = asset('img/' . $image);

      // add bank movement
      $bank = DB::table('company_bank as cb')
        ->join('banks as b', 'b.id', '=', 'cb.bank_id')
        ->where('cb.company_id', $request->company)
        ->where('b.type', 2)
        ->select('b.id')
        ->first()
        ->id;


      $symbol = ($request->type == 1) ? "-" : "";

      $bankMov = new BankMovement;
      $bankMov->date = Carbon::today();
      $bankMov->concept = $request->concept;
      $bankMov->amount = $symbol."".$request->amount;
      $bankMov->bank_id = $bank;
      $bankMov->state = 2;
      $bankMov->save();

      // add to movements_voucher
      $mov = new MovementsVoucher;
      $mov->id_bank_movements = $bankMov->id;
      $mov->id_voucher = $voucher->id;
      $mov->state = 2;
      $mov->amount = $request->amount;
      $mov->save();

      $bank = Bank::find($bank);
      if ($request->type == 1) {
          $bank->expenditure = $bank->expenditure - $mov->amount;
          $bank->save();
      } else if ($request->type == 2) {
          $bank->income = $bank->income + $mov->amount;
          $bank->save();
      }

      $voucher->currency_id = intval($voucher->currency_id);

      return response()->json([
          'res' => true,
          'body' => [
              'data' => $voucher
          ],
          'message' => 'Voucher creado correctamente'
      ]);
  }

  public function storeAudio(Request $request) {
      $name = 'Voucher_'. $request->id .'_'. date('mdYhis');

      $controller = new BankMovementController;
      $url = $controller->addAudio($name, $request->file('file_img'));

      $audio = new Audio;
      $audio->name = $name;
      $audio->url = $url;
      $audio->state = 1;
      $audio->save();

      $link = new MovementsAudio;
      $link->voucher_id = $request->id;
      $link->audio_id = $audio->id;
      $link->state = 1;
      $link->save();

      return response()->json([
          'res' => true,
          'body' => [
              'data' => true
          ],
          'message' => 'Audio guardado correctamente'
      ]);
    }

  public function delete(Request $request) {
    $voucher = Voucher::find($request->id);
    $voucher->state = 0;
    $voucher->save();

    return response()->json([
        'res' => true,
        'body' => [
            'data' => 'Eliminado'
        ],
        'message' => 'Voucher eliminado correctamente'
    ]);
  }

  public function list(Request $request) {
      $input = $request->all();
      $query = DB::table('voucher')
      ->join('spending', 'voucher.id_spending', 'spending.id')
      ->join('voucher_categories', 'spending.id_voucher_categories', 'voucher_categories.id');
      if (isset($input['id_spending'])) {
        $query->where('id_spending', '=', $input['id_spending']);
      } else if (isset($input['companyId'])) {
        $query->where('voucher_categories.company_id', '=', $input['companyId']);
      }

      $query->where('voucher.state', '>', 0)
      ->select('voucher.*', 'spending.name as tipogasto', 'voucher_categories.name as category')
      ->orderBy('voucher.created_at', 'desc');

      $voucher = $query
      ->get();
      for ($i = 0; $i < count($voucher); $i++) {
          $voucher[$i]->img_url = asset('img/' . $voucher[$i]->file_img);
      }
      return response()->json([
          'res' => true,
          'body' => [
              'data' => $voucher
          ],
          'message' => 'Consultado correctamente'
      ]);
  }

  public function listVoucherCompany(Request $request) {
      $arrayGeneric = [];
      $input = $request->all();
      $currency = isset($input['currency_id']) ? $input['currency_id'] : 9;
      $categories = DB::table('voucher_categories')
        ->where('company_id', '=', $input['company_id'])
        ->where('type', '=', $input['type'])
        ->whereIn('state', [1, 2])
        ->get();
      $spendings = DB::table('spending')
        ->join('voucher_categories', 'spending.id_voucher_categories', 'voucher_categories.id')
        ->where('voucher_categories.company_id', '=', $input['company_id'])
        ->where('voucher_categories.type', '=', $input['type'])
        ->whereIn('spending.state', [1, 2])
        ->select('spending.*', 'voucher_categories.name as category')
        ->get();
      $vouchers = DB::table('voucher')
        ->join('spending', 'voucher.id_spending', 'spending.id')
        ->join('voucher_categories', 'spending.id_voucher_categories', 'voucher_categories.id')
        ->where('voucher_categories.company_id', '=', $input['company_id'])
        ->where('voucher_categories.type', '=', $input['type'])
        ->where('currency_id', $currency)
        ->where('voucher.state', 1)
          // ->whereNotIn('voucher.id',
          //     DB::table('movements_voucher as mv')
          //         ->join('bank_movements as bm', 'bm.id', '=', 'mv.id_bank_movements')
          //         ->join('company_bank as cb', 'cb.bank_id', '=', 'bm.bank_id')
          //         ->where('cb.company_id', $input['company_id'])
          //         ->pluck('mv.id_voucher')
          //         ->values()
          //     )
        ->select('voucher.*', 'voucher_categories.name as category', 'spending.name as tipogasto')
        ->get();
          // $movementsVoucher = DB::table('movements_voucher')
          // ->where('state', 1)
          // ->groupBy('id_voucher')
          // ->select('*', 'sum(amount) as sum')
          // ->get();
        $movementsVoucher = DB::select('call sp_get_voucher_movements');
        for ($i = 0; $i < count($categories); $i++) {
            $categories[$i]->spendings = [];
            for ($j = 0; $j < count($spendings); $j++) {
                $spendings[$j]->vouchers = [];
                if ($categories[$i]->id == $spendings[$j]->id_voucher_categories) {
                    array_push($categories[$i]->spendings, $spendings[$j]);
                }
                for ($k = 0; $k < count($vouchers); $k++) {
                    $vouchers[$k]->img_url = asset('img/' . $vouchers[$k]->file_img);
                    if ($spendings[$j]->id == $vouchers[$k]->id_spending) {
                        array_push($spendings[$j]->vouchers, $vouchers[$k]);
                    }
                    for ($l = 0; $l < count($movementsVoucher); $l++) {
                        if (intval($vouchers[$k]->id) == intval($movementsVoucher[$l]->id_voucher)) {
                            $vouchers[$k]->remaining_amount = $vouchers[$k]->amount - $movementsVoucher[$l]->totalAmount;
                        }
                    }
                    if (!isset($vouchers[$k]->remaining_amount)) {
                      $vouchers[$k]->remaining_amount = $vouchers[$k]->amount;
                    }
                }
            }
          }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $categories
            ],
            'message' => 'Consultado correctamente'
        ]);
    }

    /**
     * @method addVoucher() guarda el comprobante pdf generado en la facturación.
     * @param int $companyId es el id de la compañía.
     * @param string $filename es el nombre del documento.
     * @param float $amount es el precio total del ingreso.
     * @param int $id es el id de la tabla `sale_report`
     * @param int $currency es el tipo de moneda en código. 9 para soles, y 10 para dólares.
     * @param string $voucherName es el nombre del comprobante
     */
    public function addVoucher(
        int $companyId
        , string $filename
        , float $amount
        , int $id
        , int $currency
        , string $voucherName
    ) {
        $folder = VoucherController::SUNAT_DOCUMENT;

        if (
          DB::table('voucher_categories')
              ->where('company_id', $companyId)
              ->where('type', 2)
              ->where('name', $folder)
              ->exists()
        ) {

          $spending = DB::table('voucher_categories as vc')
              ->join('spending as s', 's.id_voucher_categories', '=', 'vc.id')
              ->where('vc.company_id', $companyId)
              ->where('vc.type', 2)
              ->where('vc.name', $folder)
              ->where('s.name', $voucherName)
              ->select('s.*')
              ->first();

          if ($spending) {

            $voucher = new Voucher;
            $voucher->id_spending = $spending->id;
            $voucher->amount = $amount;
            $voucher->file_img = $filename;
            $voucher->state = 1;
            $voucher->sale_report_id = $id;
            $voucher->currency_id = isset($currency) ? $currency : 9;

            $voucher->save();

          } else { // Crear spending y voucher

            $category = DB::table('voucher_categories')
                ->where('company_id', $companyId)
                ->where('type', 2)
                ->where('name', $folder)
                ->first();

            $spending = new Spending;
            $spending->name = $voucherName;
            $spending->id_voucher_categories = $category->id;
            $spending->state = 2;
            $spending->save();

            $voucher = new Voucher;
            $voucher->id_spending = $spending->id;
            $voucher->amount = $amount;
            $voucher->file_img = $filename;
            $voucher->state = 1;
            $voucher->sale_report_id = $id;
            $voucher->currency_id = isset($currency) ? $currency : 9;
            $voucher->save();

          }

        } else { // Crear todos los registros
          $category = new VoucherCategories;
          $category->name = $folder;
          $category->state = 2;
          $category->company_id = $companyId;
          $category->type = 2;
          $category->save();

          $spending = new Spending;
          $spending->name = $voucherName;
          $spending->id_voucher_categories = $category->id;
          $spending->state = 2;
          $spending->save();

          $voucher = new Voucher;
          $voucher->id_spending = $spending->id;
          $voucher->amount = $amount;
          $voucher->file_img = $filename;
          $voucher->state = 1;
          $voucher->sale_report_id = $id;
          $voucher->currency_id = isset($currency) ? $currency : 9;
          $voucher->save();
        }

    }

}
