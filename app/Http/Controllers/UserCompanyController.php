<?php

namespace App\Http\Controllers;

use App\Models\UserCompany;
use Illuminate\Http\Request;

class UserCompanyController extends Controller
{
    public function store(Request $request) {
        $input = $request->all();

        $user_company = UserCompany::create($input);
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $user_company
            ],
            'message' => 'Vinculación correcta'
        ]);
    }
}
