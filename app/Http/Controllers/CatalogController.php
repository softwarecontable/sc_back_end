<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TypeDocument;
use Illuminate\Support\Facades\DB;

class CatalogController extends Controller
{
    public function list() {
        $masterData = [];
        $groupCatalog = DB::select('call sp_get_group_catalog');
        for ($i=0; $i < count($groupCatalog); $i++) {
            $masterData[$groupCatalog[$i]->descripcion] = [];
        }
        $catalog =  DB::select('call sp_get_catalog');
        foreach($masterData as $clave => $val) {
            for ($k=0; $k < count($catalog); $k++) {
                if ($clave === $catalog[$k]->grupo) {
                    $catalog[$k]->id = intval($catalog[$k]->id);
                    $catalog[$k]->id_grupo = intval($catalog[$k]->id_grupo);
                    array_push($masterData[$clave], $catalog[$k]);
                }
            }
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $masterData
            ],
            'message' => ''
        ]);
    }

    public function listItems($id) {
      $items = DB::table('unidad_medida')->get();
      $cuentas = DB::table('plan_cuentas')
        ->where('company_id', $id)
        ->get();

      $data = [
        'measures' => $items,
        'accounts' => $cuentas
      ];

      return response()->json([
          'res' => true,
          'body' => [
              'data' => $data
          ],
          'message' => ''
      ]);
    }

    public function saleItems($id) {
        //   $documentType = [
        //     self::catalogModel(1, 'OTROS', 15, '0', 0),
        //     self::catalogModel(2, 'DNI', 8, '1', 1),
        //     self::catalogModel(3, 'CE', 12, '4', 0),
        //     self::catalogModel(4, 'RUC', 11, '6', 1),
        //     self::catalogModel(5, 'PASAPORTE', 12, '7', 0),
        //     self::catalogModel(6, 'CDI', 15, 'A', 0),
        //   ];
        // id_grupo: Cantidad de caracteres.
        // values: Indica si la cantidad de caracteres es máxima o exacta
        $documentType = DB::table('sale_catalog')
            ->where('id_group', 5)
            ->select(DB::raw('id, description as descripcion, value as id_grupo, label as grupo, aditional as valores'))
            ->get();


      // id_grupo=1->necesita certificado para sunat
      // id_grupo=0->no necesita certificado
      // id_grupo=2->necesita notas y forma de pago
        //   $voucherType = [
        //     self::catalogModel(1, 'BOLETA DE VENTA', 1, '03'),
        //     self::catalogModel(2, 'FACTURA', 1, '01'),
        //     self::catalogModel(3, 'NOTA DE VENTA', 0, '99'),
        //     self::catalogModel(4, 'COTIZACION', 2, '100')
        //   ];
        $voucherType = DB::table('sale_catalog')
            ->where('id_group', 1)
            ->select(DB::raw('id, description as descripcion, value as id_grupo, label as grupo, aditional as valores'))
            ->get();


        //   $currencyType = [
        //     self::catalogModel(1, 'SOLES', 9, 'PEN'),
        //     self::catalogModel(2, 'DOLARES', 10, 'USD')
        //   ];
        $currencyType = DB::table('sale_catalog')
            ->where('id_group', 2)
            ->select(DB::raw('id, description as descripcion, value as id_grupo, label as grupo, aditional as valores'))
            ->get();


      // 1: Boleta, 2: Factura, 3: Nota de Venta, 0: Todos
        //   $operationType = [
        //     self::catalogModel(1, 'Venta interna', 0, '0101'),
        // self::catalogModel(2, 'Venta Interna – Anticipos', 0, '0102'),
        // self::catalogModel(3, 'Venta Interna - Itinerante', 0, '0103'),
        // self::catalogModel(4, 'Venta Interna - Sustenta Traslado de Mercadería - Remitente', 1, '0110'),
        // self::catalogModel(5, 'Venta Interna - Sustenta Traslado de Mercadería - Transportista', 1, '0111'),
        // self::catalogModel(6, 'Venta Interna - Sustenta Gastos Deducibles Persona Natural', 2, '0112'),
        // self::catalogModel(7, 'Venta Interna - Sujeta al IVAP', 0, '0120'),
        // self::catalogModel(8, 'Venta Interna - Sujeta al FISE', 0, '0121'),
        // self::catalogModel(9, 'Venta Interna - Sujeta a otros impuestos', 0, '0122'),
        // self::catalogModel(10, 'Venta Interna - Realizadas al Estado', 0, '0130'),
        // self::catalogModel(11, 'Exportación de Bienes', 0, '0200'),
        // self::catalogModel(12, 'Exportación de Servicios – Prestación servicios realizados íntegramente en el país', 0, '0201'),
        // self::catalogModel(13, 'Exportación de Servicios – Prestación de servicios de hospedaje No Domiciliado', 0, '0202'),
        // self::catalogModel(14, 'Exportación de Servicios – Transporte de navieras', 0, '0203'),
        // self::catalogModel(15, 'Exportación de Servicios – Servicios a naves y aeronaves de bandera extranjera', 0, '0204'),
        // self::catalogModel(16, 'Exportación de Servicios - Servicios que conformen un Paquete Turístico', 0, '0205'),
        // self::catalogModel(17, 'Exportación de Servicios – Servicios complementarios al transporte de carga', 0, '0206'),
        // self::catalogModel(18, 'Exportación de Servicios – Suministro de energía eléctrica a favor de sujetos domiciliados en ZED', 0, '0207'),
        // self::catalogModel(19, 'Exportación de Servicios – Prestación servicios realizados parcialmente en el extranjero', 0, '0208'),
        // self::catalogModel(20, 'Operaciones con Carta de porte aéreo (emitidas en el ámbito nacional)', 0, '0301'),
        // self::catalogModel(21, 'Operaciones de Transporte ferroviario de pasajeros', 0, '0302'),
        // self::catalogModel(22, 'Operaciones de Pago de regalía petrolera', 0, '0303'),
        // self::catalogModel(23, 'Operación Sujeta a Detracción', 0, '1001'),
        // self::catalogModel(24, 'Operación Sujeta a Detracción- Recursos Hidrobiológicos', 0, '1002'),
        // self::catalogModel(25, 'Venta interna', 3, '0001')
        //     self::catalogModel(2, 'Venta Interna – Anticipos', 0, '0101'),
        //     self::catalogModel(3, 'Venta Interna - Itinerante', 0, '0101'),
        //     self::catalogModel(4, 'Venta Interna - Sustenta Traslado de Mercadería - Remitente', 1, '0101'),
        //     self::catalogModel(5, 'Venta Interna - Sustenta Traslado de Mercadería - Transportista', 1, '0101'),
        //     self::catalogModel(6, 'Venta Interna - Sustenta Gastos Deducibles Persona Natural', 2, '0101'),
        //     self::catalogModel(7, 'Venta Interna - Sujeta al IVAP', 0, '0101'),
        //     self::catalogModel(8, 'Venta Interna - Sujeta al FISE', 0, '0101'),
        //     self::catalogModel(9, 'Venta Interna - Sujeta a otros impuestos', 0, '0101'),
        //     self::catalogModel(10, 'Venta Interna - Realizadas al Estado', 0, '0101'),
        //     self::catalogModel(11, 'Exportación de Bienes', 0, '0101'),
        //     self::catalogModel(12, 'Exportación de Servicios – Prestación servicios realizados íntegramente en el país', 0, '0101'),
        //     self::catalogModel(13, 'Exportación de Servicios – Prestación de servicios de hospedaje No Domiciliado', 0, '0101'),
        //     self::catalogModel(14, 'Exportación de Servicios – Transporte de navieras', 0, '0101'),
        //     self::catalogModel(15, 'Exportación de Servicios – Servicios a naves y aeronaves de bandera extranjera', 0, '0101'),
        //     self::catalogModel(16, 'Exportación de Servicios - Servicios que conformen un Paquete Turístico', 0, '0101'),
        //     self::catalogModel(17, 'Exportación de Servicios – Servicios complementarios al transporte de carga', 0, '0101'),
        //     self::catalogModel(18, 'Exportación de Servicios – Suministro de energía eléctrica a favor de sujetos domiciliados en ZED', 0, '0101'),
        //     self::catalogModel(19, 'Exportación de Servicios – Prestación servicios realizados parcialmente en el extranjero', 0, '0101'),
        //     self::catalogModel(20, 'Operaciones con Carta de porte aéreo (emitidas en el ámbito nacional)', 0, '0101'),
        //     self::catalogModel(21, 'Operaciones de Transporte ferroviario de pasajeros', 0, '0101'),
        //     self::catalogModel(22, 'Operaciones de Pago de regalía petrolera', 0, '0101'),
        //     self::catalogModel(23, 'Operación Sujeta a Detracción', 0, '0101'),
        //     self::catalogModel(24, 'Operación Sujeta a Detracción- Recursos Hidrobiológicos', 0, '0102'),
        //     self::catalogModel(25, 'Venta interna', 3, '0001'),
        //     self::catalogModel(26, 'Venta interna', 4, '0001'),
        //   ];
        $operationType = DB::table('sale_catalog')
            ->where('id_group', 3)
            ->select(DB::raw('id, description as descripcion, value as id_grupo, label as grupo, aditional as valores'))
            ->get();


        //   $paymentType = [
        //     self::catalogModel(1, 'Contado', 0, ''),
        //     self::catalogModel(2, '8 días', 8, '1'),
        //     self::catalogModel(3, '15 días', 15, '1'),
        //     self::catalogModel(4, '30 días', 30, '1'),
        //     self::catalogModel(5, '60 días', 60, '1'),
        //     self::catalogModel(6, 'Vencimiento manual', 0, '1'),
        //   ];
        $paymentType = DB::table('sale_catalog')
            ->where('id_group', 4)
            ->select(DB::raw('id, description as descripcion, value as id_grupo, label as grupo, aditional as valores'))
            ->get();

      $correlatives = TypeDocument::getNumber($id);

      $items = DB::table('unidad_medida')->get();
      $cuentas = DB::table('plan_cuentas')
        ->where('company_id', $id)
        ->get();

      $saleCatalog = array(
        'document' => $documentType,
        'currency' => $currencyType,
        'operation' => $operationType,
        'payment' => $paymentType,
        'voucher' => $voucherType,
        'number' => $correlatives,
        'measures' => $items,
        'accounts' => $cuentas
      );

      return response()->json([
          'res' => true,
          'body' => [
              'data' => $saleCatalog
          ],
          'message' => ''
      ]);
    }

    public function noteItems($id) {
      $items = DB::table('sc_catalog as sc')
        ->join('sc_group_catalog as gc', 'sc.id_grupo', '=', 'gc.id')
        ->where('id_grupo', 14)
        ->select('sc.*', 'gc.descripcion as grupo')
        ->get();

      $voucher_number = TypeDocument::getNumber($id)->credit_note;

      $reportCatalog = array(
        'noteItems' => $items,
        'number' => $voucher_number + 1
      );

      return response()->json([
        'res' => true,
        'body' => [
            'data' => $reportCatalog
        ],
        'message' => ''
      ]);
    }

    public function getCompanyParams($id) {
        $igv = 18;
        if ($id == 44 || $id == 45) { // Restaurant Morocho
          $igv = 10;
      }

        $params = [
            'igv' => $igv
        ];
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $params
            ],
            'message' => ''
        ]);
    }

    private function catalogModel($id, $desc, $id_group, $group, $values = 0) {
      return array(
        'id' => $id,
        'descripcion' => $desc,
        'id_grupo' => $id_group,
        'grupo' => $group,
        'valores' => $values
      );
    }
}
