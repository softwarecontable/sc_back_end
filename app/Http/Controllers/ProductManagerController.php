<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\OrderItem;
use Carbon\Carbon;

class ProductManagerController extends Controller
{

    public function list($id) {
        $products = DB::table('product_store as ps')
            ->join('producto as p', 'p.id', '=', 'ps.product_id')
            ->where('ps.store_id', $id)
            ->where('p.estado', 1)
            ->select(DB::raw('ps.*, p.nombre as name, p.impuesto as tax'))
            ->orderBy('p.id', 'desc')
            ->get();

        foreach ($products as $key => $item) {
            $item->image = asset("images/" .$item->image);
        }

        return response()->json([
            'res' => true,
            'body' => [
                'data' => $products
            ],
            'message' => (count($products) > 0) ? 'Consultado correctamente' : 'No tiene reportes registrados'
        ]);
    }

    public function status($id) {
        $today = Carbon::today()->format('Y-m-d');
        $state = DB::table('sale_day')
            ->where('store_id', $id)
            ->whereDate('date', $today)
            ->get();

        return response()->json([
            'res' => true,
            'body' => [
                'data' => !empty($state)
            ],
            'message' => 'Estado de los productos'
        ]);
    }

    public function params($id) {
        $params = DB::table('parameters')
            ->whereIn('group_id', ['product_type', 'table_state', 'user_default'])
            ->get();

        $storeManager = DB::table('store_manager')
            ->where('store_id', $id)
            ->get();

        $elements = DB::table('product_element')
            ->get();

        $tables = DB::table('store_table')
            ->where('store_id', $id)
            ->get();

        $attributes = DB::table('attribute')
            ->get();

        $productAttributes = DB::table('product_attribute')
            ->get();

        return response()->json([
            'res' => true,
            'body' => [
                'data' => [
                    'params' => $params,
                    'storeManager' => $storeManager,
                    'elements' => $elements,
                    'tables' => $tables,
                    'attributes' => $attributes,
                    'productAttributes' => $productAttributes
                ]
            ],
            'message' => 'Lista de parámetros'
        ]);
    }

    public function update(Request $request) {
        $today = Carbon::today()->format('Y-m-d');

        if (strcmp($today, $request->date) !== 0) {
            return response()->json([
                'res' => false,
                'body' => [
                    'data' => null
                ],
                'message' => 'La fecha de su dispositivo no es la correcta.'
            ]);
        }

        $hasData = DB::select(DB::raw("SELECT * FROM `store_manager` WHERE Date('" .$today. "') = Date(store_manager.date) LIMIT 1"));

        if (!$hasData) {
            $id = DB::table('store_manager')->insertGetId([
                'user_id' => $request->user_id,
                'store_id' => $request->store_id,
                'date'  => $today
            ]);

            foreach ($request->list as $key => $value) {
                DB::table('product_store')
                    ->where('id', $value["id"])
                    ->update([
                        'state' => $value["state"],
                        'stock' => $value["stock"],
                        'price' => $value["price"],
                        'sale_day_id' => $id
                    ]);

                Log::info($request->all());

                // Se va a actualizar el precio del producto también en la tabla `producto`
                DB::table('producto as p')
                    ->join('product_store as ps', 'ps.product_id', '=', 'p.id')
                    ->where('ps.id', $value["id"])
                    ->update([
                        'p.precio_venta' => $value["price"]
                    ]);
            }
        } else {
            $id = $hasData[0]->id;

            foreach ($request->list as $key => $value) {
                DB::table('product_store')
                    ->where('id', $value["id"])
                    ->update([
                        'state' => $value["state"],
                        'stock' => $value["stock"],
                        'price' => $value["price"]
                    ]);

                Log::info($request->all());

                // Se va a actualizar el precio del producto también en la tabla `producto`
                DB::table('producto as p')
                    ->join('product_store as ps', 'ps.product_id', '=', 'p.id')
                    ->where('ps.id', $value["id"])
                    ->update([
                        'p.precio_venta' => $value["price"]
                    ]);
            }
        }

        return response()->json([
            'res' => true,
            'body' => [
                'data' => $id
            ],
            'message' => '¡Productos actualizados correctamente!'
        ]);
    }

    public function newOrder(Request $request) {
        $id = DB::table('order')->insertGetId([
            "user_id" => $request->user_id,
            "store_id" => $request->store_id,
            "table_id" => $request->table_id,
            "price" => $request->price,
            "state" => 1
        ]);

        if ($id) {
            return response()->json([
                'res' => true,
                'body' => [
                    'data' => $id
                ],
                'message' => 'Orden creada correctamente.'
            ]);
        } else {
            return response()->json([
                'res' => false,
                'message' => 'No se pudo crear la orden, inténtelo de nuevo.'
            ]);
        }

    }

    public function endOrder($id, Request $request) {
        // Update the order, set status = 3, that ends it.
        DB::table('order')->where('id', $id)
            ->update([
                "state" => 3
            ]);

        Log::info($request->all());
        $result = true;

        foreach ($request->all() as $key => $value) {
            Log::info($value);
            $attrs = json_encode($value['attrs']);
            $elements = json_encode($value['elements']);

            $new = new OrderItem;
            $new->order_id = $value['order_id'];
            $new->product_id = $value['product_id'];
            $new->table_id = $value['table_id'];
            $new->product_name = $value['product_name'];
            $new->price = $value['price'];
            $new->attrs_price = $value['attrs_price'];
            $new->quantity = $value['quantity'];
            $new->attrs = $attrs;
            $new->elements = $elements;
            $result = $result && $new->save();
        }

        return response()->json([
            'res' => $result,
            'body' => [
                'data' => $result
            ],
            'message' => "Resultado de actualizar la orden: $result"
        ]);

    }

}
