<?php

namespace App\Http\Controllers;

use App\Models\VentaDetalle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VentaDetalleController extends Controller
{
    public function store(Request $request) {
        $input = $request->all();
        $venta_detalle = DB::table('venta_detalle')->where('id', '=', $input['id'])->get();
        if (count($venta_detalle) > 0) {
            $venta_detalle = DB::table('venta_detalle')->where('id', '=', $input['id'])->update($input);
        } else {
            $venta_detalle = VentaDetalle::create($input);
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $venta_detalle
            ],
            'message' => 'Detalle venta creada correctamente'
        ]);
    }

    public function listDetalleVenta(Request $request) {
        $input = $request->all();
        $venta = DB::table('venta')
        ->where('id_movimiento', '=', $input['movimiento_id'])
        ->orderBy('secuencia', 'asc')
        ->get();
        if (count($venta) > 0 ) {
            $venta_detalle = DB::table('venta_detalle')->where('venta_id', '=', $venta[0]->id)->get();
        } else {
            $venta_detalle = [];
        }
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $venta_detalle
            ],
            'message' => 'Consultado correctamente'
        ]); 
    }

    public function deleteDetalleVenta(Request $request) {
        $input = $request->all();
        $venta_detalle = DB::table('venta_detalle')->where('id', '=', $input['id'])->delete();
        return response()->json([
            'res' => true,
            'body' => [
                'data' => $venta_detalle
            ],
            'message' => 'Eliminado correctamente'
        ]); 
    }
}
