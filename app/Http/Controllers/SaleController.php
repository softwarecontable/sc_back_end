<?php

namespace App\Http\Controllers;
use App\Models\TypeDocument;
use Illuminate\Support\Facades\DB;
use App\Models\SaleReport;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\GreenterController;

class SaleController extends Controller
{

    protected $greenter;

    public function __construct(GreenterController $greenter) {
        $this->greenter = $greenter;
    }

    public function listSaleReport($id) {
        $items = DB::table('sale_report')
            ->where('company_id', $id)
            ->orderBy('id','desc')
            ->get();

        foreach ($items as $value) {
            $value->url = isset($value->url) ? asset('img/' . $value->url) : "";
            $value->details = DB::table('sale_report_detail')
                ->where('sale_report_id', $value->id)
                ->get();
        }

        return response()->json([
            'res' => count($items) > 0,
            'body' => [
                'data' => $items
            ],
            'message' => (count($items) > 0) ? 'Consultado correctamente' : 'No tiene reportes registrados'
        ]);
    }

    public function voidedSend(Request $request) {
        $ceSendCode = time();
        Log::info("Code send voided: $ceSendCode");
        Log::info($request->all());
        $company = Company::find($request->company_id);

        $data = array(
            'fecGeneration' => $request->fecGeneration,
            'fecComunicacion' => $request->fecComunicacion,
            'company' => array(
                'ruc' => $company->ruc,
                'razonSocial' => $company->name,
                'nombreComercial' => $company->commercial_name,
                'address' => array(
                    'direccion' => $company->address,
                    'provincia' => 'LIMA',
                    'departamento' => 'LIMA',
                    'distrito' => 'LIMA',
                    'ubigueo' => '150101'
                )
            )
        );

        $data['details'] = [];
        foreach ($request->details as $key => $value) {
            array_push(
                $data['details'],
                array(
                    'tipoDoc'       => $value['tipoDoc'],
                    'serie'         => $value['serie'],
                    'correlativo'   => $value['correlativo'],
                    'desMotivoBaja' => $value['motivo']
                )
            );
        }

        $response = $this->greenter->sendReversion($data, $company);
        if (!$response['success']) {
            return response()->json([
                'res' => $response['success'],
                'body' => [
                    'data' => 0
                ],
                'message' => $response['message']
            ]);
        }

        $correlative = TypeDocument::updateNumber(
            false,
            TypeDocument::RETENCION,
            $company->id
        );

        // Update report state
        $report = SaleReport::find($request->id);
        $report->state = SaleReport::ANULAR;
        $report->save();

        return response()->json([
            'res' => $response['success'],
            'body' => [
                'data' => $correlative
            ],
            'message' => $response['message']
        ]);

    }

}
