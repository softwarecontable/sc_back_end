<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\EncryptionController;

class UserController extends Controller
{

    protected $encryption;

    public function __construct(EncryptionController $encryption) {
        $this->encryption = $encryption;
    }

    public function store(Request $request) {
        $input = $request->all();

        if (DB::table('users')->where('email', $input['email'])->exists()) {
          return response()->json([
              'res' => false,
              'message' => 'El usuario ya se encuentra registrado'
          ]);
        }

        if (DB::table('users')->where('cell_phone', $input['cell_phone'])->exists()) {
          return response()->json([
              'res' => false,
              'message' => 'El usuario ya se encuentra registrado'
          ]);
        }

        $input['password'] = Hash::make($request->password);
        $user = User::create($input);


        $subject = "ZUMAR: ¡Bienvenido!";
        self::welcomeEmail($input['email'], $subject);

        return response()->json([
            'res' => true,
            'body' => [
                'data' => $user
            ],
            'message' => 'Usuario creado correctamente'
        ]);
    }

    private function welcomeEmail($to, $subject) {
        $message = view('bienvenida');

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <develop@zumar.pe>' . "\r\n";
        //$headers .= 'Cc: walter.cojal@gmail.com' . "\r\n";

        mail($to,$subject,$message,$headers);
    }

    public function sendEmailRecover(Request $request) {
      if (!DB::table('users')->where('email', $request->email)->exists()) {
        return response()->json([
            'res' => false,
            'message' => 'Ocurrió un error, inténtelo de nuevo'
        ]);
      }

      $code = rand(100000, 1000000 - 1);

      $user = User::where('email', $request->email)->first();
      $user->verification_code = $code;
      $user->save();

      $subject = "ZUMAR: Código de verificación";
      self::emailSender($code, $request->email, $subject);

      return response()->json([
          'res' => true,
          'body' => [
              'data' => $code
          ],
          'message' => 'Correo enviado'
      ]);
    }

    public function changePassword(Request $request) {
      if (!DB::table('users')->where('verification_code', $request->code)->exists()) {
        return response()->json([
            'res' => false,
            'message' => 'El código de verificación es incorrecto'
        ]);
      }

      $request->password = Hash::make($request->password);

      $user = User::where('verification_code', $request->code)->first();
      $user->password = $request->password;
      $user->verification_code = null;
      $user->save();

      self::passwordResetEmail($user->email, $user->name);

      return response()->json([
          'res' => true,
          'body' => [
              'data' => true
          ],
          'message' => 'Correo enviado'
      ]);
    }

    private function passwordResetEmail($to, $name) {
        $message = view('password', ['name' => $name]);

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <develop@zumar.pe>' . "\r\n";
        //$headers .= 'Cc: walter.cojal@gmail.com' . "\r\n";

        mail($to,'ZUMAR: ¡Contraseña cambiada!',$message,$headers);
    }

    public function emailSender($code, $to, $subject) {

        $message = view('recover', ['code' => $code]);

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <develop@zumar.pe>' . "\r\n";
        //$headers .= 'Cc: walter.cojal@gmail.com' . "\r\n";

        mail($to,$subject,$message,$headers);
    }

    public function login(Request $request) {
        $user = User::whereEmail($request->email)->first();
        if (!is_null($user) && Hash::check($request->password, $user->password)) {
          if (DB::table('user_company')->where('user_id', $user->id)->exists()) {
            $data = self::getLoginData($user);
            return response()->json([
                'res' => true,
                'body' => [
                    'data' => $data
                ],
                'message' => 'Bienvenido'
            ]);
          } else {
            return response()->json([
                'res' => false,
                'message' => 'No tiene una compañía asociada'
            ]);
          }
        } else {
            return response()->json([
                'res' => false,
                'message' => 'Credenciales incorrectas'
            ]);
        }
    }

    private function getLoginData(User $user) {
      $token = $user->createToken('software contable')->accessToken;
      $user['remember_token'] = $token;
      $user['token'] = $token;
      $user_company = DB::table('user_company')->where('user_id', $user['id'])->first();

      if (asset($user_company->store_id)) {
        $store = DB::table('store')->where('id', $user_company->store_id)->first();
      } else {
        $store = null;
      }

      $company = [];
      array_push($company, Company::find($user_company->company_id));

      if (assert($company[0]->logo)) {
        $company[0]->logo = asset('img/' . $company[0]->logo);
      }

      if (assert($company[0]->certified)) {
        $company[0]->certified = asset('img/' . $company[0]->certified);
      }

      $grow = array(
        'box_added' => false,
        'has_entity' => false,
        'has_category' => false,
        'has_link' => false,
        'has_stock' => false,
        'progress' => 0.6,
        'bank_label' => 0,
        'stock_label' => 0
      );

      $boxAdded = DB::table('company_bank as cb')
        ->join('banks as b', 'cb.bank_id', 'b.id')
        ->where('cb.company_id', $company[0]->id)
        ->where('b.type', 2)
        ->exists();

      $grow['has_entity'] = DB::table('company_bank as cb')
        ->join('banks as b', 'cb.bank_id', 'b.id')
        ->where('cb.company_id', $company[0]->id)
        ->exists();

      $grow['has_category'] = DB::table('bank_movements as bm')
        ->join('company_bank as cb', 'bm.bank_id', '=', 'cb.bank_id')
        ->where('cb.company_id', $company[0]->id)
        ->exists();

      $grow['has_stock'] = DB::table('producto')
        ->where('company_id', $company[0]->id)
        ->exists();

      $grow['progress'] = $grow['has_entity'] ? $grow['progress'] + 0.1 : $grow['progress'];
      $grow['progress'] = $grow['has_category'] ? $grow['progress'] + 0.1 : $grow['progress'];
      $grow['progress'] = $grow['has_stock'] ? $grow['progress'] + 0.1 : $grow['progress'];
      $grow['bank_label'] = $grow['has_entity'] ? $grow['bank_label'] + 1 : $grow['bank_label'];
      $grow['bank_label'] = $grow['has_category'] ? $grow['bank_label'] + 1 : $grow['bank_label'];
      $grow['stock_label'] = $grow['has_stock'] ? $grow['stock_label'] + 1 : $grow['stock_label'];

      //   $grow['progress'] = round($grow['progress'], 2);
      $grow['progress'] = 1.0;

      $grow['box_added'] = $boxAdded;
      $user['grow'] = $grow;
      $user['box_added'] = $boxAdded;

      if (isset($company[0]->sol_user)) {
        $company[0]->sol_user = $this->encryption->decrypt($company[0]->sol_user);
      }

      if (isset($company[0]->sol_pass)) {
        $company[0]->sol_pass = $this->encryption->decrypt($company[0]->sol_pass);
      }

        //   $company[0]->sol_user = $this->encryption->decrypt($company[0]->sol_user);
        //   $company[0]->sol_pass = $this->encryption->decrypt($company[0]->sol_pass);

      $user['company'] = $company;
      $user['store'] = $store;

      return $user;
    }

    public function loginFacebook(Request $request) {
      $user = User::where('facebook_id', $request->id)
        ->orWhere('email', $request->email)
        ->first();
      if (!is_null($user)) {
        if (DB::table('user_company')->where('user_id', $user->id)->exists()) {
          $data = self::getLoginData($user);
          return response()->json([
              'res' => true,
              'body' => [
                  'data' => $data
              ],
              'message' => 'Bienvenido'
          ]);
        } else {
          return response()->json([
              'res' => false,
              'message' => 'No tiene una compañía asociada'
          ]);
        }
      } else {
        return response()->json([
            'res' => false,
            'message' => 'No está registrado'
        ]);
      }
    }

    public function loginApple(Request $request) {
      $user = User::where('apple_id', $request->id)
        ->orWhere('email', $request->email)
        ->first();
      if (!is_null($user)) {
        if (DB::table('user_company')->where('user_id', $user->id)->exists()) {
          $data = self::getLoginData($user);
          return response()->json([
              'res' => true,
              'body' => [
                  'data' => $data
              ],
              'message' => 'Bienvenido'
          ]);
        } else {
          return response()->json([
              'res' => false,
              'message' => 'No tiene una compañía asociada'
          ]);
        }
      } else {
        return response()->json([
            'res' => false,
            'message' => 'No está registrado'
        ]);
      }
    }

    public function loginBackOffice(Request $request) {
        $user = User::whereEmail($request->email)->first();
        if (!is_null($user) && Hash::check($request->password, $user->password)) {
            $token = $user->createToken('software contable')->accessToken;
            $user['remember_token'] = $token;
            $user['token'] = $token;
            return response()->json([
                'res' => true,
                'body' => [
                    'data' => $user
                ],
                'message' => 'Bienvenido'
            ]);
        } else {
            return response()->json([
                'res' => false,
                'message' => 'Credenciales incorrectas'
            ]);
        }
    }

    public function logout() {
        $user = auth()->user();
        $user->remember_token = null;
        $user->save();
        return response()->json([
            'res' => true,
            'message' => 'Adios amigo'
        ]);
    }
}
