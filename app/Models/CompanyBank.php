<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyBank extends Model
{
    use HasFactory;

    protected $table = 'company_bank';

    protected $fillable = [
        'company_id',
        'bank_id',
        'state'
    ];
}
