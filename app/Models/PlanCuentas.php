<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanCuentas extends Model {
    use HasFactory;

    protected $table = 'plan_cuentas';

    protected $fillable = [
        'codigo',
        'descripcion',
        'company_id'
    ];
}