<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InventarioMovimiento extends Model
{
    use HasFactory;

    protected $table = 'inventario_movimiento';

    protected $fillable = [
        'producto_id',
        'inventario_id',
        'cantidad'
    ];
}
