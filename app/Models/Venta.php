<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    use HasFactory;

    protected $table = 'venta';

    protected $fillable = [
        'secuencia',
        'conceptoComprobante',
        'anexo',
        'codigo',
        'razonSocial',
        'ruc',
        'tipoDocumento',
        'serie',
        'numero',
        'fechaEmision',
        'fechaVencimiento',
        'destino',
        'tasaIgv',
        'baseImp',
        'montoIgv',
        'total',
        'medioPago',
        'glosaComp',
        'otroImp',
        'glosaMovimiento',
        'moneda',
        'fechaVenta',
        'periodoContable',
        'fechaDcto',
        'fechaVcto',
        'conversion',
        'fechaTipoCambio',
        'tcEspecial',
        'tcMonto',
        'debe',
        'haber',
        'diferencia',
        'id_movimiento'
    ];
}
