<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;

    protected $table = 'producto';

    protected $fillable = [
        'categoria_id',
        'nombre',
        'precio_venta',
        'impuesto',
        'unidad_medida',
        'sunat_id',
        'cuenta_contable',
        'company_id'
    ];
}
