<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventario extends Model
{
    use HasFactory;

    protected $table = 'inventario';

    protected $fillable = [
        'producto_id',
        'cantidad_inicial',
        'cantidad_minima',
        'cantidad_maxima',
        'cantidad_actual',
        'company_id'
    ];
}
