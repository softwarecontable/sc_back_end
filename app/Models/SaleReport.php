<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\SaleReportDetail;
use Illuminate\Http\Request;

class SaleReport extends Model
{
    use HasFactory;

    protected $table = 'sale_report';

    public const NUEVO = 1;
    public const MODIFICAR = 2;
    public const ANULAR = 3;

    protected $fillable = [
      'id',
      'company_id',
      'index',
      'doc_type',
      'currency_id',
      'price',
      'client_name',
      'client_email',
      'client_phone',
      'status'
    ];

    /**
     * Crea nuevos registros en las tablas `sale_report`, `sale_report_detail` y `sale_payment`.
     * @param Request $request es el request que envía el front end.
     * @return int el id de la tabla `sale_report`
     */
    public static function saveReport(Request $request): int {
        $report = new SaleReport;
        $report->company_id = $request->company_id;
        $report->ublVersion = '2.1';
        $report->tipoOperacion = $request->tipoOperacion;
        $report->tipoDoc = $request->tipoDoc;
        $report->serie = $request->serie;
        $report->correlativo = $request->correlativo;
        $report->fechaEmision = $request->fechaEmision;
        $report->tipoMoneda = $request->tipoMoneda;
        $report->formaPago = 'Contado';
        $report->client_tipo_doc = $request->client['tipoDoc'];
        $report->client_doc = $request->client['numDoc'];
        $report->client_name = $request->client['rznSocial'];
        $report->client_email = isset($request->client['email']) ? $request->client['email'] : "";
        $report->client_phone = isset($request->client['phone']) ? $request->client['phone'] : "";
        $report->client_address = isset($request->client['address']['direccion']) ? $request->client['address']['direccion'] : "";
        $report->company_doc = $request->company['ruc'];
        $report->company_name = $request->company['razonSocial'];
        $report->company_address = $request->company['address']['direccion'];
        $report->operacionGravadas = $request->mtoOperGravadas;
        $report->igv = $request->mtoIGV;
        $report->precio = $request->valorVenta;
        $report->totalImpuestos = $request->totalImpuestos;
        $report->subTotal = $request->subTotal;
        $report->legend = $request->legends[0]['value'];
        $report->planPago = isset($request->planPago) ? $request->planPago : 'Contado';
        $report->expiration = isset($request->expiration) ? $request->expiration : '';
        $report->tip = isset($request->tip) ? $request->tip : '0.00';
        $report->state = 1;
        $report->save();

        foreach ($request->payment_type as $key => $value) {
            $payment = new SalePayment;
            $payment->report_id = $report->id;
            $payment->payment_id = $value["id"];
            $payment->amount = $value["amount"];
            $payment->save();
        }

        foreach ($request->details as $key => $value) {
            $detail = new SaleReportDetail;
            $detail->sale_report_id = $report->id;
            $detail->mtoPrecioUnitario = $value['mtoPrecioUnitario'];
            $detail->igv = $value['igv'];
            $detail->mtoValorUnitario = $value['mtoValorUnitario'];
            $detail->mtoValorVenta = $value['mtoValorVenta'];
            $detail->unidad = $value['unidad'];
            $detail->discount = $value['discount'];
            $detail->descripcion = $value['descripcion'];
            $detail->cantidad = $value['cantidad'];
            $detail->idProduct = $value['idProduct'];
            $detail->totalImpuestos = $value['totalImpuestos'];
            $detail->porcentajeIgv = $value['porcentajeIgv'];
            $detail->codProducto = $value['codProducto'];
            $detail->tipAfeIgv = $value['tipAfeIgv'];
            $detail->mtoBaseIgv = $value['mtoBaseIgv'];
            $detail->nominalDiscount = $value['nominalDiscount'];
            $detail->nominalTotal = $value['nominalTotal'];
            $detail->baseAmount = $value['baseAmount'];
            $detail->save();
        }

        return $report->id;
    }

}
