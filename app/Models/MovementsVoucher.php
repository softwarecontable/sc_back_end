<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MovementsVoucher extends Model
{
    use HasFactory;

    protected $table = 'movements_voucher';

    protected $fillable = [
        'id_bank_movements',
        'id_voucher',
        'state',
        'amount'
    ];
}
