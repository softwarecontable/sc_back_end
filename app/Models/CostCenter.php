<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CostCenter extends Model
{
    use HasFactory;

    protected $table = 'centro_costos';

    protected $fillable = [
        'codigo',
        'descripcion',
        'company_id'
    ];
}
