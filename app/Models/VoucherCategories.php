<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoucherCategories extends Model
{
    use HasFactory;

    protected $table = 'voucher_categories';

    protected $fillable = [
        'name',
        'type',
        'company_id',
        'state'
    ];
}
