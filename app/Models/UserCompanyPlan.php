<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCompanyPlan extends Model
{
    use HasFactory;

    protected $table = 'user_company_plan';

    protected $fillable = [
        'user_company_id',
        'state'
    ];
}
