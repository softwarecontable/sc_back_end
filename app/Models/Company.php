<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $table = 'company';

    protected $casts = [
        'id' => 'integer',
        'name'=> 'string',
        'type_company' => 'integer',
        'tax_regime' => 'integer',
        'currency' => 'integer',
    ];

    protected $fillable = [
        'name',
        'line_business',
        'type_company',
        'tax_regime',
        'ruc',
        'currency'
    ];
}
