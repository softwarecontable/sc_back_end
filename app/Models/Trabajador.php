<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trabajador extends Model
{
    use HasFactory;

    protected $table = 'trabajador';

    protected $fillable = [
        'type_document',
        'nro_document',
        'razon_social',
        'email',
        'telefono',
        'direccion',
        'type',
        'company_id'
    ];
}
