<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    use HasFactory;

    protected $table = 'compra';

    protected $fillable = [
        'secuencia',
        'conceptoComprobante',
        'radioCompras',
        'anexo',
        'codigo',
        'razonSocial',
        'ruc',
        'tipoDocumento',
        'serie',
        'numero',
        'fechaEmision',
        'fechaVencimiento',
        'destino',
        'tasaIgv',
        'baseImp',
        'montoIgv',
        'total',
        'medioPago',
        'glosa',
        'unidadNegocio',
        'fechaCompra',
        'periodoContable',
        'moneda',
        'numeroDetraccion',
        'fechaDetraccion',
        'totalRecHonorarios',
        'irRecHonorarios',
        'conversion',
        'fechaTipoCambio',
        'tcEspecial',
        'tcMonto',
        'totalCabecera',
        'totalDetalle',
        'id_movimiento'
    ];
}
