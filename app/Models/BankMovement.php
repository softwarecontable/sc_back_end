<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankMovement extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $fillable = [
      'date',
      'concept',
      'amount',
      'bank_id',
      'state'
    ];
}
