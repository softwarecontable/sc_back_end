<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    use HasFactory;

    protected $table = 'voucher';

    protected $fillable = [
        'id_spending',
        'amount',
        'file_img',
        'state',
        'currency_id',
        'sale_report_id'
    ];
}
