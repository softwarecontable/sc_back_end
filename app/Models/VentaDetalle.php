<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VentaDetalle extends Model
{
    use HasFactory;

    protected $table = 'venta_detalle';

    protected $fillable = [
        'venta_id',
        'secuencia',
        'numero_cuenta',
        'nombre_cuenta',
        'codigo_producto',
        'nombre_producto',
        'cantidad',
        'unidad_medida',
        'costo_unitario',
        'cod_c_costos',
        'nombre_c_costos',
        'destino',
        'importe',
        'tipo_doc',
        'serie',
        'numero'
    ];
}
