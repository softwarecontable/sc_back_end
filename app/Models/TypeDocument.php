<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TypeDocument extends Model
{
    use HasFactory;

    protected $table = 'company_number';


    /**
     *  TIPOS DE DOCUMENTOS
     */
    public const BOLETA_VENTA = 3;
    public const FACTURA = 1;
    public const NOTA_VENTA = 99;
    public const COTIZACION = 100;
    public const RESUMEN = 200;
    public const NOTA_CREDITO = 7;
    public const NOTA_DEBITO = 8;
    public const RETENCION = 20;

    public static function needsSunatSend(string $tipoDoc): bool {
        $requiring = array(
            self::BOLETA_VENTA
            , self::FACTURA
            , self::NOTA_CREDITO
            , self::NOTA_DEBITO
            , self::RETENCION
        );
        return in_array(intval($tipoDoc), $requiring);
    }

    public static function requiresStock(string $tipoDoc): bool {
        $requiring = array(self::BOLETA_VENTA, self::FACTURA, self::NOTA_VENTA);
        return in_array(intval($tipoDoc), $requiring);
    }

    public static function updateNumber($isDebug, $docType, $id) {
        $exists = DB::table('company_number')->where('company_id', $id)->exists();

        if (!$exists) {
            DB::table('company_number')
                ->insert([
                    "company_id" => $id
                ]);
        }

        $item = DB::table('company_number')->where('company_id', $id)->first();
        $number = 0;

        switch ($docType) {
          case TypeDocument::BOLETA_VENTA:
            $number = $item->sale_bill;
            if (!$isDebug) {
                DB::table('company_number')
                    ->where('company_id', $id)
                    ->update([
                        "sale_bill" => $number + 1
                    ]);
            }
            break;
          case TypeDocument::FACTURA:
            $number = $item->bill;
            if (!$isDebug) {
                DB::table('company_number')
                    ->where('company_id', $id)
                    ->update([
                        "bill" => $number + 1
                    ]);
            }
            break;
          case TypeDocument::NOTA_VENTA:
            $number = $item->sale_note;
            if (!$isDebug) {
                DB::table('company_number')
                    ->where('company_id', $id)
                    ->update([
                        "sale_note" => $number + 1
                    ]);
            }
            break;
          case TypeDocument::COTIZACION:
            $number = $item->price;
            if (!$isDebug) {
                DB::table('company_number')
                    ->where('company_id', $id)
                    ->update([
                        "price" => $number + 1
                    ]);
            }
            break;
          case TypeDocument::NOTA_CREDITO:
            $number = $item->credit_note;
            if (!$isDebug) {
                DB::table('company_number')
                    ->where('company_id', $id)
                    ->update([
                        "credit_note" => $number + 1
                    ]);
            }
            break;
          case TypeDocument::NOTA_DEBITO:
            $number = $item->debit_note;
            if (!$isDebug) {
                DB::table('company_number')
                    ->where('company_id', $id)
                    ->update([
                        "debit_note" => $number + 1
                    ]);
            }
            break;
          case TypeDocument::RETENCION:
            $number = $item->debit_note;
            if (!$isDebug) {
                DB::table('company_number')
                    ->where('company_id', $id)
                    ->update([
                        "retention" => $number + 1
                    ]);
            }
            break;
          default:
            $number = $item->sale_bill;
            if (!$isDebug) {
                DB::table('company_number')
                    ->where('company_id', $id)
                    ->update([
                        "sale_bill" => $number + 1
                    ]);
            }
            break;
        }
        return $number + 1;
    }

    public static function getNumber($id) {
        $exists = DB::table('company_number')->where('company_id', $id)->exists();

        if (!$exists) {
            DB::table('company_number')
                ->insert([
                    "company_id" => $id
                ]);
        }

        $number = DB::table('company_number')->where('company_id', $id)->first();
        return $number;
    }

    public static function getTag($id, $tipoDoc): string {
        $number = TypeDocument::getNumber($id);
        $voucher_tag = "";
        if (intval($tipoDoc) == TypeDocument::BOLETA_VENTA) {
            $voucher_tag = "000" .$number->sale_bill;
        } else if (intval($tipoDoc) == TypeDocument::FACTURA) {
            $voucher_tag = "000" .$number->bill;
        } else if (intval($tipoDoc) == TypeDocument::NOTA_VENTA) {
            $voucher_tag = "000" .$number->sale_note;
        } else if (intval($tipoDoc) == TypeDocument::COTIZACION) {
            $voucher_tag = "000" .$number->price;
        } else if (intval($tipoDoc) == TypeDocument::NOTA_CREDITO) {
            $voucher_tag = "000" .$number->credit_note;
        } else if (intval($tipoDoc) == TypeDocument::NOTA_DEBITO) {
            $voucher_tag = "000" .$number->debit_note;
        }
        return $voucher_tag;
    }

    public static function docSummaries() {
        return ["0".self::BOLETA_VENTA, "0".self::NOTA_DEBITO, "0".self::NOTA_CREDITO];
    }

    public static function getSerie($id): string {
        $serie = DB::table('company')
            ->where('id', $id)
            ->first()
            ->serie;

        if ($serie < 10) {
            return "00" .$serie;
        } else if ($serie < 100) {
            return "0" .$serie;
        } else {
            return strval($serie);
        }
    }

}
