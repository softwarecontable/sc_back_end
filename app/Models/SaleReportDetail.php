<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaleReportDetail extends Model
{
    use HasFactory;

    protected $table = 'sale_report_detail';

}
