<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    use HasFactory;

    protected $table = 'banks';

    protected $fillable = [
        'bank_catalog_id',
        'cuenta_catalog_id',
        'account_number',
        'account_name',
        'currency_catalog_id',
        'initial_balance',
        'type',
        'date'
    ];
}
