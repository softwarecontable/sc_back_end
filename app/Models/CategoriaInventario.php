<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoriaInventario extends Model
{
    use HasFactory;

    protected $table = 'categoria_inventario';

    protected $fillable = [
        'id_padre',
        'name',
        'company_id'
    ];
}
