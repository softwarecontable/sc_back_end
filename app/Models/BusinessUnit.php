<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessUnit extends Model
{
    use HasFactory;

    protected $table = 'unidad_negocio';

    protected $fillable = [
        'codigo',
        'descripcion',
        'company_id'
    ];
}
