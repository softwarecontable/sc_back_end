<?php

use App\Http\Controllers\QrCodeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\UserCompanyController;
use App\Http\Controllers\UserCompanyPlanController;
use App\Http\Controllers\CatalogController;
use App\Http\Controllers\BankController;
use App\Http\Controllers\BankMovementController;
use App\Http\Controllers\VoucherCategoriesController;
use App\Http\Controllers\SpendingController;
use App\Http\Controllers\VoucherController;
use App\Http\Controllers\MovementsVoucherController;
use App\Http\Controllers\CompraController;
use App\Http\Controllers\VentaController;
use App\Http\Controllers\CompraDetalleController;
use App\Http\Controllers\VentaDetalleController;
use App\Http\Controllers\PlanCuentasController;
use App\Http\Controllers\CentroCostosController;
use App\Http\Controllers\ProductosController;
use App\Http\Controllers\UnidadMedidaController;
use App\Http\Controllers\ExchangeController;
use App\Http\Controllers\ReporteController;
use App\Http\Controllers\CategoriaInventarioController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\ProveedorController;
use App\Http\Controllers\TrabajadorController;
use App\Http\Controllers\InventarioController;
use App\Http\Controllers\CostCenterController;
use App\Http\Controllers\ChartAccountsController;
use App\Http\Controllers\BusinessUnitController;
use App\Http\Controllers\SaleController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\AnalyticController;
use App\Http\Controllers\ProductManagerController;
use App\Http\Controllers\GreenterController;
use App\Http\Controllers\EncryptionController;
use App\Http\Controllers\PdfController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('showBill/{url}', [PdfController::class, 'showBill']);
Route::get('qrCode', [QrCodeController::class, 'test']);
Route::get('test', [GreenterController::class, 'test']);
Route::get('summary', [GreenterController::class, 'sendSummary']);
//Route::get('encrypt/{string}', [EncryptionController::class, 'encrypt']);
Route::get('catalog', [CatalogController::class, 'list']);
Route::post('users', [UserController::class, 'store']);
Route::post('companies', [CompanyController::class, 'store']);
Route::post('vinculecompany', [UserCompanyController::class, 'store']);
Route::post('companyplan', [UserCompanyPlanController::class, 'store']);
Route::post('login', [UserController::class, 'login']);
Route::post('loginFacebook', [UserController::class, 'loginFacebook']);
Route::post('loginApple', [UserController::class, 'loginApple']);
Route::post('loginBackOffice', [UserController::class, 'loginBackOffice']);
Route::post('sendEmailRecover', [UserController::class, 'sendEmailRecover']);
Route::post('changePassword', [UserController::class, 'changePassword']);
Route::post('contact', [ContactController::class, 'contact']);

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('listItems/{id}', [CatalogController::class, 'listItems']);
    Route::get('saleItems/{id}', [CatalogController::class, 'saleItems']);
    Route::get('noteItems/{id}', [CatalogController::class, 'noteItems']);
    Route::get('getCompanyParams/{id}', [CatalogController::class, 'getCompanyParams']);
    Route::post('storeAudio', [BankMovementController::class, 'storeAudio']);
    Route::post('company_bank', [BankController::class, 'store']);
    Route::post('add_cash', [BankController::class, 'addCash']);
    Route::post('list_company', [CompanyController::class, 'list']);
    Route::post('updateCompany', [CompanyController::class, 'update']);
    // Route::post('uploadLogo', [CompanyController::class, 'uploadLogo']);
    // Route::post('uploadCertified', [CompanyController::class, 'uploadCertified']);
    Route::post('listReports', [ReporteController::class, 'listReports']);
    Route::post('listBank', [BankController::class, 'list']);
    Route::post('preview', [BankMovementController::class, 'previewImport']);
    Route::post('import', [BankMovementController::class, 'import']);
    Route::post('deleteMovement', [BankMovementController::class, 'delete']);
    Route::post('saveBankMovement', [BankMovementController::class, 'store']);
    Route::post('listBankMovements', [BankMovementController::class, 'list']);
    Route::post('listBankMovementsBackOffice', [BankMovementController::class, 'listBackOffice']);
    Route::post('listTransacc', [BankMovementController::class, 'listTransactionByMovement']);
    Route::post('logout', [UserController::class, 'logout']);
    Route::post('saveVoucherCategories', [VoucherCategoriesController::class, 'store']);
    Route::post('listVoucherCategories', [VoucherCategoriesController::class, 'list']);
    Route::post('deleteVoucherCategories', [VoucherCategoriesController::class, 'delete']);
    Route::post('editVoucherCategories', [VoucherCategoriesController::class, 'edit']);
    Route::post('saveSpending', [SpendingController::class, 'store']);
    Route::post('deleteSpending', [SpendingController::class, 'delete']);
    Route::post('editSpending', [SpendingController::class, 'edit']);
    Route::post('listSpending', [SpendingController::class, 'list']);
    Route::post('saveVoucher', [VoucherController::class, 'store']);
    Route::post('saveVoucherBox', [VoucherController::class, 'storeBox']);
    Route::post('listVoucher', [VoucherController::class, 'list']);
    Route::post('deleteVoucher', [VoucherController::class, 'delete']);
    Route::post('storeVoucherAudio', [VoucherController::class, 'storeAudio']);
    Route::post('listVoucherCompany', [VoucherController::class, 'listVoucherCompany']);
    Route::post('saveMovementsVoucher', [MovementsVoucherController::class, 'store']);
    Route::post('listVoucherMovements', [MovementsVoucherController::class, 'listByMovement']);
    Route::post('listVoucherByMovement', [MovementsVoucherController::class, 'listVoucherByMovement']);
    Route::post('listAudioByMovement', [MovementsVoucherController::class, 'listAudioByMovement']);
    Route::post('saveCompra', [CompraController::class, 'store']);
    Route::post('listCompraByMovement', [CompraController::class, 'listByMovement']);
    Route::post('deleteCompra', [CompraController::class, 'deleteCompra']);
    Route::post('saveVenta', [VentaController::class, 'store']);
    Route::post('listVentaByMovement', [VentaController::class, 'listByMovement']);
    Route::post('deleteVenta', [VentaController::class, 'deleteVenta']);
    Route::post('saveCompraDetalle', [CompraDetalleController::class, 'store']);
    Route::post('listCompraDetalle', [CompraDetalleController::class, 'listDetalleCompra']);
    Route::post('deleteCompraDetalle', [CompraDetalleController::class, 'deleteDetalleCompra']);
    Route::post('saveVentaDetalle', [VentaDetalleController::class, 'store']);
    Route::post('listVentaDetalle', [VentaDetalleController::class, 'listDetalleVenta']);
    Route::post('deleteVentaDetalle', [VentaDetalleController::class, 'deleteDetalleVenta']);
    Route::post('searchPcByCod', [PlanCuentasController::class, 'searchByCod']);
    Route::post('searchCcByCod', [CentroCostosController::class, 'searchByCod']);
    Route::post('invoiceSend', [ProductosController::class, 'ceSend']);
    Route::post('noteSend', [ProductosController::class, 'noteSend']);
    Route::post('saveDocument', [ProductosController::class, 'saveDocument']);
    Route::post('validateDocument', [ProductosController::class, 'validateDocument']);
    Route::post('searchProducByCod', [ProductosController::class, 'searchByCod']);
    Route::post('getAllProductsCategoriaByCompanyAndLevel', [ProductosController::class, 'getAllProductsCategoriaByCompanyAndLevel']);
    Route::post('getProductByCode', [ProductosController::class, 'getProductByCode']);
    Route::get('listSaleReport/{id}', [SaleController::class, 'listSaleReport']);
    Route::post('voidedSend', [SaleController::class, 'voidedSend']);
    Route::post('listUnidadMedida', [UnidadMedidaController::class, 'list']);
    Route::post('saveExchange', [ExchangeController::class, 'store']);
    Route::post('listExchange', [ExchangeController::class, 'list']);
    Route::post('uploadReporte', [ReporteController::class, 'store']);
    Route::post('deleteReporte', [ReporteController::class, 'deleteReport']);
    Route::post('saveCatInventario', [CategoriaInventarioController::class, 'store']);
    Route::post('listByPadre', [CategoriaInventarioController::class, 'listByPadre']);
    Route::post('deleteCatInventario', [CategoriaInventarioController::class, 'deleteCatInv']);
    Route::post('getAllSegmentos', [CategoriaInventarioController::class, 'getAllSegmento']);
    Route::post('getFamiliaBySegmento', [CategoriaInventarioController::class, 'getFamiliaBySegmento']);
    Route::post('getClaseByFamilia', [CategoriaInventarioController::class, 'getClaseByFamilia']);
    Route::post('getAllProductsByCompany', [ProductosController::class, 'getAllProductsByCompany']);
    Route::post('getProductsByClase', [ProductosController::class, 'getProductsByClase']);
    Route::post('saveClient', [ClienteController::class, 'store']);
    Route::post('listClient', [ClienteController::class, 'list']);
    Route::post('deleteClient', [ClienteController::class, 'delete']);
    Route::post('saveProvider', [ProveedorController::class, 'store']);
    Route::post('listProvider', [ProveedorController::class, 'list']);
    Route::post('deleteProvider', [ProveedorController::class, 'delete']);
    Route::post('saveTrabajador', [TrabajadorController::class, 'store']);
    Route::post('listTrabajador', [TrabajadorController::class, 'list']);
    Route::post('deleteTrabajador', [TrabajadorController::class, 'delete']);
    Route::post('saveProductInventario', [InventarioController::class, 'store']);
    Route::post('editInventario', [InventarioController::class, 'edit']);
    Route::post('deleteInventario', [InventarioController::class, 'delete']);
    Route::post('saveInventario', [InventarioController::class, 'addInventario']);
    Route::post('importInventario', [InventarioController::class, 'import']);
    Route::post('saveCostCenter', [CostCenterController::class, 'store']);
    Route::post('listCostCenter', [CostCenterController::class, 'list']);
    Route::post('deleteCostCenter', [CostCenterController::class, 'delete']);
    Route::post('saveChartAccounts', [ChartAccountsController::class, 'store']);
    Route::post('listChartAccounts', [ChartAccountsController::class, 'list']);
    Route::post('deleteChartAccounts', [ChartAccountsController::class, 'delete']);
    Route::post('saveBusinessUnit', [BusinessUnitController::class, 'store']);
    Route::post('listBusinessUnit', [BusinessUnitController::class, 'list']);
    Route::post('deleteBusinessUnit', [BusinessUnitController::class, 'delete']);
    Route::post('importSavePlanCuentas', [PlanCuentasController::class, 'importSavePlanCuentas']);
    Route::get('getReport/{id}/{start?}/{end?}', [AnalyticController::class, 'getReport']);
    Route::get('getSaleReportByDate/{id}/{start}/{end}', [AnalyticController::class, 'getSaleReportByDate']);
    Route::get('productList/{id}', [ProductManagerController::class, 'list']);
    Route::get('params/{id}', [ProductManagerController::class, 'params']);
    Route::get('productStatus/{id}', [ProductManagerController::class, 'status']);
    Route::post('productUpdate', [ProductManagerController::class, 'update']);
    Route::post('newOrder', [ProductManagerController::class, 'newOrder']);
    Route::put('endOrder/{id}', [ProductManagerController::class, 'endOrder']);
});
